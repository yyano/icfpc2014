package src;

import javax.swing.JFrame;

public class Frame extends JFrame {
	private static final long serialVersionUID = 7426392552047242376L;

	public Frame() {
		super();
		setTitle("ICFPC2014");
		setSize(800, 600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
