package src;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

	public static void main(String args[]) throws InterruptedException {
		Frame frame = new Frame();
		Panel panel = new Panel();
		frame.getContentPane().add(panel);
		frame.setVisible(true);
		while (true) {
			List<List<Integer>> map = new ArrayList<List<Integer>>();
			Scanner scan = new Scanner(System.in);
			int h = scan.nextInt();
			if (h == 0) {
				System.exit(0);
			}
			int w = scan.nextInt();
			for (int i = 0; i < h; ++i) {
				List<Integer> row = new ArrayList<Integer>();
				for (int j = 0; j < w; ++j) {
					row.add(scan.nextInt());
				}
				map.add(row);
			}
			panel.pacman.v = scan.nextInt();
			panel.pacman.y = scan.nextInt();
			panel.pacman.x = scan.nextInt();
			panel.pacman.d = scan.nextInt();
			panel.pacman.l = scan.nextInt();
			panel.pacman.s = scan.nextInt();
			int n = scan.nextInt();
			List<Ghost> ghost = new ArrayList<Ghost>();
			for (int i = 0; i < n; ++i) {
				Ghost g = new Ghost();
				g.v = scan.nextInt();
				g.y = scan.nextInt();
				g.x = scan.nextInt();
				g.d = scan.nextInt();
				ghost.add(g);
			}
			panel.ghost = ghost;
			int m = scan.nextInt();
			panel.tick = scan.nextInt();
			panel.map = map;
			panel.repaint();
		}
	}
}
