package src;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

public class Panel extends JPanel {

	private static final long serialVersionUID = 444432094419798936L;

	public List<List<Integer>> map = new ArrayList<List<Integer>>();
	public Pacman pacman = new Pacman();
	public List<Ghost> ghost;
	public int tick;
	
	public void paint(Graphics graphics) {
		int h = map.size();
		if (h == 0) {
			return;
		}
		int w = map.get(0).size();
		int size = Math.min(750 / w, 550 / h);
		graphics.clearRect(0, 0, 800, 600);
		if (pacman.v > 0) {
			graphics.setColor(Color.RED);
		} else {
			graphics.setColor(Color.WHITE);
		}
		graphics.fillRect(0, 0, 600, 600);
		for (int i = 0; i < h; ++i) {
			for (int j = 0; j < w; ++j) {
				int item = map.get(i).get(j);
				switch (item) {
				case 0:
					graphics.setColor(Color.BLACK);
					break;
				case 1:
					graphics.setColor(Color.WHITE);
					break;
				case 2:
					graphics.setColor(Color.YELLOW);
					break;
				case 3:
					graphics.setColor(Color.RED);
					break;
				case 4:
					graphics.setColor(Color.PINK);
					break;
				case 5:
					graphics.setColor(Color.BLUE);
					break;
				case 6:
					graphics.setColor(Color.GREEN);
					break;
				}
				graphics.fillRect(10 + size * j + 1, 10 + size * i + 1, size - 1, size - 1);
			}
		}
		graphics.setColor(Color.ORANGE);
		graphics.fillArc(10 + size * pacman.x, 10 + size * pacman.y, size, size, 120 - pacman.d * 90, 300);
		for (int i = 0; i < ghost.size(); ++i) {
			graphics.setColor(Color.LIGHT_GRAY);
			graphics.fillArc(10 + size * ghost.get(i).x, 10 + size * ghost.get(i).y, size, size, 120 - ghost.get(i).d * 90, 360);
			graphics.fillRect(10 + size * ghost.get(i).x, 10 + size * ghost.get(i).y + size / 2, size, size / 2);
		}
		graphics.setColor(Color.BLACK);
		graphics.drawString("vitality : " + String.valueOf(pacman.v), 650, 75);
		graphics.drawString((String.valueOf(pacman.x) + " , " + String.valueOf(pacman.y)), 650, 100);
		String dir[] = {"↑", "→", "↓", "←"};
		graphics.drawString(dir[pacman.d], 650, 125);
		graphics.drawString("life : " + String.valueOf(pacman.l), 650, 150);
		graphics.drawString("score : " + String.valueOf(pacman.s), 650, 175);
		graphics.drawString("tick/127 : " + String.valueOf(tick / 127), 650, 200);
	}

}
