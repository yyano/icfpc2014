#! /usr/bin/env python

import logging
import subprocess, shlex

viscommand = "java -jar vis.jar"

class VisManager:
    def __init__(self, game = None):
        args = shlex.split(viscommand)
        self.process = subprocess.Popen(args, bufsize = 1024 * 1024, stdin = subprocess.PIPE)
        self.game = game

    def set_game(self, new_game):
        self.game = new_game

    def update(self):
        message = self.game.state_string(self.game.PROTOCOL_VIS)
        logging.debug("send to visualizer.")
        logging.debug(message)
        self.process.stdin.write(message)
        self.process.stdin.flush()

    def terminate(self):
        message = "0\n"
        logging.debug("send terminate command to visualizer.")
        self.process.stdin.write(message)
        self.process.stdin.flush()
        self.process.wait()

