#! /usr/bin/env python
import logging
import random
import StringIO
import shlex, subprocess
import time

from vismanager import VisManager
from simulator import GHC

class RandomAI:
    def __init__(self):
        pass

    def run(self, game, me):
        return random.randint(0, 3)

class TestAI:
    def __init__(self):
        self.past = 0
    
    def run(self, game, me):
        if random.randint(0, 10) == 0:
            trylist = range(4)
            random.shuffle(trylist)
        else:
            trylist = range(1, 4)
            random.shuffle(trylist)
            trylist = [0] + trylist
        for i in trylist:
            tryid = (self.past + i) % 4
            next_pos = dirs[tryid].new_pos(*me.pos)
            if game.map_data.is_passible(*next_pos): break
        self.past = tryid
        return tryid

class HumanAI:
    def __init__(self):
        pass

    def init_ai(self, game, me):
        pass

    def run(self, game, me):
        print 'I am at {0}. choose direction (s = up, c = right, x = down, z = left)'.format(me.pos)
        ret = -1
        while not 0 <= ret < 4:
            print 'direction? >',
            ret = {'s': 0, 'c': 1, 'x': 2, 'z': 3}.get(raw_input(), -1)
        return ret

class LambdaManPlayer:
    @staticmethod
    def generator(command):
        return lambda :LambdaManPlayer(command)

    def __init__(self, command):
        args = shlex.split(command)
        self.process = subprocess.Popen(args, stdin = subprocess.PIPE, stdout = subprocess.PIPE, stderr = subprocess.PIPE)

    def init_ai(self, game, me, need_sout = False):
        world_str = game.state_string()
        logging.debug("sent message to lambdaman ai.")
        logging.info(world_str)
        self.process.stdin.write(world_str)
        self.process.stdin.flush()

        serr_buf = StringIO.StringIO()
        while True:
            serr_line = self.process.stderr.readline()
            serr_buf.write(serr_line)
            logging.info(serr_line)
            if serr_line.startswith('MACHINE STOP'):
                break
            elif serr_line.startswith('Error'):
                serr_buf.write(self.process.stderr.read())
                break
        serr = serr_buf.getvalue()
        serr_buf.close()
        logging.info("lambdaman ai stderr response")
        logging.info(serr)
        if game.procerr: game.procerr.write(serr)

        if need_sout:
            sout = self.process.stdout.read(2)
            logging.info("lambdaman ai stdout response")
            logging.info(sout)
        else:
            sout = ""

        return sout, serr

    def run(self, game, me):
        sout, serr = self.init_ai(game, me, True)
        # recieve decision.
        return int(sout.strip())

class GhostPlayer:
    @staticmethod
    def generator(source_or_file):
        return lambda :GhostPlayer(source_or_file)

    def __init__(self, source_or_file):
        self.ghc = GHC(source_or_file)

    def run(self, game, me):
        self.ghc.game = game
        self.ghc.me = me
        return self.ghc.run()

class Direction:
    def __init__(self, dx, dy):
        self.dx = dx
        self.dy = dy

    def new_pos(self, x, y):
        return x + self.dx, y + self.dy

    def is_inverse(self, op):
        return self.dx == -op.dx and self.dy == -op.dy

    def reversed(self):
        return Direction(-self.dx, -self.dy)

    def __repr__(self):
        return "Direction({0}, {1})".format(self.dx, self.dy)

# up, right, down, left, stop
dirs = [Direction(0, -1), Direction(1, 0), Direction(0, 1), Direction(-1, 0)]

def dir2index(d):
    for i, cand_d in enumerate(dirs):
        if cand_d.dx == d.dx and cand_d.dy == d.dy: return i
    return 4

class LambdaMan:
    def __init__(self, init_position, ai_factory, game):
        self.init_pos = init_position
        self.reset()
        self.ai = ai_factory()
        self.next_tick = 128
        self.game = game

    def reset(self):
        self.pos = self.init_pos
        self.prev_direction = dirs[2]

    def move(self):
        if self.game.tick != self.next_tick: return
        direction = dirs[self.ai.run(self.game, self)]
        new_position = direction.new_pos(*self.pos)
        if self.game.map_data.is_wall(*new_position): 
            new_position = self.pos # don't move
        self.pos = new_position
        self.prev_direction = direction
        if self.game.map_data.has_food(*self.pos):
            self.next_tick += 137
        else:
            self.next_tick += 127

    def vitality(self):
        return self.game.vitality()

    def direction(self):
        return dir2index(self.prev_direction)

class Ghost:
    def __init__(self, init_position, ai_factory, game):
        self.init_pos = init_position
        self.reset()
        self.ai = ai_factory()
        self.next_tick = 1
        self.visible = True
        self.game = game

    def vitality(self):
        if not self.visible: return 2
        if self.game.fright: return 1
        return 0

    def direction(self):
        return dir2index(self.prev_direction)

    def reset(self):
        self.pos = self.init_pos
        self.prev_direction = dirs[2]

    def move(self):
        if self.game.tick != self.next_tick: return
        available_set = []
        for d in dirs:
            adj_pos = d.new_pos(*self.pos)
            if self.game.map_data.is_passible(*adj_pos): available_set.append(d)
        
        choice = len(available_set)
        if choice == 0:
            # surrounded by wall
            direction = Direction(0, 0)
        elif choice == 1:
            # dead end
            direction = available_set[0]
        elif choice == 2:
            # non-junction
            for cand_dir in available_set:
                if not cand_dir.is_inverse(self.prev_direction):
                    # non-backward direction. use this.
                    direction = cand_dir
                    break
        else:
            # junction
            direction = dirs[self.ai.run(self.game, self)]
            def is_illegal(d):
                return d.is_inverse(self.prev_direction) or self.game.map_data.is_wall(*d.new_pos(*self.pos))
            if is_illegal(direction): direction = self.prev_direction
            if is_illegal(direction):
                for cand_dir in available_set:
                    if not is_illegal(cand_dir):
                        direction = cand_dir
                        break
        self.pos = direction.new_pos(*self.pos)
        self.prev_direction = direction
        if self.game.fright:
            self.next_tick += self.tick_per_move * 3 / 2
        else:
            self.next_tick += self.tick_per_move

class Map:
    LAMBDA = '\\'
    GHOST = '='
    FRUIT = '%'
    PILL = '.'
    POWERPILL = 'o'
    WALL = '#'
    EMPTY = ' '

    @staticmethod
    def char2index(char):
        if char == Map.WALL: return 0
        if char == Map.EMPTY: return 1
        if char == Map.PILL: return 2
        if char == Map.POWERPILL: return 3
        if char == Map.FRUIT: return 4
        if char == Map.LAMBDA: return 5
        if char == Map.GHOST: return 6
        assert False, "invalid char"

    def __init__(self, str_or_file):
        # load map integer
        if type(str_or_file) == str:
            self.input_str = str_or_file
        else:
            self.input_str = str_or_file.read()
        lines = self.input_str.strip().split('\n')
        self.width = len(lines[0])
        self.height = len(lines)
        for line in lines: assert len(line) == self.width, "Map.__init__: map width differs."
        self.field = map(list, lines)

        for x in xrange(self.width):
            assert self.field[0][x] == Map.WALL, "map should have walls around the edges."
            assert self.field[self.height - 1][x] == Map.WALL, "map should have walls around the edges."

        for y in xrange(self.height):
            assert self.field[y][0] == Map.WALL, "map should have walls around the edges."
            assert self.field[y][self.width - 1] == Map.WALL, "map should have walls around the edges."

        # find initial lambda-man and ghost        
        self.lambdamans = []
        self.ghosts = []
        self.fruits = []
        self.pill_count = 0
        self.power_pill_count = 0
        for y in xrange(self.height):
            for x in xrange(self.width):
                if self.field[y][x] == Map.LAMBDA:
                    # (x, y) is lambda-man init position
                    self.lambdamans.append((x, y))
                    self.field[y][x] = Map.EMPTY # replace with empty square
                elif self.field[y][x] == Map.GHOST:
                    # (x, y) is ghost init position
                    self.ghosts.append((x, y))
                    self.field[y][x] = Map.EMPTY # replace with empty square
                elif self.field[y][x] == Map.FRUIT:
                    # (x, y) is fruit position
                    self.fruits.append((x, y))
                    self.field[y][x] = Map.EMPTY # replace with empty square
                elif self.field[y][x] == Map.PILL:
                    self.pill_count += 1

    def test(self, x, y, element):
        return self.field[y][x] == element

    def is_wall(self, x, y):
        return self.test(x, y, Map.WALL)

    def is_passible(self, x, y):
        return not self.is_wall(x, y)

    def is_pill(self, x, y):
        return self.test(x, y, Map.PILL)

    def is_powerpill(self, x, y):
        return self.test(x, y, Map.POWERPILL)

    def is_fruit(self, x, y):
        return self.test(x, y, Map.FRUIT)

    def is_empty(self, x, y):
        return self.test(x, y, Map.EMPTY)

    def has_food(self, x, y):
        return self.test(x, y, Map.PILL) or self.test(x, y, Map.POWERPILL) or self.test(x, y, Map.FRUIT)

    def eat_pill(self, x, y):
        assert self.is_pill(x, y), "no pill!"
        self.field[y][x] = Map.EMPTY
        self.pill_count -= 1

    def eat_powerpill(self, x, y):
        assert self.is_powerpill(x, y), "no powerpill!"
        self.field[y][x] = Map.EMPTY

    def eat_fruit(self, x, y):
        assert self.is_fruit(x, y), "no fruit!"
        self.field[y][x] = Map.EMPTY
        
    def remove_fruit(self):
        for x, y in self.fruits:
            self.field[y][x] = Map.EMPTY

    def put_fruit(self):
        for x, y in self.fruits:
            assert self.is_empty(x, y), "can't put fruit. not empty."
            self.field[y][x] = Map.FRUIT

        
class Game:
    def __init__(self, map_init_data, lambda_ai_factories, ghost_ai_factories, procerr = None):
        self.procerr = procerr
        self.map_data = Map(map_init_data)
        self.fright = False
        self.eol_tick = 127 * self.map_data.width * self.map_data.height * 16
        self.fright_expire_tick = -1
        self.level = (self.map_data.width * self.map_data.height + 99) / 100
        fruit_score_map = {1: 100,
                           2: 300,
                           3: 500,
                           4: 500,
                           5: 700,
                           6: 700,
                           7: 1000,
                           8: 1000,
                           9: 2000,
                           10: 2000,
                           11: 3000,
                           12: 3000}
        self.fruit_score = fruit_score_map.get(self.level, 5000)
        self.fruit_appear_ticks = [127 * 200, 127 * 400]
        self.fruit_remove_ticks = [127 * 280, 127 * 480]
        self.ghost_eated = 0

        lambda_count = len(self.map_data.lambdamans)
        self.lambdamans = [LambdaMan(self.map_data.lambdamans[i], lambda_ai_factories[i], self) for i in xrange(lambda_count)]
        self.ghosts = []
        for i, ghost_position in enumerate(self.map_data.ghosts):
            ai_index = i % len(ghost_ai_factories)
            ghost_obj = Ghost(ghost_position, ghost_ai_factories[ai_index], self)
            ghost_obj.tick_per_move = 130 + ai_index * 2
            ghost_obj.next_tick = 1 + ghost_obj.tick_per_move
            ghost_obj.index = len(self.ghosts)
            self.ghosts.append(ghost_obj)

        self.tick = 1
        self.score = 0
        self.life = 3

        for lambdaman in self.lambdamans:
            lambdaman.ai.init_ai(self, lambdaman)

    def vitality(self):
        return max(self.fright_expire_tick - self.tick , 0)

    def fruit_expire_time(self):
        for i in xrange(2):
            if self.fruit_appear_ticks[i] <= self.tick <= self.fruit_remove_ticks[i]:
                return self.fruit_remove_ticks[i] - self.tick
        return 0

    PROTOCOL_GCC = 0
    PROTOCOL_VIS = 1
    def state_string(self, protocol = PROTOCOL_GCC):
        # write world data
        world = StringIO.StringIO()
        world.write("{0} {1}\n".format(self.map_data.height, self.map_data.width)) # H W
        if protocol == Game.PROTOCOL_GCC:
            world.write(self.field_string()) # field
        elif protocol == Game.PROTOCOL_VIS:
            for y, line in enumerate(self.map_data.input_str.strip().split('\n')):
                row = [Map.char2index(c) for c in line]
                resline = []
                for x, cell in enumerate(row):
                    if cell in [2, 3, 4] and not self.map_data.has_food(x, y):
                        # initially have food and it is already eaten.
                        cell = 1
                    resline.append(str(cell))
                world.write(' '.join(resline) + '\n')
        else:
            assert False, "invalid protocol"
        world.write("{0}\n".format(self.vitality())) # vitality
        assert len(self.lambdamans) == 1, "you must update gcc-sim protocol to treat multiple lambdamans!."
        me = self.lambdamans[0]
        world.write("{1} {0}\n".format(*me.pos)) # y x
        world.write("{0}\n".format(dir2index(me.prev_direction))) # direction
        world.write("{0}\n".format(self.life)) # lives
        world.write("{0}\n".format(self.score)) # score
        world.write("{0}\n".format(len(self.ghosts))) # ghost num
        for ghost in self.ghosts:
            world.write("{0}\n".format(ghost.vitality())) # vitality 
            world.write("{1} {0}\n".format(*ghost.pos)) # y x
            world.write("{0}\n".format(dir2index(ghost.prev_direction))) # direction 
        world.write("{0}\n".format(self.fruit_expire_time())) # fruit expire time
        if protocol == Game.PROTOCOL_VIS:
            world.write("{0}\n".format(self.tick)) # fruit expire time
        retstr = world.getvalue()
        world.close()
        return retstr

    def run(self, vis = None, interval = 100, use_step = False, sleep_time = 0.5):
        next_step = 0
        next_interval = 0
        while self.tick < self.eol_tick:
            logging.info('loop start')
            logging.info(str(self))

            if vis:
                if self.tick >= next_interval or (use_step and self.tick >= next_step): 
                    vis.update()
                    time.sleep(sleep_time)

                if use_step:
                    if self.tick >= next_step:
                        print "enter the step:",
                        next_step = self.tick + int(raw_input())

                if self.tick >= next_interval:
                    next_interval += interval

            if not game.main_loop():
                break
        for lambdaman in self.lambdamans:
            lambdaman.ai.process.terminate()
        logging.info('final score : {score}'.format(score = self.score))

    def tick_update(self):
        next_tick = self.eol_tick
        def update(t):
            return min(next_tick, t) if t > self.tick else next_tick
                
        for lambdaman in self.lambdamans:
            next_tick = update(lambdaman.next_tick)
        for ghost in self.ghosts:
            next_tick = update(ghost.next_tick)
        next_tick = update(self.fright_expire_tick)
        for fruit_appear_tick in self.fruit_appear_ticks:
            next_tick = update(fruit_appear_tick)
        for fruit_remove_tick in self.fruit_remove_ticks:
            next_tick = update(fruit_remove_tick)
        self.tick = next_tick

    def main_loop(self):
        # 1. lambda-man, ghost moves as scheduled.
        # don't forget to set next_tick!!!
        for lambdaman in self.lambdamans:
            lambdaman.move()

        for ghost in self.ghosts:
            ghost.move()

        # 2. fright deactivation, fruit appearance update.
        if self.tick == self.fright_expire_tick: 
            self.fright = False
            for ghost in self.ghosts:
                ghost.visible = True

        if self.tick in self.fruit_appear_ticks:
            self.map_data.put_fruit()

        if self.tick in self.fruit_remove_ticks:
            self.map_data.remove_fruit()

        # 3. eat pill
        for lambdaman in self.lambdamans:
            if self.map_data.is_pill(*lambdaman.pos):
                self.map_data.eat_pill(*lambdaman.pos)
                self.score += 10

        # 4. eat power pill and immediately activate fright mode.
        for lambdaman in self.lambdamans:
            if self.map_data.is_powerpill(*lambdaman.pos):
                self.map_data.eat_powerpill(*lambdaman.pos)
                self.score += 50
                self.ghost_eated = 0
                self.fright = True
                self.fright_expire_tick = self.tick + 127 * 20
                for ghost in self.ghosts:
                    ghost.prev_direction = ghost.prev_direction.reversed()

        # 5. eat fruit
        for lambdaman in self.lambdamans:
            if self.map_data.is_fruit(*lambdaman.pos):
                if self.map_data.is_fruit(*lambdaman.pos):
                    self.map_data.eat_fruit(*lambdaman.pos)
                    self.score += self.fruit_score

        # 6. loss life or eat ghost
        for lambdaman in self.lambdamans:
            for ghost in self.ghosts:
                if lambdaman.pos == ghost.pos:
                    if not ghost.visible: continue # invisible ghost has no effect.
                    if self.fright:
                        # eat ghost
                        self.score += 200 * (2 ** min(self.ghost_eated, 3))
                        self.ghost_eated += 1
                        ghost.visible = False
                        ghost.reset()
                    else:
                        # loss life
                        self.life -= 1
                        for lm in self.lambdamans:
                            lm.reset()
                        for gh in self.ghosts:
                            gh.reset()

        # 7. win the game if all ordinary pills are eaten.
        if self.map_data.pill_count == 0:
            logging.info('all ordinary pills are eaten!!! lambda-man wins!')
            self.score *= self.life + 1
            return False

        # 8. lose if life equals 0.
        if self.life == 0:
            logging.info('lambda-man lost all lives!')
            return False

        # 9. increment tick counter
        self.tick_update()
        return True

    def field_string(self):
        field_builder = StringIO.StringIO()
        for y in xrange(self.map_data.height):
            for x in xrange(self.map_data.width):
                pos = (x, y)
                if pos in map(lambda x: x.pos, self.lambdamans):
                    field_builder.write(Map.LAMBDA)
                elif pos in map(lambda x: x.pos, self.ghosts):
                    field_builder.write(Map.GHOST)
                else:
                    field_builder.write(self.map_data.field[y][x])
            field_builder.write('\n')
        field_str = field_builder.getvalue()
        field_builder.close()
        return field_str

    def __str__(self):
        field_str = self.field_string()
        return """<GAME STATE> LIFE: {0}, SCORE: {1}, TICK: {2}
FRIGHT: {3}, PILL LEFT: {4}
{5}""".format(self.life, self.score, self.tick, self.fright, self.map_data.pill_count, field_str)

from argparse import ArgumentParser, FileType
if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('-l', '--lambdaman', nargs = '*', type = str, help = 'gcc file for lambdaman ai.')
    parser.add_argument('-g', '--ghost', nargs = '*', type = file, help = 'ghc file for ghost ai.')
    parser.add_argument('-p', '--processor', type = str, help = 'processor executable')
    parser.add_argument('-m', '--map', type = file, default = './sample_map.txt', help = 'map file')
    parser.add_argument('-v', '--vis', action = 'store_true', default = False, help = 'specify if you want to use a visualizer.')
    parser.add_argument('-r', '--random', action = 'store_true', default = False, help = 'randomize the ghc memory [255]')
    parser.add_argument('--human', action = 'store_true', default = False, help = 'if specified, you can play this game, enjoy!')
    parser.add_argument('-n', '--number', default = 1, help = 'number of game play')
    parser.add_argument('--sleep', default = 0.5, type = float, help = 'sleep time in sec after simulator send the data to visualizer.')
    parser.add_argument('--interval', default = 100, help = 'tick interval for visualizer update')
    parser.add_argument('--seed', default = None, help = 'seed for random module')
    parser.add_argument('--log', default = None, help = 'log destiation. if not specified, log suppressed. if - is specified, log goes to stdout.')
    parser.add_argument('-s', '--step', action = 'store_true', default = False, help = 'specify if you want to execute step by step.')
    parser.add_argument('--procerr', type = FileType('w'), default = None, help = 'lambdaman processor stderr output destination')

    args = parser.parse_args()
    if args.log == '-':
        logging.basicConfig(filename = None, level = logging.DEBUG)
    elif args.log:
        logging.basicConfig(filename = args.log, level = logging.DEBUG)
    else:
        logging.basicConfig(filename = None, level = logging.WARNING)

    ghost_source = []
    for ghost in args.ghost:
        ghost_source.append(ghost.read())
        ghost.close()
    
    map_text = args.map.read()
    args.map.close()

    vis = VisManager() if args.vis else None
    scores = []
    random.seed(args.seed)
    for i in xrange(int(args.number)):
        lambdamans = [HumanAI] if args.human else [LambdaManPlayer.generator(args.processor + ' ' + lambdaman) for lambdaman in args.lambdaman] 
        game = Game(map_text, 
                    lambdamans,
                    [GhostPlayer.generator(ghost) for ghost in ghost_source],
                    args.procerr)
        if vis: vis.set_game(game)
        if args.random or args.number > 1: # randomize seed when play game multiple times.
            for ghost in game.ghosts:
                seed = random.randint(0, 255)
                logging.info('data memory [255] of ghost #{index} is initialized as {seed}'.format(seed = seed, index = ghost.index))
                ghost.ai.ghc.dm[255] = seed
        game.run(vis, int(args.interval), args.step, args.sleep)
        print "run #{num}: Score = {score}".format(num = i, score = game.score)
        scores.append(game.score)
    if vis: vis.terminate()
    if args.procerr: args.procerr.close()
    print "avg. score = {0}".format(sum(scores) / float(args.number))
