#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <vector>
#include <string>
#include <map>
#include <algorithm>

using namespace std;

char s[1000];

vector <string> separate(void)
{
    int n = strlen(s);
    vector <string> command;
    
    for (int i = 0; i < n; i++) {
        string tmp = "";
        
        if (s[i] == ' ' || s[i] == '\t' || s[i] == '\r' || s[i] == '\n') continue;
        
        for (; i < n; i++) {
            if (s[i] == ' ' || s[i] == '\t' || s[i] == '\r' || s[i] == '\n') break;
            
            tmp += s[i];
        }
        
        command.push_back(tmp);
    }
    
    return command;
}

string to_string(int x)
{
    string ret = "";
    
    while (x) {
        ret += '0' + x % 10;
        x /= 10;
    }
    
    reverse(ret.begin(), ret.end());
    
    return ret;
}

int main()
{
    int count = 0;
    vector <vector <string> > commands;
    map <string, string> to_address;
    
    while (fgets(s, 1000, stdin) != NULL) {
        vector <string> command = separate();
        
        if (command.size() == 0) continue;
        
        if (command[0][command[0].size() - 1] == ':') {
            command[0].erase(command[0].size() - 1);
            to_address[command[0]] = to_string(count);
            command.erase(command.begin());
            if (command.size() > 0) commands.push_back(command);
        } else {
            commands.push_back(command);
            count++;
        }
    }
    
    for (int i = 0; i < commands.size(); i++) {
        for (int j = 0; j < commands[i].size(); j++) {
            if (j == 1) {
                putchar('\t');
            } else if (j > 0) {
                putchar(' ');
            }
            
            if (to_address.count(commands[i][j])) {
                printf("%s", to_address[commands[i][j]].c_str());
            } else if (j > 0 && commands[i][j][0] != ';' && (commands[i][j][0] < '0' || commands[i][j][0] > '9')) {
                fprintf(stderr, "converter error, undefined label : %s\n", commands[i][j].c_str());
                
                return 0;
            } else {
                printf("%s", commands[i][j].c_str());
            }
        }
        puts("");
    }
    
    return 0;
}
