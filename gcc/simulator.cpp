#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <vector>
#include <string>
#include <map>
#include <algorithm>

using namespace std;

enum Tag {
    TAG_INT,
    TAG_CONS,
    TAG_JOIN,
    TAG_CLOSURE,
    TAG_DUM,
    TAG_RET,
    TAG_STOP,
    TAG_NONE
};

struct Data {
    Tag tag;
    int data;
    int first;
    int second;
};

enum Command {
    LDC,
    LD,
    ADD,
    SUB,
    MUL,
    DIV,
    CEQ,
    CGT,
    CGTE,
    ATOM,
    CONS,
    CAR,
    CDR,
    SEL,
    JOIN,
    LDF,
    AP,
    RTN,
    DUM,
    RAP,
    STOP,
    TSEL,
    TAP,
    TRAP,
    ST,
    DBUG,
    BRK
};

struct Instruction {
    Command command;
    int parameter1;
    int parameter2;
};

int counter;
int control_register;
int environment_frame_register;
vector <Data> data_stack;
vector <Data> control_stack;
vector <vector <Data> > environment_frame_chain;
vector <Tag> frame_chain_tag;
vector <Data> data_heap;
vector <Instruction> code;
vector <string> comment;
int num[27] = {1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 1, 1, 0, 1, 1, 0, 2, 1, 1, 2, 0, 0};
map <Command, string> to_string;

void print_tag(Data &data);

void print_int(Data &data)
{
    printf("%d", data.data);
}

void print_cons(Data &data)
{
    printf("(");
    print_tag(data_heap[data.first]);
    putchar(',');
    print_tag(data_heap[data.second]);
    putchar(')');
}

void print_join(Data &data)
{
    printf("TAG_JOIN %d", data.data);
}

void print_closure(Data &data)
{
    printf("{%d <env %d>}", data.first, data.second);
}

void print_ret(Data &data)
{
    printf("TAG_RET %d", data.data);
}

void print_stop(Data &data)
{
    printf("TAG_STOP");
}

void print_none(Data &data)
{
    printf("<env %d>", data.data);
}

void print_tag(Data &data)
{
    switch (data.tag) {
        case TAG_INT :
        {
            print_int(data);
            break;
        }
        case TAG_CONS :
        {
            print_cons(data);
            break;
        }
        case TAG_JOIN :
        {
            print_join(data);
            break;
        }
        case TAG_CLOSURE :
        {
            print_closure(data);
            break;
        }
        case TAG_RET :
        {
            print_ret(data);
            break;
        }
        case TAG_STOP :
        {
            print_stop(data);
            break;
        }
        case TAG_NONE :
        {
            print_none(data);
            break;
        }
    }
}

void print(void)
{
    printf("number of command : %d\n", counter);
    printf("control register : %d\n", control_register);
    printf("next instruction : %s", to_string[code[control_register].command].c_str());
    if (num[code[control_register].command] == 1) {
        printf(" %d", code[control_register].parameter1);
    } else if (num[code[control_register].command] == 2) {
        printf(" %d %d", code[control_register].parameter1, code[control_register].parameter2);
    }
    puts("");
    
    printf("data stack : size %d\n", data_stack.size());
    for (int i = data_stack.size() - 1; i >= max(0, (int)data_stack.size() - 10); i--) {
        print_tag(data_stack[i]);
        puts("");
    }
    if (data_stack.size() > 10) puts("...");
    
    printf("control stack : size %d\n", control_stack.size());
    for (int i = control_stack.size() - 1; i >= max(0, (int)control_stack.size() - 10); i--) {
        print_tag(control_stack[i]);
        puts("");
    }
    if (control_stack.size() > 10) puts("...");
    
    printf("environment frame chain : size %d\n", environment_frame_register);
    for (int i = environment_frame_register - 1; i >= max(0, environment_frame_register - 10); i--) {
        if (frame_chain_tag[i] == TAG_DUM) {
            printf("[TAG_DUM(%d)]\n", environment_frame_chain[i].size());
        } else if (environment_frame_chain[i].size() == 0) {
            printf("[size 0 frame]\n");
        } else {
            for (int j = 0; j < min((int)environment_frame_chain[i].size(), 10); j++) {
                if (j > 0) putchar(' ');
                putchar('[');
                print_tag(environment_frame_chain[i][j]);
                putchar(']');
            }
            if (environment_frame_chain[i].size() > 10) puts("...");
            puts("");
        }
    }
    if (environment_frame_register > 10) puts("...");
    
    puts("");
    fflush(stdout);
}

void error(string inst, string message) {
    fprintf(stderr, "Error %s.\n", inst.c_str());
    fprintf(stderr, "comment: %s\n", message.c_str());
}

int main(int argc, char *argv[])
{
    bool debug = false;
    char s[1000];
    map <string, Command> to_command;
    
    if (argc == 2 && strcmp(argv[1], "-d") == 0) debug = true;
    
    to_command["LDC"] = LDC;
    to_command["LD"] = LD;
    to_command["ADD"] = ADD;
    to_command["SUB"] = SUB;
    to_command["MUL"] = MUL;
    to_command["DIV"] = DIV;
    to_command["CEQ"] = CEQ;
    to_command["CGT"] = CGT;
    to_command["CGTE"] = CGTE;
    to_command["ATOM"] = ATOM;
    to_command["CONS"] = CONS;
    to_command["CAR"] = CAR;
    to_command["CDR"] = CDR;
    to_command["SEL"] = SEL;
    to_command["JOIN"] = JOIN;
    to_command["LDF"] = LDF;
    to_command["AP"] = AP;
    to_command["RTN"] = RTN;
    to_command["DUM"] = DUM;
    to_command["RAP"] = RAP;
    to_command["STOP"] = STOP;
    to_command["TSEL"] = TSEL;
    to_command["TAP"] = TAP;
    to_command["TRAP"] = TRAP;
    to_command["ST"] = ST;
    to_command["DBUG"] = DBUG;
    to_command["BRK"] = BRK;
    to_string[LDC] = "LDC";
    to_string[LD] = "LD";
    to_string[ADD] = "ADD";
    to_string[SUB] = "SUB";
    to_string[MUL] = "MUL";
    to_string[DIV] = "DIV";
    to_string[CEQ] = "CEQ";
    to_string[CGT] = "CGT";
    to_string[CGTE] = "CGTE";
    to_string[ATOM] = "ATOM";
    to_string[CONS] = "CONS";
    to_string[CAR] = "CAR";
    to_string[CDR] = "CDR";
    to_string[SEL] = "SEL";
    to_string[JOIN] = "JOIN";
    to_string[LDF] = "LDF";
    to_string[AP] = "AP";
    to_string[RTN] = "RTN";
    to_string[DUM] = "DUM";
    to_string[RAP] = "RAP";
    to_string[STOP] = "STOP";
    to_string[TSEL] = "TSEL";
    to_string[TAP] = "TAP";
    to_string[TRAP] = "TRAP";
    to_string[ST] = "ST";
    to_string[DBUG] = "DBUG";
    to_string[BRK] = "BRK";
    
    while (1) {
        Instruction i;
        
        if (scanf("%s", s) == EOF) break;
        
        if (s[0] == ';') continue;
        
        if (!to_command.count((string)s)) {
            fprintf(stderr, "No instruction \"%s\"\n", s);
            
            return 0;
        }
        
        i.command = to_command[(string)s];
        
        if (num[i.command] == 1) {
            scanf("%d", &i.parameter1);
        } else if (num[i.command] == 2) {
            scanf("%d %d", &i.parameter1, &i.parameter2);
        }
        
        fgets(s, 1000, stdin);
        
        code.push_back(i);
        comment.push_back(string(s));
    }
    
    if (code.size() > 1045876) {
        fprintf(stderr, "instructions is too long : %d instructions\n", code.size());
        
        return 0;
    }
    
    control_stack.push_back((Data){TAG_STOP, 0, 0, 0});
    
    if (debug) print();
    
    while (1) {
        if (control_register < 0 || control_register >= code.size()) {
            fprintf(stderr, "Address error\n");
            
            return 0;
        }
        
        switch (code[control_register].command) {
            case LDC :
            {
                int n = code[control_register].parameter1;
                
                data_stack.push_back((Data){TAG_INT, n, 0, 0});
                control_register++;
                
                break;
            }
            case LD :
            {
                int n = code[control_register].parameter1;
                int i = code[control_register].parameter2;
                int fp = environment_frame_register - n - 1;
                
                if (fp < 0 || fp >= environment_frame_chain.size() || frame_chain_tag[fp] == TAG_DUM || i >= environment_frame_chain[fp].size()) {
                    error("LD", comment[control_register]);                    
                    return 0;
                }
                
                data_stack.push_back(environment_frame_chain[fp][i]);
                control_register++;
                
                break;
            }
            case ADD :
            {
                Data x, y;
                
                y = data_stack.back();
                data_stack.pop_back();
                x = data_stack.back();
                data_stack.pop_back();
                
                if (x.tag != TAG_INT || y.tag != TAG_INT) {
                    error("ADD", comment[control_register]);
                    return 0;
                }
                
                data_stack.push_back((Data){TAG_INT, x.data + y.data, 0, 0});
                control_register++;
                
                break;
            }
            case SUB :
            {
                Data x, y;
                
                y = data_stack.back();
                data_stack.pop_back();
                x = data_stack.back();
                data_stack.pop_back();
                
                if (x.tag != TAG_INT || y.tag != TAG_INT) {
                    error("SUB", comment[control_register]);
                    return 0;
                }
                
                data_stack.push_back((Data){TAG_INT, x.data - y.data, 0, 0});
                control_register++;
                
                break;
            }
            case MUL :
            {
                Data x, y;
                
                y = data_stack.back();
                data_stack.pop_back();
                x = data_stack.back();
                data_stack.pop_back();
                
                if (x.tag != TAG_INT || y.tag != TAG_INT) {
                    error("MUL", comment[control_register]);
                    return 0;
                }
                
                data_stack.push_back((Data){TAG_INT, x.data * y.data, 0, 0});
                control_register++;
                
                break;
            }
            case DIV :
            {
                int f = 0;
                Data x, y;
                
                y = data_stack.back();
                data_stack.pop_back();
                x = data_stack.back();
                data_stack.pop_back();
                
                if (x.tag != TAG_INT || y.tag != TAG_INT) {
                    error("DIV", comment[control_register]);
                    return 0;
                }
                
                if ((long long)x.data * y.data < 0 && abs(x.data) % abs(y.data) != 0) f = 1;
                
                data_stack.push_back((Data){TAG_INT, x.data / y.data - f, 0, 0});
                control_register++;
                
                break;
            }
            case CEQ :
            {
                Data x, y;
                
                y = data_stack.back();
                data_stack.pop_back();
                x = data_stack.back();
                data_stack.pop_back();
                
                if (x.tag != TAG_INT || y.tag != TAG_INT) {
                    error("CEQ", comment[control_register]);
                    return 0;
                }
                
                if (x.data == y.data) {
                    data_stack.push_back((Data){TAG_INT, 1, 0, 0});
                } else {
                    data_stack.push_back((Data){TAG_INT, 0, 0, 0});
                }
                control_register++;
                
                break;
            }
            case CGT :
            {
                Data x, y;
                
                y = data_stack.back();
                data_stack.pop_back();
                x = data_stack.back();
                data_stack.pop_back();
                
                if (x.tag != TAG_INT || y.tag != TAG_INT) {
                    error("CGT", comment[control_register]);
                    return 0;
                }
                
                if (x.data > y.data) {
                    data_stack.push_back((Data){TAG_INT, 1, 0, 0});
                } else {
                    data_stack.push_back((Data){TAG_INT, 0, 0, 0});
                }
                control_register++;
                
                break;
            }
            case CGTE :
            {
                Data x, y;
                
                y = data_stack.back();
                data_stack.pop_back();
                x = data_stack.back();
                data_stack.pop_back();
                
                if (x.tag != TAG_INT || y.tag != TAG_INT) {
                    error("CGTE", comment[control_register]);
                    return 0;
                }
                
                if (x.data >= y.data) {
                    data_stack.push_back((Data){TAG_INT, 1, 0, 0});
                } else {
                    data_stack.push_back((Data){TAG_INT, 0, 0, 0});
                }
                control_register++;
                
                break;
            }
            case ATOM :
            {
                Data x;
                
                x = data_stack.back();
                data_stack.pop_back();
                
                if (x.tag == TAG_INT) {
                    data_stack.push_back((Data){TAG_INT, 1, 0, 0});
                } else {
                    data_stack.push_back((Data){TAG_INT, 0, 0, 0});
                }
                control_register++;
                
                break;
            }
            case CONS :
            {
                Data x, y;
                
                y = data_stack.back();
                data_stack.pop_back();
                x = data_stack.back();
                data_stack.pop_back();
                
                data_heap.push_back(x);
                data_heap.push_back(y);
                
                data_stack.push_back((Data){TAG_CONS, 0, data_heap.size() - 2, data_heap.size() - 1});
                control_register++;
                
                break;
            }
            case CAR :
            {
                Data x;
                
                x = data_stack.back();
                data_stack.pop_back();
                
                if (x.tag != TAG_CONS) {
                    error("CAR", comment[control_register]);
                    return 0;
                }
                
                data_stack.push_back(data_heap[x.first]);
                control_register++;
                
                break;
            }
            case CDR :
            {
                Data x;
                
                x = data_stack.back();
                data_stack.pop_back();
                
                if (x.tag != TAG_CONS) {
                    error("CDR", comment[control_register]);
                    return 0;
                }
                
                data_stack.push_back(data_heap[x.second]);
                control_register++;
                
                break;
            }
            case SEL :
            {
                int t = code[control_register].parameter1;
                int f = code[control_register].parameter2;
                Data x;
                
                x = data_stack.back();
                data_stack.pop_back();
                
                if (x.tag != TAG_INT) {
                    error("SEL", comment[control_register]);
                    return 0;
                }
                
                control_stack.push_back((Data){TAG_JOIN, control_register + 1, 0, 0});
                if (x.data == 0) {
                    control_register = f;
                } else {
                    control_register = t;
                }
                
                break;
            }
            case JOIN :
            {
                Data x;
                
                x = control_stack.back();
                control_stack.pop_back();
                
                if (x.tag != TAG_JOIN) {
                    error("JOIN", comment[control_register]);
                    return 0;
                }
                
                control_register = x.data;
                
                break;
            }
            case LDF :
            {
                int f = code[control_register].parameter1;
                
                data_stack.push_back((Data){TAG_CLOSURE, 0, f, environment_frame_register});
                control_register++;
                
                break;
            }
            case AP :
            {
                int n = code[control_register].parameter1;
                Data x;
                
                x = data_stack.back();
                data_stack.pop_back();
                
                if (x.tag != TAG_CLOSURE) {
                    error("AP", comment[control_register]);
                    return 0;
                }
                
                while (environment_frame_chain.size() > x.second) environment_frame_chain.pop_back();
                while (frame_chain_tag.size() > x.second) frame_chain_tag.pop_back();
                environment_frame_chain.push_back(vector<Data>(n));
                frame_chain_tag.push_back(TAG_NONE);
                n--;
                while (n >= 0) {
                    Data y = data_stack.back();
                    data_stack.pop_back();
                    environment_frame_chain[x.second][n] = y;
                    n--;
                }
                control_stack.push_back((Data){TAG_NONE, environment_frame_register, 0, 0});
                control_stack.push_back((Data){TAG_RET, control_register + 1, 0, 0});
                environment_frame_register = x.second + 1;
                control_register = x.first;
                
                break;
            }
            case RTN :
            {
                Data x, y;
                
                x = control_stack.back();
                control_stack.pop_back();
                
                if (x.tag == TAG_STOP) {
                    fprintf(stderr, "%d instructions\n", counter);
                    fprintf(stderr, "MACHINE STOP\n");
                    
                    return 0;
                }
                
                if (x.tag != TAG_RET) {
                    error("RTN", comment[control_register]);
                    return 0;
                }
                
                y = control_stack.back();
                control_stack.pop_back();
                environment_frame_register = y.data;
                control_register = x.data;
                
                break;
            }
            case DUM :
            {
                int n = code[control_register].parameter1;
                
                while (environment_frame_chain.size() > environment_frame_register) environment_frame_chain.pop_back();
                while (frame_chain_tag.size() > environment_frame_register) frame_chain_tag.pop_back();
                environment_frame_chain.push_back(vector<Data>(n));
                frame_chain_tag.push_back(TAG_DUM);
                environment_frame_register++;
                control_register++;
                
                break;
            }
            case RAP :
            {
                int n = code[control_register].parameter1;
                Data x;
                
                x = data_stack.back();
                data_stack.pop_back();
                
                if (x.tag != TAG_CLOSURE || frame_chain_tag[environment_frame_register - 1] != TAG_DUM || environment_frame_chain[environment_frame_register - 1].size() != n) {
                    error("RAP", comment[control_register]);
                    return 0;
                }
                
                n--;
                while (n >= 0) {
                    Data y = data_stack.back();
                    data_stack.pop_back();
                    environment_frame_chain[x.second - 1][n] = y;
                    n--;
                }
                frame_chain_tag[environment_frame_register - 1] = TAG_NONE;
                control_stack.push_back((Data){TAG_NONE, environment_frame_register - 1, 0, 0});
                control_stack.push_back((Data){TAG_RET, control_register + 1, 0, 0});
                environment_frame_register = x.second;
                control_register = x.first;
                
                break;
            }
            case STOP :
            {
                fprintf(stderr, "%d instructions\n", counter);
                fprintf(stderr, "MACHINE STOP\n");
                
                return 0;
            }
            case TSEL :
            {
                int t = code[control_register].parameter1;
                int f = code[control_register].parameter2;
                Data x;
                
                x = data_stack.back();
                data_stack.pop_back();
                
                if (x.tag != TAG_INT) {
                    error("TSEL", comment[control_register]);
                    return 0;
                }
                
                if (x.data == 0) {
                    control_register = f;
                } else {
                    control_register = t;
                }
                
                break;
            }
            case TAP :
            {
                int n = code[control_register].parameter1;
                Data x;
                
                x = data_stack.back();
                data_stack.pop_back();
                
                if (x.tag != TAG_CLOSURE) {
                    error("TAP", comment[control_register]);
                    return 0;
                }
                
                while (environment_frame_chain.size() > x.second) environment_frame_chain.pop_back();
                while (frame_chain_tag.size() > x.second) frame_chain_tag.pop_back();
                environment_frame_chain.push_back(vector<Data>(n));
                frame_chain_tag.push_back(TAG_NONE);
                n--;
                while (n >= 0) {
                    Data y = data_stack.back();
                    data_stack.pop_back();
                    environment_frame_chain[x.second][n] = y;
                    n--;
                }
                environment_frame_register = x.second + 1;
                control_register = x.first;
                
                break;
            }
            case TRAP :
            {
                int n = code[control_register].parameter1;
                Data x;
                
                x = data_stack.back();
                data_stack.pop_back();
                
                if (x.tag != TAG_CLOSURE || frame_chain_tag[environment_frame_register - 1] != TAG_DUM || environment_frame_chain[environment_frame_register - 1].size() != n) {
                    error("TRAP", comment[control_register]);
                    return 0;
                }
                
                n--;
                while (n >= 0) {
                    Data y = data_stack.back();
                    data_stack.pop_back();
                    environment_frame_chain[x.second - 1][n] = y;
                    n--;
                }
                frame_chain_tag[environment_frame_register - 1] = TAG_NONE;
                environment_frame_register = x.second;
                control_register = x.first;
                
                break;
            }
            case ST :
            {
                int n = code[control_register].parameter1;
                int i = code[control_register].parameter2;
                int fp = environment_frame_register - n - 1;
                Data v;
                
                if (fp < 0 || fp >= environment_frame_chain.size() || frame_chain_tag[fp] == TAG_DUM || i >= environment_frame_chain[fp].size()) {
                    error("ST", comment[control_register]);
                    return 0;
                }
                
                v = data_stack.back();
                data_stack.pop_back();
                environment_frame_chain[fp][i] = v;
                control_register++;
                
                break;
            }
            case DBUG :
            {
                Data x;
                
                x = data_stack.back();
                data_stack.pop_back();
                print_tag(x);
                puts("");
                control_register++;
                
                break;
            }
            case BRK :
            {
                control_register++;
                
                break;
            }
        }
        
        counter++;
        
        if (debug) print();
        
        if (counter == 3072000) {
            fprintf(stderr, "cycle limit\n");
            
            return 0;
        }
    }
    
    return 0;
}
