#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <vector>
#include <string>
#include <map>
#include <algorithm>

using namespace std;

enum Tag {
    TAG_INT,
    TAG_CONS,
    TAG_JOIN,
    TAG_CLOSURE,
    TAG_DUM,
    TAG_RET,
    TAG_STOP,
    TAG_NONE
};

struct Data {
    Tag tag;
    int data;
    int first;
    int second;
};

enum Command {
    LDC,
    LD,
    ADD,
    SUB,
    MUL,
    DIV,
    CEQ,
    CGT,
    CGTE,
    ATOM,
    CONS,
    CAR,
    CDR,
    SEL,
    JOIN,
    LDF,
    AP,
    RTN,
    DUM,
    RAP,
    STOP,
    TSEL,
    TAP,
    TRAP,
    ST,
    DBUG,
    BRK
};

struct Instruction {
    Command command;
    int parameter1;
    int parameter2;
};

struct LambdaMan {
    int vitality;
    int x;
    int y;
    int direction;
    int lives;
    int score;
};

struct Ghost {
    int vitality;
    int x;
    int y;
    int direction;
};

int counter;
int control_register;
int environment_frame_register;
Data state;
Data AIfunc;
vector <Data> data_stack;
vector <Data> control_stack;
vector <vector <Data> > environment_frame_chain;
vector <Tag> frame_chain_tag;
vector <Data> data_heap;
vector <Instruction> code;
vector <string> comment;
int num[27] = {1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 1, 1, 0, 1, 1, 0, 2, 1, 1, 2, 0, 0};
map <Command, string> to_string;

void print_tag(Data &data);

void print_int(Data &data)
{
    fprintf(stderr, "%d", data.data);
}

void print_cons(Data &data)
{
    fprintf(stderr, "(");
    print_tag(data_heap[data.first]);
    fprintf(stderr, ",");
    print_tag(data_heap[data.second]);
    fprintf(stderr, ")");
}

void print_join(Data &data)
{
    fprintf(stderr, "TAG_JOIN %d", data.data);
}

void print_closure(Data &data)
{
    fprintf(stderr, "{%d <env %d>}", data.first, data.second);
}

void print_ret(Data &data)
{
    fprintf(stderr, "TAG_RET %d", data.data);
}

void print_stop(Data &data)
{
    fprintf(stderr, "TAG_STOP");
}

void print_none(Data &data)
{
    fprintf(stderr, "<env %d>", data.data);
}

void print_tag(Data &data)
{
    switch (data.tag) {
        case TAG_INT :
        {
            print_int(data);
            break;
        }
        case TAG_CONS :
        {
            print_cons(data);
            break;
        }
        case TAG_JOIN :
        {
            print_join(data);
            break;
        }
        case TAG_CLOSURE :
        {
            print_closure(data);
            break;
        }
        case TAG_RET :
        {
            print_ret(data);
            break;
        }
        case TAG_STOP :
        {
            print_stop(data);
            break;
        }
        case TAG_NONE :
        {
            print_none(data);
            break;
        }
    }
}

void print(void)
{
    fprintf(stderr, "number of command : %d\n", counter);
    fprintf(stderr, "control register : %d\n", control_register);
    fprintf(stderr, "next instruction : %s", to_string[code[control_register].command].c_str());
    if (num[code[control_register].command] == 1) {
        fprintf(stderr, " %d", code[control_register].parameter1);
    } else if (num[code[control_register].command] == 2) {
        fprintf(stderr, " %d %d", code[control_register].parameter1, code[control_register].parameter2);
    }
    fprintf(stderr, "\n");
    
    fprintf(stderr, "data stack : size %d\n", data_stack.size());
    for (int i = data_stack.size() - 1; i >= max(0, (int)data_stack.size() - 10); i--) {
        print_tag(data_stack[i]);
        fprintf(stderr, "\n");
    }
    if (data_stack.size() > 10) fprintf(stderr, "...\n");
    
    fprintf(stderr, "control stack : size %d\n", control_stack.size());
    for (int i = control_stack.size() - 1; i >= max(0, (int)control_stack.size() - 10); i--) {
        print_tag(control_stack[i]);
        fprintf(stderr, "\n");
    }
    if (control_stack.size() > 10) fprintf(stderr, "...\n");
    
    fprintf(stderr, "environment frame chain : size %d\n", environment_frame_register);
    for (int i = environment_frame_register - 1; i >= max(0, environment_frame_register - 10); i--) {
        if (frame_chain_tag[i] == TAG_DUM) {
            fprintf(stderr, "[TAG_DUM(%d)]\n", environment_frame_chain[i].size());
        } else if (environment_frame_chain[i].size() == 0) {
            fprintf(stderr, "[size 0 frame]\n");
        } else {
            for (int j = 0; j < min((int)environment_frame_chain[i].size(), 10); j++) {
                if (j > 0) fprintf(stderr, " ");
                fprintf(stderr, "[");
                print_tag(environment_frame_chain[i][j]);
                fprintf(stderr, "]");
            }
            if (environment_frame_chain[i].size() > 10) fprintf(stderr, "...");
            fprintf(stderr, "\n");
        }
    }
    if (environment_frame_register > 10) fprintf(stderr, "...\n");
    
    fprintf(stderr, "\n");
    fflush(stderr);
}

Data make_cons(const Data &data1, const Data &data2)
{
    data_heap.push_back(data1);
    data_heap.push_back(data2);
    return (Data){TAG_CONS, 0, data_heap.size() - 2, data_heap.size() - 1};
}

void input(bool start)
{
    int h, w, n, fruit;
    char board[256][300];
    LambdaMan l;
    vector <Ghost> gs;
    Data world, tmp;
    
    if (scanf("%d %d%*c", &h, &w) == EOF) exit(0);
    
    for (int i = 0; i < h; i++) fgets(board[i], 300, stdin);
    
    scanf("%d %d %d %d %d %d", &l.vitality, &l.y, &l.x, &l.direction, &l.lives, &l.score);
    
    scanf("%d", &n);
    
    for (int i = 0; i < n; i++) {
        Ghost g;
        
        scanf("%d %d %d %d", &g.vitality, &g.y, &g.x, &g.direction);
        
        gs.push_back(g);
    }
    
    scanf("%d", &fruit);
    
    world = (Data){TAG_INT, fruit, 0, 0};
    
    tmp = (Data){TAG_INT, 0, 0, 0};
    
    for (int i = n - 1; i >= 0; i--) {
        Data tmp2;
        
        tmp2 = (Data){TAG_INT, gs[i].direction, 0, 0};
        tmp2 = make_cons(make_cons((Data){TAG_INT, gs[i].x, 0, 0}, (Data){TAG_INT, gs[i].y, 0, 0}), tmp2);
        tmp2 = make_cons((Data){TAG_INT, gs[i].vitality, 0, 0}, tmp2);
        tmp = make_cons(tmp2, tmp);
    }
    
    world = make_cons(tmp, world);
    
    tmp = (Data){TAG_INT, l.score, 0, 0};
    tmp = make_cons((Data){TAG_INT, l.lives, 0, 0}, tmp);
    tmp = make_cons((Data){TAG_INT, l.direction, 0, 0}, tmp);
    tmp = make_cons(make_cons((Data){TAG_INT, l.x, 0, 0}, (Data){TAG_INT, l.y, 0, 0}), tmp);
    tmp = make_cons((Data){TAG_INT, l.vitality, 0, 0}, tmp);
    
    world = make_cons(tmp, world);
    
    tmp = (Data){TAG_INT, 0, 0, 0};
    
    for (int i = h - 1; i >= 0; i--) {
        Data tmp2 = (Data){TAG_INT, 0, 0, 0};
        
        for (int j = w - 1; j >= 0; j--) {
            int st;
            
            if (board[i][j] == '#') {
                st = 0;
            } else if (board[i][j] == ' ') {
                st = 1;
            } else if (board[i][j] == '.') {
                st = 2;
            } else if (board[i][j] == 'o') {
                st = 3;
            } else if (board[i][j] == '%') {
                st = 4;
            } else if (board[i][j] == '\\') {
                st = 5;
            } else if (board[i][j] == '=') {
                st = 6;
            }
            
            tmp2 = make_cons((Data){TAG_INT, st, 0, 0}, tmp2);
        }
        
        tmp = make_cons(tmp2, tmp);
    }
    
    world = make_cons(tmp, world);
    
    while (environment_frame_chain.size() > environment_frame_register) environment_frame_chain.pop_back();
    while (frame_chain_tag.size() > environment_frame_register) frame_chain_tag.pop_back();
    environment_frame_chain.push_back(vector <Data>(2));
    frame_chain_tag.push_back(TAG_NONE);
    if (start) {
        environment_frame_chain[environment_frame_register][0] = world;
        environment_frame_chain[environment_frame_register][1] = (Data){TAG_INT, 0, 0, 0};
    } else {
        environment_frame_chain[environment_frame_register][0] = state;
        environment_frame_chain[environment_frame_register][1] = world;
    }
    environment_frame_register++;
}

void error(string inst, string message) {
    fprintf(stderr, "Error %s.\n", inst.c_str());
    fprintf(stderr, "comment: %s\n", message.c_str());
}

int main(int argc, char *argv[])
{
    bool debug = false;
    bool start = true;
    string file_name = "";
    FILE *fp;
    char s[1000];
    map <string, Command> to_command;
    
    for (int i = 1; i < argc; i++) {
        if (strcmp(argv[i], "-d") == 0) {
            debug = true;
        } else {
            file_name = (string)argv[i];
        }
    }
    
    to_command["LDC"] = LDC;
    to_command["LD"] = LD;
    to_command["ADD"] = ADD;
    to_command["SUB"] = SUB;
    to_command["MUL"] = MUL;
    to_command["DIV"] = DIV;
    to_command["CEQ"] = CEQ;
    to_command["CGT"] = CGT;
    to_command["CGTE"] = CGTE;
    to_command["ATOM"] = ATOM;
    to_command["CONS"] = CONS;
    to_command["CAR"] = CAR;
    to_command["CDR"] = CDR;
    to_command["SEL"] = SEL;
    to_command["JOIN"] = JOIN;
    to_command["LDF"] = LDF;
    to_command["AP"] = AP;
    to_command["RTN"] = RTN;
    to_command["DUM"] = DUM;
    to_command["RAP"] = RAP;
    to_command["STOP"] = STOP;
    to_command["TSEL"] = TSEL;
    to_command["TAP"] = TAP;
    to_command["TRAP"] = TRAP;
    to_command["ST"] = ST;
    to_command["DBUG"] = DBUG;
    to_command["BRK"] = BRK;
    to_string[LDC] = "LDC";
    to_string[LD] = "LD";
    to_string[ADD] = "ADD";
    to_string[SUB] = "SUB";
    to_string[MUL] = "MUL";
    to_string[DIV] = "DIV";
    to_string[CEQ] = "CEQ";
    to_string[CGT] = "CGT";
    to_string[CGTE] = "CGTE";
    to_string[ATOM] = "ATOM";
    to_string[CONS] = "CONS";
    to_string[CAR] = "CAR";
    to_string[CDR] = "CDR";
    to_string[SEL] = "SEL";
    to_string[JOIN] = "JOIN";
    to_string[LDF] = "LDF";
    to_string[AP] = "AP";
    to_string[RTN] = "RTN";
    to_string[DUM] = "DUM";
    to_string[RAP] = "RAP";
    to_string[STOP] = "STOP";
    to_string[TSEL] = "TSEL";
    to_string[TAP] = "TAP";
    to_string[TRAP] = "TRAP";
    to_string[ST] = "ST";
    to_string[DBUG] = "DBUG";
    to_string[BRK] = "BRK";
    
    if ((fp = fopen(file_name.c_str(), "r")) == NULL) {
        fprintf(stderr, "Can't open file \"%s\"\n", file_name.c_str());
        
        return 0;
    }
    
    while (1) {
        Instruction i;
        
        if (fscanf(fp, "%s", s) == EOF) break;
        
        if (s[0] == ';') continue;
        
        if (!to_command.count((string)s)) {
            fprintf(stderr, "No instruction \"%s\"\n", s);
            
            return 0;
        }
        
        i.command = to_command[(string)s];
        
        if (num[i.command] == 1) {
            fscanf(fp, "%d", &i.parameter1);
        } else if (num[i.command] == 2) {
            fscanf(fp, "%d %d", &i.parameter1, &i.parameter2);
        }
        
        fgets(s, 1000, fp);
        
        code.push_back(i);
        comment.push_back(string(s));
    }
    
    fclose(fp);
    
    if (code.size() > 1045876) {
        fprintf(stderr, "instructions is too long : %d instructions\n", code.size());
        
        return 0;
    }
    
    while (1) {
        if (!start) {
            control_register = AIfunc.first;
            environment_frame_register = AIfunc.second;
        }
        
        input(start);
        
        data_stack.clear();
        control_stack.clear();
        control_stack.push_back((Data){TAG_STOP, 0, 0, 0});
        
        counter = 0;
        
        if (debug) print();
        
        while (1) {
            if (control_register < 0 || control_register >= code.size()) {
                fprintf(stderr, "Address error %d\n", control_register);
                
                return 0;
            }
            
            switch (code[control_register].command) {
                case LDC :
                {
                    int n = code[control_register].parameter1;
                    
                    data_stack.push_back((Data){TAG_INT, n, 0, 0});
                    control_register++;
                    
                    break;
                }
                case LD :
                {
                    int n = code[control_register].parameter1;
                    int i = code[control_register].parameter2;
                    int fp = environment_frame_register - n - 1;
                    
                    if (fp < 0 || fp >= environment_frame_chain.size() || frame_chain_tag[fp] == TAG_DUM || i >= environment_frame_chain[fp].size()) {
                        error("LD", comment[control_register]);
                        return 0;
                    }
                    
                    data_stack.push_back(environment_frame_chain[fp][i]);
                    control_register++;
                    
                    break;
                }
                case ADD :
                {
                    Data x, y;
                    
                    y = data_stack.back();
                    data_stack.pop_back();
                    x = data_stack.back();
                    data_stack.pop_back();
                    
                    if (x.tag != TAG_INT || y.tag != TAG_INT) {
                        error("ADD", comment[control_register]);
                        return 0;
                    }
                    
                    data_stack.push_back((Data){TAG_INT, x.data + y.data, 0, 0});
                    control_register++;
                    
                    break;
                }
                case SUB :
                {
                    Data x, y;
                    
                    y = data_stack.back();
                    data_stack.pop_back();
                    x = data_stack.back();
                    data_stack.pop_back();
                    
                    if (x.tag != TAG_INT || y.tag != TAG_INT) {
                        error("SUB", comment[control_register]);
                        return 0;
                    }
                    
                    data_stack.push_back((Data){TAG_INT, x.data - y.data, 0, 0});
                    control_register++;
                    
                    break;
                }
                case MUL :
                {
                    Data x, y;
                    
                    y = data_stack.back();
                    data_stack.pop_back();
                    x = data_stack.back();
                    data_stack.pop_back();
                    
                    if (x.tag != TAG_INT || y.tag != TAG_INT) {
                        error("MUL", comment[control_register]);
                        return 0;
                    }
                    
                    data_stack.push_back((Data){TAG_INT, x.data * y.data, 0, 0});
                    control_register++;
                    
                    break;
                }
                case DIV :
                {
                    int f = 0;
                    Data x, y;
                    
                    y = data_stack.back();
                    data_stack.pop_back();
                    x = data_stack.back();
                    data_stack.pop_back();
                    
                    if (x.tag != TAG_INT || y.tag != TAG_INT) {
                        error("DIV", comment[control_register]);
                        return 0;
                    }
                    
                    if ((long long)x.data * y.data < 0 && abs(x.data) % abs(y.data) != 0) f = 1;
                    
                    data_stack.push_back((Data){TAG_INT, x.data / y.data - f, 0, 0});
                    control_register++;
                    
                    break;
                }
                case CEQ :
                {
                    Data x, y;
                    
                    y = data_stack.back();
                    data_stack.pop_back();
                    x = data_stack.back();
                    data_stack.pop_back();
                    
                    if (x.tag != TAG_INT || y.tag != TAG_INT) {
                        error("CEQ", comment[control_register]);
                        return 0;
                    }
                    
                    if (x.data == y.data) {
                        data_stack.push_back((Data){TAG_INT, 1, 0, 0});
                    } else {
                        data_stack.push_back((Data){TAG_INT, 0, 0, 0});
                    }
                    control_register++;
                    
                    break;
                }
                case CGT :
                {
                    Data x, y;
                    
                    y = data_stack.back();
                    data_stack.pop_back();
                    x = data_stack.back();
                    data_stack.pop_back();
                    
                    if (x.tag != TAG_INT || y.tag != TAG_INT) {
                        error("CGT", comment[control_register]);
                        return 0;
                    }
                    
                    if (x.data > y.data) {
                        data_stack.push_back((Data){TAG_INT, 1, 0, 0});
                    } else {
                        data_stack.push_back((Data){TAG_INT, 0, 0, 0});
                    }
                    control_register++;
                    
                    break;
                }
                case CGTE :
                {
                    Data x, y;
                    
                    y = data_stack.back();
                    data_stack.pop_back();
                    x = data_stack.back();
                    data_stack.pop_back();
                    
                    if (x.tag != TAG_INT || y.tag != TAG_INT) {
                        error("CGTE", comment[control_register]);
                        return 0;
                    }
                    
                    if (x.data >= y.data) {
                        data_stack.push_back((Data){TAG_INT, 1, 0, 0});
                    } else {
                        data_stack.push_back((Data){TAG_INT, 0, 0, 0});
                    }
                    control_register++;
                    
                    break;
                }
                case ATOM :
                {
                    Data x;
                    
                    x = data_stack.back();
                    data_stack.pop_back();
                    
                    if (x.tag == TAG_INT) {
                        data_stack.push_back((Data){TAG_INT, 1, 0, 0});
                    } else {
                        data_stack.push_back((Data){TAG_INT, 0, 0, 0});
                    }
                    control_register++;
                    
                    break;
                }
                case CONS :
                {
                    Data x, y;
                    
                    y = data_stack.back();
                    data_stack.pop_back();
                    x = data_stack.back();
                    data_stack.pop_back();
                    
                    data_heap.push_back(x);
                    data_heap.push_back(y);
                    
                    data_stack.push_back((Data){TAG_CONS, 0, data_heap.size() - 2, data_heap.size() - 1});
                    control_register++;
                    
                    break;
                }
                case CAR :
                {
                    Data x;
                    
                    x = data_stack.back();
                    data_stack.pop_back();
                    
                    if (x.tag != TAG_CONS) {
                        error("CAR", comment[control_register]);
                        return 0;
                    }
                    
                    data_stack.push_back(data_heap[x.first]);
                    control_register++;
                    
                    break;
                }
                case CDR :
                {
                    Data x;
                    
                    x = data_stack.back();
                    data_stack.pop_back();
                    
                    if (x.tag != TAG_CONS) {
                        error("CDR", comment[control_register]);
                        return 0;
                    }
                    
                    data_stack.push_back(data_heap[x.second]);
                    control_register++;
                    
                    break;
                }
                case SEL :
                {
                    int t = code[control_register].parameter1;
                    int f = code[control_register].parameter2;
                    Data x;
                    
                    x = data_stack.back();
                    data_stack.pop_back();
                    
                    if (x.tag != TAG_INT) {
                        error("SEL", comment[control_register]);
                        return 0;
                    }
                    
                    control_stack.push_back((Data){TAG_JOIN, control_register + 1, 0, 0});
                    if (x.data == 0) {
                        control_register = f;
                    } else {
                        control_register = t;
                    }
                    
                    break;
                }
                case JOIN :
                {
                    Data x;
                    
                    x = control_stack.back();
                    control_stack.pop_back();
                    
                    if (x.tag != TAG_JOIN) {
                        error("JOIN", comment[control_register]);
                        return 0;
                    }
                    
                    control_register = x.data;
                    
                    break;
                }
                case LDF :
                {
                    int f = code[control_register].parameter1;
                    
                    data_stack.push_back((Data){TAG_CLOSURE, 0, f, environment_frame_register});
                    control_register++;
                    
                    break;
                }
                case AP :
                {
                    int n = code[control_register].parameter1;
                    Data x;
                    
                    x = data_stack.back();
                    data_stack.pop_back();
                    
                    if (x.tag != TAG_CLOSURE) {
                        error("AP", comment[control_register]);
                        return 0;
                    }
                    
                    while (environment_frame_chain.size() > x.second) environment_frame_chain.pop_back();
                    while (frame_chain_tag.size() > x.second) frame_chain_tag.pop_back();
                    environment_frame_chain.push_back(vector<Data>(n));
                    frame_chain_tag.push_back(TAG_NONE);
                    n--;
                    while (n >= 0) {
                        Data y = data_stack.back();
                        data_stack.pop_back();
                        environment_frame_chain[x.second][n] = y;
                        n--;
                    }
                    control_stack.push_back((Data){TAG_NONE, environment_frame_register, 0, 0});
                    control_stack.push_back((Data){TAG_RET, control_register + 1, 0, 0});
                    environment_frame_register = x.second + 1;
                    control_register = x.first;
                    
                    break;
                }
                case RTN :
                {
                    Data x, y;
                    
                    x = control_stack.back();
                    control_stack.pop_back();
                    
                    if (x.tag == TAG_STOP) {
                        fprintf(stderr, "%d instructions\n", counter);
                        fprintf(stderr, "MACHINE STOP\n\n");
                        fflush(stderr);
                        
                        if (start) {
                            state = data_heap[data_stack.back().first];
                            AIfunc = data_heap[data_stack.back().second];
                        } else {
                            state = data_heap[data_stack.back().first];
                            
                            printf("%d\n", data_heap[data_stack.back().second].data);
                            fflush(stdout);
                        }
                        
                        goto end;
                    }
                    
                    if (x.tag != TAG_RET) {
                        error("RTN", comment[control_register]);
                        return 0;
                    }
                    
                    y = control_stack.back();
                    control_stack.pop_back();
                    environment_frame_register = y.data;
                    control_register = x.data;
                    
                    break;
                }
                case DUM :
                {
                    int n = code[control_register].parameter1;
                    
                    while (environment_frame_chain.size() > environment_frame_register) environment_frame_chain.pop_back();
                    while (frame_chain_tag.size() > environment_frame_register) frame_chain_tag.pop_back();
                    environment_frame_chain.push_back(vector<Data>(n));
                    frame_chain_tag.push_back(TAG_DUM);
                    environment_frame_register++;
                    control_register++;
                    
                    break;
                }
                case RAP :
                {
                    int n = code[control_register].parameter1;
                    Data x;
                    
                    x = data_stack.back();
                    data_stack.pop_back();
                    
                    if (x.tag != TAG_CLOSURE || frame_chain_tag[environment_frame_register - 1] != TAG_DUM || environment_frame_chain[environment_frame_register - 1].size() != n) {
                        error("RAP", comment[control_register]);
                        return 0;
                    }
                    
                    n--;
                    while (n >= 0) {
                        Data y = data_stack.back();
                        data_stack.pop_back();
                        environment_frame_chain[x.second - 1][n] = y;
                        n--;
                    }
                    frame_chain_tag[environment_frame_register - 1] = TAG_NONE;
                    control_stack.push_back((Data){TAG_NONE, environment_frame_register - 1, 0, 0});
                    control_stack.push_back((Data){TAG_RET, control_register + 1, 0, 0});
                    environment_frame_register = x.second;
                    control_register = x.first;
                    
                    break;
                }
                case STOP :
                {
                    fprintf(stderr, "%d instructions\n", counter);
                    fprintf(stderr, "MACHINE STOP\n");
                    
                    return 0;
                }
                case TSEL :
                {
                    int t = code[control_register].parameter1;
                    int f = code[control_register].parameter2;
                    Data x;
                    
                    x = data_stack.back();
                    data_stack.pop_back();
                    
                    if (x.tag != TAG_INT) {
                        error("TSEL", comment[control_register]);
                        return 0;
                    }
                    
                    if (x.data == 0) {
                        control_register = f;
                    } else {
                        control_register = t;
                    }
                    
                    break;
                }
                case TAP :
                {
                    int n = code[control_register].parameter1;
                    Data x;
                    
                    x = data_stack.back();
                    data_stack.pop_back();
                    
                    if (x.tag != TAG_CLOSURE) {
                        error("TAP", comment[control_register]);
                        return 0;
                    }
                    
                    while (environment_frame_chain.size() > x.second) environment_frame_chain.pop_back();
                    while (frame_chain_tag.size() > x.second) frame_chain_tag.pop_back();
                    environment_frame_chain.push_back(vector<Data>(n));
                    frame_chain_tag.push_back(TAG_NONE);
                    n--;
                    while (n >= 0) {
                        Data y = data_stack.back();
                        data_stack.pop_back();
                        environment_frame_chain[x.second][n] = y;
                        n--;
                    }
                    environment_frame_register = x.second + 1;
                    control_register = x.first;
                    
                    break;
                }
                case TRAP :
                {
                    int n = code[control_register].parameter1;
                    Data x;
                    
                    x = data_stack.back();
                    data_stack.pop_back();
                    
                    if (x.tag != TAG_CLOSURE || frame_chain_tag[environment_frame_register - 1] != TAG_DUM || environment_frame_chain[environment_frame_register - 1].size() != n) {
                        error("TRAP", comment[control_register]);
                        return 0;
                    }
                    
                    n--;
                    while (n >= 0) {
                        Data y = data_stack.back();
                        data_stack.pop_back();
                        environment_frame_chain[x.second - 1][n] = y;
                        n--;
                    }
                    frame_chain_tag[environment_frame_register - 1] = TAG_NONE;
                    environment_frame_register = x.second;
                    control_register = x.first;
                    
                    break;
                }
                case ST :
                {
                    int n = code[control_register].parameter1;
                    int i = code[control_register].parameter2;
                    int fp = environment_frame_register - n - 1;
                    Data v;
                    
                    if (fp < 0 || fp >= environment_frame_chain.size() || frame_chain_tag[fp] == TAG_DUM || i >= environment_frame_chain[fp].size()) {
                        error("ST", comment[control_register]);
                        return 0;
                    }
                    
                    v = data_stack.back();
                    data_stack.pop_back();
                    environment_frame_chain[fp][i] = v;
                    control_register++;
                    
                    break;
                }
                case DBUG :
                {
                    Data x;
                    
                    x = data_stack.back();
                    data_stack.pop_back();
                    print_tag(x);
                    fprintf(stderr, "\n");
                    fflush(stderr);
                    control_register++;
                    
                    break;
                }
                case BRK :
                {
                    control_register++;
                    
                    break;
                }
            }
            
            counter++;
            
            if (debug) print();
            
            if (counter == 3072000) {
                fprintf(stderr, "cycle limit\n");
                
                return 0;
            }
        }
        
        end:
        
        start = false;
    }
    
    return 0;
}
