#! /usr/bin/env python

import logging

register_names = list('ABCDEFGH') + ['PC']
register_to_index = dict((name, i) for i, name in enumerate(register_names))

mnemonic_names = ['MOV', 'INC', 'DEC', 'ADD', 'SUB', 'MUL', 'DIV', 'AND', 'OR', 'XOR', 'JLT', 'JEQ', 'JGT', 'INT', 'HLT']
mnemonic_arity = [2, 1, 1, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 1, 0]

class Argument:
    def __init__(self, phrase):
        self.reference = phrase.startswith('[') and phrase.endswith(']')
        if self.reference: phrase = phrase[1: -1].strip()
        self.register = phrase in register_names
        if self.register:
            self.value = register_to_index[phrase]
        else:
            self.value = int(phrase)
        if self.register and self.reference:
            assert self.value != 8, "register indirect access with PC is not allowed."

    def __str__(self):
        if self.register:
            name = register_names[self.value]
        else:
            name = self.value
        if self.reference:
            return "[{0}]".format(name)
        else:
            return "{0}".format(name)

class Instruction:
    def __init__(self, line):
        line = line.upper()
        if line == 'HLT':
            mnemonic, args = line, ''
        else:
            mnemonic, args = line.split(' ', 1)
        assert mnemonic in mnemonic_names, "invalid mnemonic"
        self.mnemonic = mnemonic.strip()
        self.args = [Argument(arg.strip()) for arg in args.strip().split(',') if arg.strip()]

    def __str__(self):
        return "{0} {1}".format(self.mnemonic, ', '.join(str(arg) for arg in self.args))
        
class GHC:
    def __init__(self, source_or_file, game = None, me = None):
        self.game = game
        self.me = me
        if type(source_or_file) == file:
            self.code = source_or_file.read()
        else:
            self.code = source_or_file.strip()
        
        lines = self.code.split('\n')
        self.cm = []
        for line in lines:
            line = line.strip()
            if line.find(';') >= 0: line = line[:line.find(';')].strip()
            if len(line) > 0: self.cm.append(Instruction(line))

        assert len(self.cm) <= 256, "GHC code is too large."
        self.dm = [0] * 256
        self.reg = [0] * 8
        self.clock = 0
        self.pc = 0
        self.result = 4

    def set_value(self, dest_arg, imm):
        imm = (imm + 256) % 256
        if dest_arg.reference and dest_arg.register:
            # register indirect.
            self.dm[self.reg[dest_arg.value]] = imm
        elif not dest_arg.reference and dest_arg.register:
            # register direct
            assert dest_arg.value != 8, "Cannot assign value to PC."
            self.reg[dest_arg.value] = imm
        elif dest_arg.reference and not dest_arg.register:
            # immediate indirect
            self.dm[dest_arg.value] = imm
        else:
            # immediate
            assert False, "immediate cannot be dest argument."

    def get_value(self, src_arg):
        result = src_arg.value
        if src_arg.register: result = self.reg[result]
        if src_arg.reference: result = self.dm[result]
        return result

    def run(self):
        self.clock = 0
        self.pc = 0
        while self.clock < 1024:
            inst = self.cm[self.pc]
            if inst.mnemonic == 'MOV':
                self.set_value(inst.args[0], self.get_value(inst.args[1]))
                self.pc = (self.pc + 1) % 256
            elif inst.mnemonic == 'INC':
                self.set_value(inst.args[0], self.get_value(inst.args[0]) + 1)
                self.pc = (self.pc + 1) % 256
            elif inst.mnemonic == 'DEC':
                self.set_value(inst.args[0], self.get_value(inst.args[0]) - 1)
                self.pc = (self.pc + 1) % 256
            elif inst.mnemonic == 'ADD':
                self.set_value(inst.args[0], self.get_value(inst.args[0]) + self.get_value(inst.args[1]))
                self.pc = (self.pc + 1) % 256
            elif inst.mnemonic == 'SUB':
                self.set_value(inst.args[0], self.get_value(inst.args[0]) - self.get_value(inst.args[1]))
                self.pc = (self.pc + 1) % 256
            elif inst.mnemonic == 'MUL':
                self.set_value(inst.args[0], self.get_value(inst.args[0]) * self.get_value(inst.args[1]))
                self.pc = (self.pc + 1) % 256
            elif inst.mnemonic == 'DIV':
                self.set_value(inst.args[0], self.get_value(inst.args[0]) / self.get_value(inst.args[1]))
                self.pc = (self.pc + 1) % 256
            elif inst.mnemonic == 'AND':
                self.set_value(inst.args[0], self.get_value(inst.args[0]) & self.get_value(inst.args[1]))
                self.pc = (self.pc + 1) % 256
            elif inst.mnemonic == 'OR':
                self.set_value(inst.args[0], self.get_value(inst.args[0]) | self.get_value(inst.args[1]))
                self.pc = (self.pc + 1) % 256
            elif inst.mnemonic == 'XOR':
                self.set_value(inst.args[0], self.get_value(inst.args[0]) ^ self.get_value(inst.args[1]))
                self.pc = (self.pc + 1) % 256
            elif inst.mnemonic == 'JLT':
                assert not inst.args[0].register and not inst.args[0].reference, "Jump destination must be constant."
                if self.get_value(inst.args[1]) < self.get_value(inst.args[2]):
                    self.pc = self.get_value(inst.args[0])
                else:
                    self.pc = (self.pc + 1) % 256
            elif inst.mnemonic == 'JEQ':
                assert not inst.args[0].register and not inst.args[0].reference, "Jump destination must be constant."
                if self.get_value(inst.args[1]) == self.get_value(inst.args[2]):
                    self.pc = self.get_value(inst.args[0])
                else:
                    self.pc = (self.pc + 1) % 256
            elif inst.mnemonic == 'JGT':
                assert not inst.args[0].register and not inst.args[0].reference, "Jump destination must be constant."
                if self.get_value(inst.args[1]) > self.get_value(inst.args[2]):
                    self.pc = self.get_value(inst.args[0])
                else:
                    self.pc = (self.pc + 1) % 256
            elif inst.mnemonic == 'INT':
                if self.get_value(inst.args[0]) == 0:
                    self.result = self.reg[0]
                elif self.get_value(inst.args[0]) == 1:
                    self.reg[0] = self.game.lambdamans[0].pos[0]
                    self.reg[1] = self.game.lambdamans[0].pos[1]
                elif self.get_value(inst.args[0]) == 2:
                    self.reg[0] = self.game.lambdamans[1].pos[0]
                    self.reg[1] = self.game.lambdamans[1].pos[1]
                elif self.get_value(inst.args[0]) == 3:
                    self.reg[0] = self.me.index
                elif self.get_value(inst.args[0]) == 4:
                    index = self.reg[0]
                    self.reg[0] = self.game.ghosts[index].init_pos[0]
                    self.reg[1] = self.game.ghosts[index].init_pos[1]
                elif self.get_value(inst.args[0]) == 5:
                    index = self.reg[0]
                    self.reg[0] = self.game.ghosts[index].pos[0]
                    self.reg[1] = self.game.ghosts[index].pos[1]
                elif self.get_value(inst.args[0]) == 6:
                    index = self.reg[0]
                    self.reg[0] = self.game.ghosts[index].vitality()
                    self.reg[1] = self.game.ghosts[index].direction()
                elif self.get_value(inst.args[0]) == 7:
                    conv = self.game.map_data.char2index
                    self.reg[0] = conv(self.game.map_data.field[self.reg[1]][self.reg[0]])
                elif self.get_value(inst.args[0]) == 8:
                    logging.info("ghost cpu #{id} register dumps".format(id = self.me.index))
                    logging.info("program counter: {0}".format(self.pc))
                    logging.info("general register: {0}".format(self.reg))
                else:
                    assert False, "invalid interruption"
                self.pc = (self.pc + 1) % 256
            elif inst.mnemonic == 'HLT':
                break
            else:
                assert False, "invalid mnemonic"

            self.clock += 1
        return self.result

    def __str__(self):
        return """<GHC STATE> PC = {pc}, CLOCK = {clock}
REGISTERS = {regs}, RETURNS: {ret}
{inst}""".format(ret = self.result, pc = self.pc, clock = self.clock, regs = self.reg, inst = '\n'.join('{ln:3}: {ins}'.format(ln = ln, ins = str(ins) + ('\t\t<- PC' if ln == self.pc else '')) for ln, ins in enumerate(self.cm)))
        
from argparse import ArgumentParser

class DummyMe:
    def __init__(self):
        self.index = 0

if __name__ == '__main__':
    logging.basicConfig(level = logging.DEBUG)

    parser = ArgumentParser()
    parser.add_argument('src', type = file, help = 'ghc file')
    args = parser.parse_args()
    logging.info(str(args))

    cpu = GHC(args.src, None, DummyMe())
    args.src.close()
    cpu.run()
    print cpu
