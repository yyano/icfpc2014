#include<iostream>
#include<vector>
#include<map>
#include<sstream>
#include<set>
#include<algorithm>

using namespace std;

#define rep(i, n) for (int i = 0; i < int(n); ++i)

bool greaterlen(string a, string b) {
  if (a.size() != b.size()) return a.size() > b.size();
  return a < b;
}

int main() {
  vector<string> raw;
  string str;
  int lc = 1000;
  while (getline(cin, str)) {
    int p = str.find(";");
    if (p != (int)string::npos) {
      str = str.substr(0, p);
    }
    while (str != "" && str[str.size() - 1] == ' ') {
      str = str.substr(0, str.size() - 1);
    }
    while (str != "" && str[0] == ' ') {
      str = str.substr(1);
    }
    while (str != "" && str[0] == '\t') {
      str = str.substr(1);
    }
    if (str != "") {
      raw.push_back(str);
    }
  }
  rep (i, raw.size()) {
    if (raw[i].substr(0, 2) == "IF") {
      int cnt = 1;
      for (int j = i + 1; j < (int)raw.size(); ++j) {
        if (raw[j].substr(0, 2) == "IF") ++cnt;
        if (raw[j] == "ENDIF") {
          --cnt;
          if (cnt == 0) {
            stringstream ss;
            ss << lc;
            string label;
            ss >> label;
            label = "_LABEL" + label;
            ++lc;
            vector<string> newraw;
            rep (k, raw.size()) {
              if (k == i) {
                if (raw[i].find("==") != string::npos) {
                  int p = raw[i].find("==");
                  newraw.push_back("jgt " + label + "," + raw[i].substr(3, p - 3) + "," + raw[i].substr(p + 2));
                  newraw.push_back("jlt " + label + "," + raw[i].substr(3, p - 3) + "," + raw[i].substr(p + 2));
                } else if (raw[i].find("!=") != string::npos) {
                  int p = raw[i].find("!=");
                  newraw.push_back("jeq " + label + "," + raw[i].substr(3, p - 3) + "," + raw[i].substr(p + 2));
                } else if (raw[i].find(">=") != string::npos) {
                  int p = raw[i].find(">=");
                  newraw.push_back("jlt " + label + "," + raw[i].substr(3, p - 3) + "," + raw[i].substr(p + 2));
                } else if (raw[i].find("<=") != string::npos) {
                  int p = raw[i].find("<=");
                  newraw.push_back("jgt " + label + "," + raw[i].substr(3, p - 3) + "," + raw[i].substr(p + 2));
                } else if (raw[i].find(">") != string::npos) {
                  int p = raw[i].find(">");
                  newraw.push_back("jeq " + label + "," + raw[i].substr(3, p - 3) + "," + raw[i].substr(p + 1));
                  newraw.push_back("jlt " + label + "," + raw[i].substr(3, p - 3) + "," + raw[i].substr(p + 1));
                } else if (raw[i].find("<") != string::npos) {
                  int p = raw[i].find("<");
                  newraw.push_back("jeq " + label + "," + raw[i].substr(3, p - 3) + "," + raw[i].substr(p + 1));
                  newraw.push_back("jgt " + label + "," + raw[i].substr(3, p - 3) + "," + raw[i].substr(p + 1));
                }
              } else if (k == j) {
                newraw.push_back(label + ":");
              } else {
                newraw.push_back(raw[k]);
              }
            }
            raw = newraw;
            break;
          }
        }
      }
    }
  }
  rep (i, raw.size()) {
    if (raw[i].substr(0, 5) == "WHILE") {
      int cnt = 1;
      for (int j = i + 1; j < (int)raw.size(); ++j) {
        if (raw[j].substr(0, 5) == "WHILE") ++cnt;
        if (raw[j] == "ENDWHILE") {
          --cnt;
          if (cnt == 0) {
            stringstream ss1;
            ss1 << lc;
            string label1;
            ss1 >> label1;
            label1 = "_LABEL" + label1;
            ++lc;
            stringstream ss2;
            ss2 << lc;
            string label2;
            ss2 >> label2;
            label2 = "_LABEL" + label2;
            ++lc;
            vector<string> newraw;
            rep (k, raw.size()) {
              if (i == k) {
                newraw.push_back(label1 + ":");
                if (raw[k].find("==") != string::npos) {
                  int p = raw[k].find("==");
                  newraw.push_back("jgt " + label2 + "," + raw[k].substr(6, p - 6) + "," + raw[k].substr(p + 2));
                  newraw.push_back("jlt " + label2 + "," + raw[k].substr(6, p - 6) + "," + raw[k].substr(p + 2));
                } else if (raw[k].find("!=") != string::npos) {
                  int p = raw[k].find("!=");
                  newraw.push_back("jeq " + label2 + "," + raw[k].substr(6, p - 6) + "," + raw[k].substr(p + 2));
                } else if (raw[k].find(">=") != string::npos) {
                  int p = raw[k].find(">=");
                  newraw.push_back("jlt " + label2 + "," + raw[k].substr(6, p - 6) + "," + raw[k].substr(p + 2));
                } else if (raw[k].find("<=") != string::npos) {
                  int p = raw[k].find("<=");
                  newraw.push_back("jgt " + label2 + "," + raw[k].substr(6, p - 6) + "," + raw[k].substr(p + 2));
                } else if (raw[k].find(">") != string::npos) {
                  int p = raw[k].find(">");
                  newraw.push_back("jeq " + label2 + "," + raw[k].substr(6, p - 6) + "," + raw[k].substr(p + 1));
                  newraw.push_back("jlt " + label2 + "," + raw[k].substr(6, p - 6) + "," + raw[k].substr(p + 1));
                } else if (raw[k].find("<") != string::npos) {
                  int p = raw[k].find("<");
                  newraw.push_back("jeq " + label2 + "," + raw[k].substr(6, p - 6) + "," + raw[k].substr(p + 1));
                  newraw.push_back("jgt " + label2 + "," + raw[k].substr(6, p - 6) + "," + raw[k].substr(p + 1));
                }
              } else if (j == k) {
                newraw.push_back("jeq " + label1 + ",0,0");
                newraw.push_back(label2 + ":");
              } else {
                newraw.push_back(raw[k]);
              }
            }
            raw = newraw;
            break;
          }
        }
      }
    }
  }
  vector<string> newraw;
  rep (i, raw.size()) {
    if (raw[i].substr(0, 3) == "rnd") {
      newraw.push_back("mul [255],97");
      newraw.push_back("add [255],89");
      newraw.push_back("mov " + raw[i].substr(4) + ",[255]"); 
    } else if (raw[i].substr(0, 3) == "mod") {
      string a, b = raw[i].substr(raw[i].find(",") + 1);
      a = raw[i].substr(4, raw[i].size() - b.size() - 5);
      newraw.push_back("mov [254]," + a);
      newraw.push_back("div [254]," + b);
      newraw.push_back("mul [254]," + b);
      newraw.push_back("sub " + a + ",[254]");
    } else if (raw[i].substr(0, 6) == "return") {
      if (raw[i] == "return") {
        newraw.push_back("int 0");
        newraw.push_back("hlt");
      } else {
        string a = raw[i].substr(7);
        newraw.push_back("mov a," + a);
        newraw.push_back("int 0");
        newraw.push_back("hlt");
      }
    } else if (raw[i].find("+=") != string::npos) {
      int p = raw[i].find("+=");
      newraw.push_back("add " + raw[i].substr(0, p) + "," + raw[i].substr(p + 2));
    } else if (raw[i].find("-=") != string::npos) {
      int p = raw[i].find("-=");
      newraw.push_back("sub " + raw[i].substr(0, p) + "," + raw[i].substr(p + 2));
    } else if (raw[i].find("*=") != string::npos) {
      int p = raw[i].find("*=");
      newraw.push_back("mul " + raw[i].substr(0, p) + "," + raw[i].substr(p + 2));
    } else if (raw[i].find("/=") != string::npos) {
      int p = raw[i].find("/=");
      newraw.push_back("div " + raw[i].substr(0, p) + "," + raw[i].substr(p + 2));
    } else if (raw[i].find("&=") != string::npos) {
      int p = raw[i].find("&=");
      newraw.push_back("and " + raw[i].substr(0, p) + "," + raw[i].substr(p + 2));
    } else if (raw[i].find("|=") != string::npos) {
      int p = raw[i].find("|=");
      newraw.push_back("or " + raw[i].substr(0, p) + "," + raw[i].substr(p + 2));
    } else if (raw[i].find("^=") != string::npos) {
      int p = raw[i].find("^=");
      newraw.push_back("xor " + raw[i].substr(0, p) + "," + raw[i].substr(p + 2));
    } else if (raw[i].find("++") != string::npos) {
      int p = raw[i].find("++");
      newraw.push_back("inc " + raw[i].substr(0, p));
    } else if (raw[i].find("--") != string::npos) {
      int p = raw[i].find("--");
      newraw.push_back("dec " + raw[i].substr(0, p));
    } else if (raw[i].find("=") != string::npos) {
      int p = raw[i].find("=");
      newraw.push_back("mov " + raw[i].substr(0, p) + "," + raw[i].substr(p + 1));
    } else if (raw[i].substr(0, 4) == "goto") {
      newraw.push_back("jeq " + raw[i].substr(5) + ",0,0");
    } else {
      newraw.push_back(raw[i]);
    }
  }
  raw = newraw;
  vector<string> res;
  map<string, int> tag;
  int line = 0;
  rep (i, raw.size()) {
    str = raw[i];
    if (str[str.size() - 1] == ':') {
      tag[str.substr(0, str.size() - 1)] = line;
    } else {
      res.push_back(str);
      ++line;
    }
  }
  for (map<string, int>::iterator itr = tag.begin(); itr != tag.end(); ++itr) {
    stringstream ss;
    ss << itr->second;
    string num;
    ss >> num;
    rep (i, res.size()) {
      while (true) {
        int p = res[i].find(itr->first);
        if (p == (int)string::npos) break;
        res[i] = res[i].substr(0, p) + num + res[i].substr(p + itr->first.size());
      }
    }
  }
  map<string, string> cnst;
  cnst["WALL"] = "0";
  cnst["EMPTY"] = "1";
  cnst["PILL"] = "2";
  cnst["POWER"] = "3";
  cnst["FRUIT"] = "4";
  cnst["LAMBDA"] = "5";
  cnst["GHOST"] = "6";
  cnst["UP"] = "0";
  cnst["RIGHT"] = "1";
  cnst["DOWN"] = "2";
  cnst["LEFT"] = "3";
  for (map<string, string>::iterator itr = cnst.begin(); itr != cnst.end(); ++itr) {
    rep (i, res.size()) {
      while (true) {
        int p = res[i].find(itr->first);
        if (p == (int)string::npos) break;
        res[i] = res[i].substr(0, p) + itr->second + res[i].substr(p + itr->first.size());
      }
    }
  }
  set<string> vars;
  rep (i, res.size()) {
    int p = 0;
    while (res[i].find("@", p) != string::npos) {
      p = res[i].find("@", p);
      int q;
      for (q = p + 1; q < (int)res[i].size() && (isdigit(res[i][q]) || isalpha(res[i][q])); ++q);
      vars.insert(res[i].substr(p, q - p));
      ++p;
    }
  }
  vector<string> varvec;
  for (set<string>::iterator itr = vars.begin(); itr != vars.end(); ++itr) {
    varvec.push_back(*itr);
  }
  sort(varvec.begin(), varvec.end(), greaterlen);
  int memory = 253;
  rep (i, varvec.size()) {
    string var = varvec[i];
    string num;
    stringstream ss;
    ss << memory;
    ss >> num;
    --memory;
    num = "[" + num + "]";
    rep (j, res.size()) {
      int p = 0;
      while (res[j].find(var, p) != string::npos) {
        p = res[j].find(var, p);
        res[j] = res[j].substr(0, p) + num + res[j].substr(p + var.size());
        ++p;
      }
    }
  }
  rep (i, res.size()) {
    cout << res[i] << endl;
  }
}
