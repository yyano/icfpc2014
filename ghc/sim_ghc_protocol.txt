GHCがSIMに求めるAPIリスト。各行の#以降の説明にあるような情報が取れればいいです。プロトコルは一案です。 by wistery_k

SIM -> GHC
EXEC # 実行開始

GHC -> SIM -> GHC
L1   => x y # 1人目のラムダマンの座標
L2   => x y # 2人目のラムダマンの座標
IX   => i   # そのゴーストのインデックス
GS i => x y # i人目のゴーストの開始座標
GC i => x y # i人目のゴーストの現在座標
GI i => v d # i人目のゴーストのvatalityとdirection。vatality = { 0: standard, 1: fright mode, 2 = invisible }。 direction = { 0: up, 1: right, 2: down, 3: left }。
M x y => a # mapの(x,y)の内容。 a = { 0: 壁, 1: 何も無し, 2: Pill, 3: Power pill, 4: Fruit, 5: ラムダマンの開始位置, 6: ゴーストの開始位置 }

GHC -> SIM
D str   # strをデバッグ出力
RET dir # 向きをdirにして実行終了
