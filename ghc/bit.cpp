#include<iostream>
#include<cassert>

using namespace std;

#define rep(i, n) for (int i = 0; i < int(n); ++i)

int AND85(int n) {
  return
    n - n / 2 * 2
    + (n / 4 - n / 4 / 2 * 2) * 4
    + (n / 16 - n / 16 / 2 * 2) * 16
    + (n / 64 - n / 64 / 2 * 2) * 64;
}

int AND(int a, int b) {
  int a1 = AND85(a);
  int b1 = AND85(b);
  int a2 = a - a1;
  int b2 = b - b1;
  return AND85((a1 + b1) / 2) + AND85((a2 + b2) / 4) * 2;
}

int OR(int a, int b) {
  int a1 = AND85(a);
  int b1 = AND85(b);
  int a2 = a - a1;
  int b2 = b - b1;
  int s1 = a1 + b1;
  int s2 = a2 + b2;
  return AND85(s1) + AND85(s1 / 2) + AND85(s2 / 2) * 2 + AND85(s2 / 4) * 2;
}

int XOR(int a, int b) {
  int a1 = AND85(a);
  int b1 = AND85(b);
  int a2 = a - a1;
  int b2 = b - b1;
  return AND85(a1 + b1) + AND85((a2 + b2) / 2) * 2;
}

int main() {
  rep (a, 256) rep (b, 256) {
    assert((a & b) == AND(a, b));
    assert((a | b) == OR(a, b));
    assert((a ^ b) == XOR(a, b));
  }
}
