#define int var
#define rep(i,n) for(int i=0;i<(n);i++)

main(world, undef) {
  return <127, aifunc>;
}

aifunc(state, world) {
  return doit(world, state);
}

doit(world, tick) {
  int a[32][32], n, mp, x, y, ghost, column, row, dir = 0, mindist = 1000000, nextTick;

  nextTick = tick + 127;
  mp = @world;
  row = 0;
  column = 0;

  /*
    parsing world
   */
  while (atom(mp) == 0) {
    int car;
    car = @mp;
    column = 0;
    while (atom(car) == 0) {
      a[row][column] = <@car, -1>;
      if (@car == 4) {
        if (!!!world == 0) {
          a[row][column] = <1, -1>;
        }
      }
      car = !car;
      column++;
    }
    mp = !mp;
    row++;
  }
  debug(row);
  debug(column);

  /*
    fetching each ghost
   */
  ghost = @!!world;
  while (atom(ghost) == 0) {
    x = @@!@ghost;
    y = !@!@ghost;
    if (@@ghost) {
      a[y][x] = <2, -1>;
    } else {
      a[y][x] = <0, -1>;
      a[y - 1][x] = <0, -1>;
      a[y + 1][x] = <0, -1>;
      a[y][x - 1] = <0, -1>;
      a[y][x + 1] = <0, -1>;
    }
    ghost = !ghost;
  }


  rep(dp, 4) {
    int dist = 1000000, mytick = tick + 127, qa, qb, ca, cb, flag = 1;
    rep(i, row) {
      rep(j, column) {
        a[i][j] = <@a[i][j], -1>;
      }
    }
    x = @@!@!world;
    y = !@!@!world;
    if (dp == 0) {
      y -= 1;
    }
    if (dp == 1) {
      x += 1;
    }
    if (dp == 2) {
      y += 1;
    }
    if (dp == 3) {
      x -= 1;
    }
    if (@a[y][x] != 0) {
    qa = <<<x, y>, 0>, 0>;
    qb = 0;
    ca = 1;
    cb = 0;
    while (ca + cb > 0) {
      if (cb == 0) {
        rep(i, ca) {
          qb = <@qa, qb>;
          qa = !qa;
        }
        cb = ca;
        ca = 0;
      }
      cb = cb - 1;
      x = @@@qb;
      y = !@@qb;
      n = !@qb;
      qb = !qb;
      a[y][x] = <@a[y][x], n>;
      if (@a[y][x] < 5) {
        if (1 < @a[y][x]) {
          if (n == 0) {
            mytick += 10;
          }
          dist = n;
          flag = 0;
          ca = 0;
          cb = 0;
        }
      }
      if (flag == 1) {
      if (0 < @a[y][x]) {
        if (!a[y - 1][x] < 0) {
          qa = <<<x, y - 1>, n + 1>, qa>;
          ca = ca + 1;
        }
        if (!a[y][x - 1] < 0) {
          qa = <<<x - 1, y>, n + 1>, qa>;
          ca = ca + 1;
        }
        if (!a[y][x + 1] < 0) {
          qa = <<<x + 1, y>, n + 1>, qa>;
          ca = ca + 1;
        }
        if (!a[y + 1][x] < 0) {
          qa = <<<x, y + 1>, n + 1>, qa>;
          ca = ca + 1;
        }
      }
      }
    }
    dist = 10000;
    
    if (dist < mindist) {
      mindist = dist;
      dir = dp;
      nextTick = mytick;
    }
    }
  }

  return <nextTick, dir>;
}
