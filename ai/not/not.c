#define int var
#define rep(i,n) for(int i=0;i<(n);i++)
main(world, undef) {
  return <127, aifunc>;
}
aifunc(state, world) {
  int d, b, k, t;
  b = 1000000;
  t = state + 127;
  /* bfs */
  for (int x = 4; (x > 0) * (b > 900000); x = x - 1) {
    rep(i, 4) {
      k = bfs(world, i, state, x);
      if (@k < b) {
        b = @k;
        d = i;
        t = !k;
      }
    }
  }
  
  /* must move */
  /*
  if (b > 900000) {
    rep(i, 4) {
      k = bfs(world, i, state, 2);
      if (@k < b) {
        b = @k;
        d = i;
        t = !k;
      }
    }
  }
  */
  return <t, d>;
}
bfs(world, dp, state, block) {
  int a[32][32], qa, qb, ca, cb, n, mp, x, y, ghost, gv, tick, column, row, g, p, nx, ny;
  tick = state;
  
  x = @@!@!world;
  y = !@!@!world;
  if (dp == 0) {
    nx = x;
    ny = y - 1;
  }
  if (dp == 1) {
    nx = x + 1;
    ny = y;
  }
  if (dp == 2) {
    nx = x;
    ny = y + 1;
  }
  if (dp == 3) {
    nx = x - 1;
    ny = y;
  }

  /* read map */
  mp = @world;
  row = 0;
  column = 0;
  {
    while (atom(mp) == 0) {
      int car;
      car = @mp;
      while (atom(car) == 0) {
        a[row][column] = <@car, 0 - 1>;
        if (@car == 4) {
          if (!!!world == 0) {
            a[row][column] = <1, 0 - 1>;
          }
        }
        car = !car;
        column++;
      }
      mp = !mp;
      row++;
      column = 0;
    }
  }
  
  if (@a[ny][nx] == 0) {
    return <1000000, tick + 127>;
  }

  if (1000 < @@!world) {
    /* read ghost */
    ghost = @!!world;
    g = 0;
    
    while (atom(ghost) == 0) {
      g = <<<@@!@ghost, !@!@ghost>, !!@ghost>, g>;
      ghost = !ghost;
    }

    /* eat ghost */
    while (atom(g) == 0) {
      var sta, stb, now, dir;
      sta = <<@g, 0>, 0>;
      stb = 0;
      while ((atom(sta) == 0) + (atom(stb) == 0)) {
        if (atom(stb)) {
          while (atom(sta) == 0) {
            stb = <@sta, stb>;
            sta = !sta;
          }
        }
        now = @stb;
        stb = !stb;
        x = @@@now;
        y = !@@now;
        dir = !@now;
        if (!now < 5) {
          if ((x != nx) + (y != ny)) {
            if (@a[y][x] != 0) {
              sta = <<<<x, y - 1>, 0>, !now + 1>, sta>;
              sta = <<<<x + 1, y>, 1>, !now + 1>, sta>;
              sta = <<<<x, y + 1>, 2>, !now + 1>, sta>;
              sta = <<<<x - 1, y>, 3>, !now + 1>, sta>;
            }
          } else {
            return <!now - 100, tick>;
          }
        }
      }
      g = !g;
    }
  }
  /* read ghost */
  ghost = @!!world;
  g = 0;
  
  while (atom(ghost) == 0) {
    g = <<<@@!@ghost, !@!@ghost>, !!@ghost>, g>;
    ghost = !ghost;
  }

  /* ghost block */
  while (atom(g) == 0) {
    var sta, stb, now, dir;
    sta = <<@g, 0>, 0>;
    stb = 0;
    while ((atom(sta) == 0) + (atom(stb) == 0)) {
      if (atom(stb)) {
        while (atom(sta) == 0) {
          stb = <@sta, stb>;
          sta = !sta;
        }
      }
      now = @stb;
      stb = !stb;
      x = @@@now;
      y = !@@now;
      dir = !@now;
      if (!now < block) {
        if ((x != @@!@!world) + (y != !@!@!world)) {
          if (@a[y][x] != 0) {
            if (@a[y][x] != 3) {
              a[y][x] = <0, -1>;
            }
            if (dir == 0) {
              sta = <<<<x, y - 1>, 0>, !now + 1>, sta>;
              sta = <<<<x + 1, y>, 1>, !now + 1>, sta>;
              sta = <<<<x - 1, y>, 3>, !now + 1>, sta>;
            }
            if (dir == 1) {
              sta = <<<<x, y - 1>, 0>, !now + 1>, sta>;
              sta = <<<<x + 1, y>, 1>, !now + 1>, sta>;
              sta = <<<<x, y + 1>, 2>, !now + 1>, sta>;
            }
            if (dir == 2) {
              sta = <<<<x, y + 1>, 2>, !now + 1>, sta>;
              sta = <<<<x + 1, y>, 1>, !now + 1>, sta>;
              sta = <<<<x - 1, y>, 3>, !now + 1>, sta>;
            }
            if (dir == 3) {
              sta = <<<<x, y - 1>, 0>, !now + 1>, sta>;
              sta = <<<<x - 1, y>, 3>, !now + 1>, sta>;
              sta = <<<<x, y + 1>, 2>, !now + 1>, sta>;
            }
          }
        }
      }
    }
    g = !g;
  }
    
  /* bfs */
  qa = <<<nx, ny>, 0>, 0>;
  qb = 0;
  ca = 1;
  cb = 0;
  while (ca + cb > 0) {
    if (cb == 0) {
      rep(i, ca) {
        qb = <@qa, qb>;
        qa = !qa;
      }
      cb = ca;
      ca = 0;
    }
    cb = cb - 1;
    x = @@@qb;
    y = !@@qb;
    n = !@qb;
    qb = !qb;
    a[y][x] = <@a[y][x], n>;
    if (@a[y][x] < 5) {
      if (1 < @a[y][x]) {
        if (n == 0) {
          tick += 137;
        } else {
          tick += 127;
        }
        return <n, tick>;
      }
    }
    if (0 < @a[y][x]) {
      if (!a[y - 1][x] < 0) {
        qa = <<<x, y - 1>, n + 1>, qa>;
        ca = ca + 1;
      }
      if (!a[y][x - 1] < 0) {
        qa = <<<x - 1, y>, n + 1>, qa>;
        ca = ca + 1;
      }
      if (!a[y][x + 1] < 0) {
        qa = <<<x + 1, y>, n + 1>, qa>;
        ca = ca + 1;
      }
      if (!a[y + 1][x] < 0) {
        qa = <<<x, y + 1>, n + 1>, qa>;
        ca = ca + 1;
      }
    }
  }
  return <1000000 - n, tick + 127>;
}
