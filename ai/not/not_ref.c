#define int var
#define rep(i,n) for(int i=0;i<(n);i++)
#define L 4
main(world, undef) {
  return <127, aifunc>;
}
aifunc(state, world) {
  return <0, bfs(world)>;
}
bfs(world) {
  int a[64][64], qa, qb, n, mp, x, y, ghost, gv, column, row, g, p, nx, ny, qa4, qb4, qa3, qb3, qa2, qb2, count, tmp, best, bdir, dis, absx, absy, tmp2;
  
  /* read map */
  mp = @world;
  row = 0;
  column = 0;
  qa = 0;
  qb = 0;
  qa4 = 0;
  qb4 = 0;
  qa3 = 0;
  qb3 = 0;
  qa2 = 0;
  qb2 = 0;
  count = 0;
  best = 5000000;
  
  nx = @@!@!world;
  ny = !@!@!world;

  while (atom(mp) == 0) {
    int car;
    car = @mp;
    while (atom(car) == 0) {
      a[row][column] = <@car, <0, 0>>;
      if (@car == 4) {
        if (!!!world == 0) {
          a[row][column] = <1, <0, 0>>;
        }
      }
      if (1 < @a[row][column]) {
        if (@a[row][column] < 5) {
          qa = <<<row, column>, 0>, qa>;
          if (row < ny) {
            absy = ny - row;
          } else {
            absy = row - ny;
          }
          if (column < nx) {
            absx = nx - column;
          } else {
            absx = column - nx;
          }
          dis = absy + absx;
          if (dis < best) {
            best = dis;
            if (absy < absx) {
              if (nx < column) {
                bdir = 1;
              } else {
                bdir = 3;
              }
            } else {
              if (ny < row) {
                bdir = 2;
              } else {
                bdir = 0;
              }
            }
          }
        }
      }
      car = !car;
      column++;
    }
    mp = !mp;
    row++;
    column = 0;
  }

  if (0 < @@!world) {
    /* read ghost */
    ghost = @!!world;
    g = 0;
    
    while (atom(ghost) == 0) {
      g = <<<@@!@ghost, !@!@ghost>, !!@ghost>, g>;
      ghost = !ghost;
    }

    /* eat ghost */
    while (atom(g) == 0) {
      var sta, stb, now, dir;
      sta = <<@g, 0>, 0>;
      stb = 0;
      while ((atom(sta) == 0) + (atom(stb) == 0)) {
        if (atom(stb)) {
          while (atom(sta) == 0) {
            stb = <@sta, stb>;
            sta = !sta;
          }
        }
        now = @stb;
        stb = !stb;
        x = @@@now;
        y = !@@now;
        dir = !@now;
        if (!now < (@@!world / 500)) {
          if (@a[y][x] != 0) {
            if (x == nx) {
              if (y == ny - 1) {
                return 0;
              } else if (y == ny + 1) {
                return 2;
              }
            } else if (y == ny) {
              if (x == nx - 1) {
                return 3;
              } else if (x == nx + 1) {
                return 1;
              }
            }
            sta = <<<<x, y - 1>, 0>, !now + 1>, sta>;
            sta = <<<<x + 1, y>, 1>, !now + 1>, sta>;
            sta = <<<<x, y + 1>, 2>, !now + 1>, sta>;
            sta = <<<<x - 1, y>, 3>, !now + 1>, sta>;
          }
        }
      }
      g = !g;
    }
  }
  /* read ghost */
  ghost = @!!world;
  g = 0;
  while (atom(ghost) == 0) {
    g = <<<@@!@ghost, !@!@ghost>, !!@ghost>, g>;
    ghost = !ghost;
  }

  
  /* ghost block */
  while (atom(g) == 0) {
    var sta, stb, now, dir;
    sta = <<@g, 0>, 0>;
    stb = 0;
    while ((atom(sta) == 0) + (atom(stb) == 0)) {
      if (atom(stb)) {
        while (atom(sta) == 0) {
          stb = <@sta, stb>;
          sta = !sta;
        }
      }
      now = @stb;
      stb = !stb;
      x = @@@now;
      y = !@@now;
      dir = !@now;
      if (!now < 4) {
        if ((x != @@!@!world) + (y != !@!@!world)) {
          if (@a[y][x] != 0) {
            if (@a[y][x] != 0) { 
              if ((@!a[y][x] == 0) + (@!a[y][x] + 4 < !now)) {
                a[y][x] = <@a[y][x], <!now - 4, 0>>;
              }
            }
            if (dir == 0) {
              sta = <<<<x, y - 1>, 0>, !now + 1>, sta>;
              sta = <<<<x + 1, y>, 1>, !now + 1>, sta>;
              sta = <<<<x - 1, y>, 3>, !now + 1>, sta>;
            }
            if (dir == 1) {
              sta = <<<<x, y - 1>, 0>, !now + 1>, sta>;
              sta = <<<<x + 1, y>, 1>, !now + 1>, sta>;
              sta = <<<<x, y + 1>, 2>, !now + 1>, sta>;
            }
            if (dir == 2) {
              sta = <<<<x, y + 1>, 2>, !now + 1>, sta>;
              sta = <<<<x + 1, y>, 1>, !now + 1>, sta>;
              sta = <<<<x - 1, y>, 3>, !now + 1>, sta>;
            }
            if (dir == 3) {
              sta = <<<<x, y - 1>, 0>, !now + 1>, sta>;
              sta = <<<<x - 1, y>, 3>, !now + 1>, sta>;
              sta = <<<<x, y + 1>, 2>, !now + 1>, sta>;
            }
          }
        }
      }
    }
    g = !g;
  }
  /* bfs */
  while (((atom(qa) == 0) + (atom(qb) == 0)) * (count < 5000)) {
    count++;
    if (atom(qb)) {
      while (atom(qa) == 0) {
        qb = <@qa, qb>;
        qa = !qa;
      }
    }
    y = @@@qb;
    x = !@@qb;
    n = !@qb;
    qb = !qb;
    tmp = @a[y][x];
    if ((0 < tmp) * (!!a[y][x] == 0)) {
      tmp2 = @!a[y][x];
      a[y][x] = <tmp, <tmp2, 1>>;
      if (-1 < tmp2) {
        if (x == nx) {
          if (y == ny - 1) {
            return 0;
          } else if (y == ny + 1) {
            return 2;
          }
        } else if (y == ny) {
          if (x == nx - 1) {
            return 3;
          } else if (x == nx + 1) {
            return 1;
          }
        }
        if (!!a[y - 1][x] == 0) {
          qa = <<<y - 1, x>, n + 1>, qa>;
        }
        if (!!a[y][x - 1] == 0) {
          qa = <<<y, x - 1>, n + 1>, qa>;
        }
        if (!!a[y][x + 1] == 0) {
          qa = <<<y, x + 1>, n + 1>, qa>;
        }
        if (!!a[y + 1][x] == 0) {
          qa = <<<y + 1, x>, n + 1>, qa>;
        }
      } else if (tmp2 == 0) {
        qa4 = <<<y, x>, n>, qa4>;
      } else if (tmp2 == -1) {
        qa3 = <<<y, x>, n>, qa3>;
      } else if (tmp2 == -2) {
        qa2 = <<<y, x>, n>, qa2>;
      }
    }
  }
  while (atom(qa4) == 0) {
    qb4 = <@qa4, qb4>;
    qa4 = !qa4;
  }
  while (((atom(qa) == 0) + (atom(qb) == 0) + (atom(qb4) == 0)) * (count < 5000)) {
    count++;
    if (atom(qb)) {
      if (atom(qa) == 0) {
        while (atom(qa) == 0) {
          qb = <@qa, qb>;
          qa = !qa;
        }
      } else {
        qb = <@qb4, qb>;
        qb4 = !qb4;
      }
    }
    y = @@@qb;
    x = !@@qb;
    n = !@qb;
    qb = !qb;
    tmp = @a[y][x];
    if (0 < tmp) {
      tmp2 = @!a[y][x];
      a[y][x] = <tmp, <tmp2, 1>>;
      if (-1 < tmp2) {
        if (x == nx) {
          if (y == ny - 1) {
            return 0;
          } else if (y == ny + 1) {
            return 2;
          }
        } else if (y == ny) {
          if (x == nx - 1) {
            return 3;
          } else if (x == nx + 1) {
            return 1;
          }
        }
        if (!!a[y - 1][x] == 0) {
          while (atom(qb4) == 0) {
            if (!@qb4 > n) {
              goto label41;
            }
            qa = <@qb4, qa>;
            qb4 = !qb4;
          }
        label41:
          qa = <<<y - 1, x>, n + 1>, qa>;
        }
        if (!!a[y][x - 1] == 0) {
          while (atom(qb4) == 0) {
            if (!@qb4 > n) {
              goto label42;
            }
            qa = <@qb4, qa>;
            qb4 = !qb4;
          }
        label42:
          qa = <<<y, x - 1>, n + 1>, qa>;
        }
        if (!!a[y][x + 1] == 0) {
          while (atom(qb4) == 0) {
            if (!@qb4 > n) {
              goto label43;
            }
            qa = <@qb4, qa>;
            qb4 = !qb4;
          }
        label43:
          qa = <<<y, x + 1>, n + 1>, qa>;
        }
        if (!!a[y + 1][x] == 0) {
          while (atom(qb4) == 0) {
            if (!@qb4 > n) {
              goto label44;
            }
            qa = <@qb4, qa>;
            qb4 = !qb4;
          }
        label44:
          qa = <<<y + 1, x>, n + 1>, qa>;
        }
      } else if (tmp2 == -1) {
        qa3 = <<<y, x>, n>, qa3>;
      } else if (tmp2 == -2) {
        qa2 = <<<y, x>, n>, qa2>;
      }
    }
  }
  while (atom(qa3) == 0) {
    qb3 = <@qa3, qb3>;
    qa3 = !qa3;
  }
  while (((atom(qa) == 0) + (atom(qb) == 0) + (atom(qb3) == 0)) * (count < 5000)) {
    count++;
    if (atom(qb)) {
      if (atom(qa) == 0) {
        while (atom(qa) == 0) {
          qb = <@qa, qb>;
          qa = !qa;
        }
      } else {
        qb = <@qb3, qb>;
        qb3 = !qb3;
      }
    }
    y = @@@qb;
    x = !@@qb;
    n = !@qb;
    qb = !qb;
    tmp = @a[y][x];
    if (0 < tmp) {
      tmp2 = @!a[y][x];
      a[y][x] = <tmp, <tmp2, 1>>;
      if (-2 < tmp2) {
        if (x == nx) {
          if (y == ny - 1) {
            return 0;
          } else if (y == ny + 1) {
            return 2;
          }
        } else if (y == ny) {
          if (x == nx - 1) {
            return 3;
          } else if (x == nx + 1) {
            return 1;
          }
        }
        if (!!a[y - 1][x] == 0) {
          while (atom(qb3) == 0) {
            if (!@qb3 > n) {
              goto label31;
            }
            qa = <@qb3, qa>;
            qb3 = !qb3;
          }
        label31:
          qa = <<<y - 1, x>, n + 1>, qa>;
        }
        if (!!a[y][x - 1] == 0) {
          while (atom(qb3) == 0) {
            if (!@qb3 > n) {
              goto label32;
            }
            qa = <@qb3, qa>;
            qb3 = !qb3;
          }
        label32:
          qa = <<<y, x - 1>, n + 1>, qa>;
        }
        if (!!a[y][x + 1] == 0) {
          while (atom(qb3) == 0) {
            if (!@qb3 > n) {
              goto label33;
            }
            qa = <@qb3, qa>;
            qb3 = !qb3;
          }
        label33:
          qa = <<<y, x + 1>, n + 1>, qa>;
        }
        if (!!a[y + 1][x] == 0) {
          while (atom(qb3) == 0) {
            if (!@qb3 > n) {
              goto label34;
            }
            qa = <@qb3, qa>;
            qb3 = !qb3;
          }
        label34:
          qa = <<<y + 1, x>, n + 1>, qa>;
        }
      } else if (tmp2 == -2) {
        qa2 = <<<y, x>, n>, qa2>;
      }
    }
  }
  while (atom(qa2) == 0) {
    qb2 = <@qa2, qb2>;
    qa2 = !qa2;
  }
  while (((atom(qa) == 0) + (atom(qb) == 0) + (atom(qb2) == 0)) * (count < 5000)) {
    count++;
    if (atom(qb)) {
      if (atom(qa) == 0) {
        while (atom(qa) == 0) {
          qb = <@qa, qb>;
          qa = !qa;
        }
      } else {
        qb = <@qb2, qb>;
        qb2 = !qb2;
      }
    }
    y = @@@qb;
    x = !@@qb;
    n = !@qb;
    qb = !qb;
    tmp = @a[y][x];
    if (0 < tmp) {
      tmp2 = @!a[y][x];
      a[y][x] = <tmp, <tmp2, 1>>;
      if (-3 < tmp2) {
        if (x == nx) {
          if (y == ny - 1) {
            return 0;
          } else if (y == ny + 1) {
            return 2;
          }
        } else if (y == ny) {
          if (x == nx - 1) {
            return 3;
          } else if (x == nx + 1) {
            return 1;
          }
        }
        if (!!a[y - 1][x] == 0) {
          while (atom(qb2) == 0) {
            if (!@qb2 > n) {
              goto label21;
            }
            qa = <@qb2, qa>;
            qb2 = !qb2;
          }
        label21:
          qa = <<<y - 1, x>, n + 1>, qa>;
        }
        if (!!a[y][x - 1] == 0) {
          while (atom(qb2) == 0) {
            if (!@qb2 > n) {
              goto label22;
            }
            qa = <@qb2, qa>;
            qb2 = !qb2;
          }
        label22:
          qa = <<<y, x - 1>, n + 1>, qa>;
        }
        if (!!a[y][x + 1] == 0) {
          while (atom(qb2) == 0) {
            if (!@qb2 > n) {
              goto label23;
            }
            qa = <@qb2, qa>;
            qb2 = !qb2;
          }
        label23:
          qa = <<<y, x + 1>, n + 1>, qa>;
        }
        if (!!a[y + 1][x] == 0) {
          while (atom(qb2) == 0) {
            if (!@qb2 > n) {
              goto label24;
            }
            qa = <@qb2, qa>;
            qb2 = !qb2;
          }
        label24:
          qa = <<<y + 1, x>, n + 1>, qa>;
        }
      }
    }
  }
  if (@a[ny - 1][nx] != 0) {
    if (best > @!a[ny - 1][nx]) {
      best = @!a[ny - 1][nx];
      bdir = 0;
    } 
  }
  if (@a[ny][nx + 1] != 0) {
    if (best > @!a[ny][nx + 1]) {
      best = @!a[ny][nx + 1];
      bdir = 1;
    } 
  }
  if (@a[ny + 1][nx] != 0) {
    if (best > @!a[ny + 1][nx]) {
      best = @!a[ny + 1][nx];
      bdir = 2;
    } 
  }
  if (@a[ny][nx - 1] != 0) {
    if (best > @!a[ny][nx - 1]) {
      best = @!a[ny][nx - 1];
      bdir = 3;
    } 
  }
  return bdir;
}
