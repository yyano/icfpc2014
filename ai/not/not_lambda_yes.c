#define int var
#define rep(i,n) for(int i=0;i<(n);i = i + 1)
#define FRONT 4
#define LIMIT 1000
#define SIZE 32
main(world, undef) {
  var r, h, w, mp;
  r = <-1, -1>;
  r = <<-1, -1>, r>;
  r = <<-1, -1>, r>;
  r = <<-1, -1>, r>;
  r = <<-1, -1>, r>;
  r = <<-1, -1>, r>;
  r = <<-1, -1>, r>;
  r = <<-1, -1>, r>;
  r = <<-1, -1>, r>;
  r = <<-1, -1>, r>;
  r = <<-1, -1>, r>;
  r = <<-1, -1>, r>;
  r = <<-1, -1>, r>;
  r = <<-1, -1>, r>;
  r = <<-1, -1>, r>;
  r = <<-1, -1>, r>;
  r = <<-1, -1>, r>;
  r = <<-1, -1>, r>;
  r = <<-1, -1>, r>;

  mp = @world;
  h = 0;

  while (atom(mp) == 0) {
    int car;
    car = @mp;
    w = 0;
    while (atom(car) == 0) {
      car = !car;
      w = w + 1;
    }
    mp = !mp;
    h = h + 1;
  }

  return <<<r, 1000000009>, <h, w>>, aifunc>;
}
aifunc(state, world) {
  var val, bstate;
  bstate = <@@state, !@state * 1000000007 + 1000000009>;
  val = bfs(bstate, world, @!state, !!state);
  if (val == 0) {
    bstate = <<<@@!@!world, !@!@!world - 1>, @bstate>, !bstate>;
  }
  if (val == 1) {
    bstate = <<<@@!@!world + 1, !@!@!world>, @bstate>, !bstate>;
  }
  if (val == 2) {
    bstate = <<<@@!@!world, !@!@!world + 1>, @bstate>, !bstate>;
  }
  if (val == 3) {
    bstate = <<<@@!@!world - 1, !@!@!world>, @bstate>, !bstate>;
  }
  return <<bstate, <@!state, !!state>>, val>;
}
bfs(state, world, h, w) {
  int a[SIZE][SIZE], qa, qb, n, mp, x, y, ghost, gv, column, row, g, p, nx, ny, qa4, qb4, qa3, qb3, qa2, qb2, count, tmp, best, bdir, dis, absx, absy, tmp2, bx, by, tx, ty;
  mp = @world;
  row = 0;
  qa = 0;
  qb = 0;
  qa4 = 0;
  qb4 = 0;
  qa3 = 0;
  qb3 = 0;
  qa2 = 0;
  qb2 = 0;
  count = 0;
  
  nx = @@!@!world;
  ny = !@!@!world;
  
  rep(i, SIZE) {
    rep(j, SIZE) {
      a[i][j] = <0, <0, -1>>;
    }
  }

  /* read map */
  if (SIZE / 2 < ny) {
    if (h - SIZE / 2 < ny) {
      by = h - SIZE;
    } else {
      by = ny - SIZE / 2;
    }
  } else {
    by = 0;
  }
  if (SIZE / 2 < nx) {
    if (w - SIZE / 2 < nx) {
      bx = w - SIZE;
    } else {
      bx = nx - SIZE / 2;
    }
  } else {
    bx = 0;
  }
  ny = ny - by;
  nx = nx - bx;
  while (atom(mp) == 0) {
    int car;
    car = @mp;
    column = 0;
    while (atom(car) == 0) {
      if (by - 1 < row && row < by + SIZE && bx - 1 < column && column < bx + SIZE) {
        a[row - by][column - bx] = <@car, <0, -1>>;
        if (@car == 4) {
          if (!!!world == 0) {
            a[row - by][column - bx] = <1, <0, -1>>;
          }
        }
      }
      car = !car;
      column = column + 1;
    }
    mp = !mp;
    row = row + 1;
  }

  /* Ikidomari tsubushi kokokara */

  for(int i = 1; i < SIZE - 1; i++) {
    for(int j = 1; j < SIZE - 1; j++) {
      //debug(a[i][j]);
      if(@a[i][j] == 1) {
        var up, right, down, left;
        (up, right, down, left) = getAround(a, i, j);
        
        if(1 == ((@up != 0) + (@right != 0) + (@down != 0) + (@left != 0))) {
          a[i][j] = <0, !a[i][j]>;
        }
      }
    }
  }

  /* kokomade */

  rep (i, SIZE) {
    a[i][0] = <0, <0, -1>>;
    a[0][i] = <0, <0, -1>>;
    a[i][SIZE - 1] = <0, <0, -1>>;
    a[SIZE - 1][i] = <0, <0, -1>>;
  }
  
  if (0 < @@!world) {
      var sta, stb;
    /* read ghost */
    ghost = @!!world;
    sta = 0;
    stb = 0;
    
    while (atom(ghost) == 0) {
      if (-1 < @@!@ghost - bx && @@!@ghost - bx < SIZE && -1 < !@!@ghost - by && !@!@ghost - by < SIZE && @@ghost == 1) {
          stb = <<<@@!@ghost - bx, !@!@ghost - by>, 0>, stb>;
      }
      ghost = !ghost;
    }
    
    /* eat ghost */
    while ((atom(sta) == 0) || (atom(stb) == 0)) {
        var now, x, y, dist, up, right, down, left;
        
        if (atom(stb)) {
            while (atom(sta) == 0) {
                stb = <@sta, stb>;
                sta = !sta;
            }
        }
        
        now = @stb;
        stb = !stb;
        x = @@now;
        y = !@now;
        dist = !now;
        
        (up, right, down, left) = getAround(a, y, x);

        if (dist < (@@!world / 500) || dist == 0) {
            if (x == nx) {
                if (y == ny - 1) {
                    return 0;
                } else if (y == ny + 1) {
                    return 2;
                }
            } else if (y == ny) {
                if (x == nx - 1) {
                    return 3;
                } else if (x == nx + 1) {
                    return 1;
                }
            }
            
            if (@up != 0) {
              sta = <<<x, y - 1>, dist + 1>, sta>;
            }
            if (@right != 0) {
              sta = <<<x + 1, y>, dist + 1>, sta>;
            }
            if (@down != 0) {
              sta = <<<x, y + 1>, dist + 1>, sta>;
            }
            if (@left != 0) {
              sta = <<<x - 1, y>, dist + 1>, sta>;
            }
        }
    }
  }

  /* read ghost */
  ghost = @!!world;
  g = 0;
  while (atom(ghost) == 0) {
    if (-1 < @@!@ghost - bx && @@!@ghost - bx < SIZE && -1 < !@!@ghost - by && !@!@ghost - by < SIZE) {
      g = <<<@@!@ghost - bx, !@!@ghost - by>, !!@ghost>, g>;
    }
    ghost = !ghost;
  }

  /* ghost block */
  while (atom(g) == 0) {
    var sta, stb, now, dir;
    sta = <<@g, 0>, 0>;
    stb = 0;
    while ((atom(sta) == 0) + (atom(stb) == 0)) {
      if (atom(stb)) {
        while (atom(sta) == 0) {
          stb = <@sta, stb>;
          sta = !sta;
        }
      }
      now = @stb;
      stb = !stb;
      x = @@@now;
      y = !@@now;
      dir = !@now;
      if (!now < FRONT) {
        if (@a[y][x] != 0) {
          if ((@a[y][x] != 0) && (@a[y][x] != 3)) { 
            if ((@!a[y][x] == 0) || (@!a[y][x] + FRONT > !now)) {
              a[y][x] = <@a[y][x], <!now - FRONT, -1>>;
            }
          }
          if (dir == 0) {
            sta = <<<<x, y - 1>, 0>, !now + 1>, sta>;
            sta = <<<<x + 1, y>, 1>, !now + 1>, sta>;
            sta = <<<<x - 1, y>, 3>, !now + 1>, sta>;
          }
          if (dir == 1) {
            sta = <<<<x, y - 1>, 0>, !now + 1>, sta>;
            sta = <<<<x + 1, y>, 1>, !now + 1>, sta>;
            sta = <<<<x, y + 1>, 2>, !now + 1>, sta>;
          }
          if (dir == 2) {
            sta = <<<<x, y + 1>, 2>, !now + 1>, sta>;
            sta = <<<<x + 1, y>, 1>, !now + 1>, sta>;
            sta = <<<<x - 1, y>, 3>, !now + 1>, sta>;
          }
          if (dir == 3) {
            sta = <<<<x, y - 1>, 0>, !now + 1>, sta>;
            sta = <<<<x - 1, y>, 3>, !now + 1>, sta>;
            sta = <<<<x, y + 1>, 2>, !now + 1>, sta>;
          }
        }
      }
    }
    g = !g;
  }

  if (-3 < @!a[ny - 1][nx]) {
    qa = <<<ny - 1, nx>, 0>, qa>;
  }
  if (-3 < @!a[ny][nx + 1]) {
    qa = <<<ny, nx + 1>, 1>, qa>;
  }
  if (-3 < @!a[ny + 1][nx]) {
    qa = <<<ny + 1, nx>, 2>, qa>;
  }
  if (-3 < @!a[ny][nx - 1]) {
    qa = <<<ny, nx - 1>, 3>, qa>;
  }

  /* bfs */
  while (((atom(qa) == 0) + (atom(qb) == 0)) * (count < LIMIT)) {
    var up, down, right, left;
    count = count + 1;
    if (atom(qb)) {
      while (atom(qa) == 0) {
        qb = <@qa, qb>;
        qa = !qa;
      }
    }
    y = @@@qb;
    x = !@@qb;
    n = !@qb;
    qb = !qb;
    tmp = @a[y][x];
    if ((0 < tmp) && (!!a[y][x] == -1)) {
      tmp2 = @!a[y][x];
      if (-1 < tmp2) {
        a[y][x] = <tmp, <tmp2, n>>;
        if (1 < tmp && tmp < 5) {
          return n;
        }
        (up, right, down, left) = getAround(a, y, x);
        if (!!up == -1) {
          qa = <<<y - 1, x>, n>, qa>;
        }
        if (!!left == -1) {
          qa = <<<y, x - 1>, n>, qa>;
        }
        if (!!right == -1) {
          qa = <<<y, x + 1>, n>, qa>;
        }
        if (!!down == -1) {
          qa = <<<y + 1, x>, n>, qa>;
        }
      } else if (tmp2 == -1) {
        qa4 = <<<y, x>, n>, qa4>;
      } else if (tmp2 == -2) {
        qa3 = <<<y, x>, n>, qa3>;
      } else if (tmp2 == -3) {
        qa2 = <<<y, x>, n>, qa2>;
      }
    }
  }
  while (atom(qa4) == 0) {
    qb4 = <@qa4, qb4>;
    qa4 = !qa4;
  }
  while (((atom(qa) == 0) + (atom(qb) == 0) + (atom(qb4) == 0)) * (count < LIMIT)) {
    count = count + 1;
    if (atom(qb)) {
      if (atom(qa) == 0) {
        while (atom(qa) == 0) {
          qb = <@qa, qb>;
          qa = !qa;
        }
      } else {
        qb = <@qb4, qb>;
        qb4 = !qb4;
      }
    }
    y = @@@qb;
    x = !@@qb;
    n = !@qb;
    qb = !qb;
    tmp = @a[y][x];
    if (0 < tmp) {
      var up, right, down, left;
      tmp2 = @!a[y][x];
      if (-2 < tmp2) {
        a[y][x] = <tmp, <tmp2, n>>;
        if (1 < tmp && tmp < 5) {
          return n;
        }
        
        (up, right, down, left) = getAround(a, y, x);
        if (!!up == -1) {
          while (atom(qb4) == 0) {
            if (!@qb4 > n) {
              goto label41;
            }
            qa = <@qb4, qa>;
            qb4 = !qb4;
          }
        label41:
          qa = <<<y - 1, x>, n>, qa>;
        }
        if (!!left == -1) {
          while (atom(qb4) == 0) {
            if (!@qb4 > n) {
              goto label42;
            }
            qa = <@qb4, qa>;
            qb4 = !qb4;
          }
        label42:
          qa = <<<y, x - 1>, n>, qa>;
        }
        if (!!right == -1) {
          while (atom(qb4) == 0) {
            if (!@qb4 > n) {
              goto label43;
            }
            qa = <@qb4, qa>;
            qb4 = !qb4;
          }
        label43:
          qa = <<<y, x + 1>, n>, qa>;
        }
        if (!!down == -1) {
          while (atom(qb4) == 0) {
            if (!@qb4 > n) {
              goto label44;
            }
            qa = <@qb4, qa>;
            qb4 = !qb4;
          }
        label44:
          qa = <<<y + 1, x>, n>, qa>;
        }
      } else if (tmp2 == -2) {
        qa3 = <<<y, x>, n>, qa3>;
      } else if (tmp2 == -3) {
        qa2 = <<<y, x>, n>, qa2>;
      }
    }
  }
  while (atom(qa3) == 0) {
    qb3 = <@qa3, qb3>;
    qa3 = !qa3;
  }
  while (((atom(qa) == 0) + (atom(qb) == 0) + (atom(qb3) == 0)) * (count < LIMIT)) {
    count = count + 1;
    if (atom(qb)) {
      if (atom(qa) == 0) {
        while (atom(qa) == 0) {
          qb = <@qa, qb>;
          qa = !qa;
        }
      } else {
        qb = <@qb3, qb>;
        qb3 = !qb3;
      }
    }
    y = @@@qb;
    x = !@@qb;
    n = !@qb;
    qb = !qb;
    tmp = @a[y][x];
    if (0 < tmp) {
      tmp2 = @!a[y][x];
      if (-3 < tmp2) {
        var up, right, down, left;
      
        a[y][x] = <tmp, <tmp2, n>>;
        if (1 < tmp && tmp < 5) {
          return n;
        }
        (up, right, down, left) = getAround(a, y, x);
        if (!!up == -1) {
          while (atom(qb3) == 0) {
            if (!@qb3 > n) {
              goto label31;
            }
            qa = <@qb3, qa>;
            qb3 = !qb3;
          }
        label31:
          qa = <<<y - 1, x>, n>, qa>;
        }
        if (!!left == -1) {
          while (atom(qb3) == 0) {
            if (!@qb3 > n) {
              goto label32;
            }
            qa = <@qb3, qa>;
            qb3 = !qb3;
          }
        label32:
          qa = <<<y, x - 1>, n>, qa>;
        }
        if (!!right == -1) {
          while (atom(qb3) == 0) {
            if (!@qb3 > n) {
              goto label33;
            }
            qa = <@qb3, qa>;
            qb3 = !qb3;
          }
        label33:
          qa = <<<y, x + 1>, n>, qa>;
        }
        if (!!down == -1) {
          while (atom(qb3) == 0) {
            if (!@qb3 > n) {
              goto label34;
            }
            qa = <@qb3, qa>;
            qb3 = !qb3;
          }
        label34:
          qa = <<<y + 1, x>, n>, qa>;
        }
      } else if (tmp2 == -3) {
        qa2 = <<<y, x>, n>, qa2>;
      }
    }
  }
  while (atom(qa2) == 0) {
    qb2 = <@qa2, qb2>;
    qa2 = !qa2;
  }
  while (((atom(qa) == 0) + (atom(qb) == 0) + (atom(qb2) == 0)) * (count < LIMIT)) {
    count = count + 1;
    if (atom(qb)) {
      if (atom(qa) == 0) {
        while (atom(qa) == 0) {
          qb = <@qa, qb>;
          qa = !qa;
        }
      } else {
        qb = <@qb2, qb>;
        qb2 = !qb2;
      }
    }
    y = @@@qb;
    x = !@@qb;
    n = !@qb;
    qb = !qb;
    tmp = @a[y][x];
    if (0 < tmp) {
      tmp2 = @!a[y][x];
      if (-4 < tmp2) {
        var up, right, down, left;

        a[y][x] = <tmp, <tmp2, n>>;
        if (1 < tmp && tmp < 5) {
          return n;
        }
        (up, right, down, left) = getAround(a, y, x);
        if (!!up == -1) {
          while (atom(qb2) == 0) {
            if (!@qb2 > n) {
              goto label21;
            }
            qa = <@qb2, qa>;
            qb2 = !qb2;
          }
        label21:
          qa = <<<y - 1, x>, n>, qa>;
        }
        if (!!left == -1) {
          while (atom(qb2) == 0) {
            if (!@qb2 > n) {
              goto label22;
            }
            qa = <@qb2, qa>;
            qb2 = !qb2;
          }
        label22:
          qa = <<<y, x - 1>, n>, qa>;
        }
        if (!!right == -1) {
          while (atom(qb2) == 0) {
            if (!@qb2 > n) {
              goto label23;
            }
            qa = <@qb2, qa>;
            qb2 = !qb2;
          }
        label23:
          qa = <<<y, x + 1>, n>, qa>;
        }
        if (!!down == -1) {
          while (atom(qb2) == 0) {
            if (!@qb2 > n) {
              goto label24;
            }
            qa = <@qb2, qa>;
            qb2 = !qb2;
          }
        label24:
          qa = <<<y + 1, x>, n>, qa>;
        }
      }
    }
  }
  best = -100000;
  tx = bx + nx;
  ty = by + ny;
  if (@a[ny - 1][nx] != 0) {
    if (best < @!a[ny - 1][nx]) {
      if (@!a[ny - 1][nx] > -3
          && (tx != @@@state || ty - 1 != !@@state)
          && (tx != @@!@state || ty -1 != !@!@state)
          && (tx != @@!!@state || ty -1 != !@!!@state)
          && (tx != @@!!!@state || ty -1 != !@!!!@state)
          && (tx != @@!!!!@state || ty -1 != !@!!!!@state)
          && (tx != @@!!!!!@state || ty -1 != !@!!!!!@state)
          && (tx != @@!!!!!!@state || ty -1 != !@!!!!!!@state)
          && (tx != @@!!!!!!!@state || ty -1 != !@!!!!!!!@state)
          && (tx != @@!!!!!!!!@state || ty -1 != !@!!!!!!!!@state)
          && (tx != @@!!!!!!!!!@state || ty -1 != !@!!!!!!!!!@state)
          ) {
        best = @!a[ny - 1][nx];
        bdir = 0;
      }
    } 
  }
  if (@a[ny][nx + 1] != 0) {
    if (best < @!a[ny][nx + 1]) {
      if (@!a[ny][nx + 1] > -3
          && (tx + 1 != @@@state || ty != !@@state)
          && (tx + 1 != @@!@state || ty != !@!@state)
          && (tx + 1 != @@!!@state || ty != !@!!@state)
          && (tx + 1 != @@!!!@state || ty != !@!!!@state)
          && (tx + 1 != @@!!!!@state || ty != !@!!!!@state)
          && (tx + 1 != @@!!!!!@state || ty != !@!!!!!@state)
          && (tx + 1 != @@!!!!!!@state || ty != !@!!!!!!@state)
          && (tx + 1 != @@!!!!!!!@state || ty != !@!!!!!!!@state)
          && (tx + 1 != @@!!!!!!!!@state || ty != !@!!!!!!!!@state)
          && (tx + 1 != @@!!!!!!!!!@state || ty != !@!!!!!!!!!@state)
          ) {
        best = @!a[ny][nx + 1];
        bdir = 1;
      }
    }
  }
  if (@a[ny + 1][nx] != 0) {
    if (best < @!a[ny + 1][nx]) {
      if (@!a[ny + 1][nx] > -3
          && (tx != @@@state || ty + 1 != !@@state)
          && (tx != @@!@state || ty + 1 != !@!@state)
          && (tx != @@!!@state || ty + 1 != !@!!@state)
          && (tx != @@!!!@state || ty + 1 != !@!!!@state)
          && (tx != @@!!!!@state || ty + 1 != !@!!!!@state)
          && (tx != @@!!!!!@state || ty + 1 != !@!!!!!@state)
          && (tx != @@!!!!!!@state || ty + 1 != !@!!!!!!@state)
          && (tx != @@!!!!!!!@state || ty + 1 != !@!!!!!!!@state)
          && (tx != @@!!!!!!!!@state || ty + 1 != !@!!!!!!!!@state)
          && (tx != @@!!!!!!!!!@state || ty + 1 != !@!!!!!!!!!@state)
          ) {
        best = @!a[ny + 1][nx];
        bdir = 2;
      }
    }
  }
  if (@a[ny][nx - 1] != 0) {
    if (best < @!a[ny][nx - 1]) {
      if (@!a[ny][nx - 1] > -3
          && (tx - 1 != @@@state || ty != !@@state)
          && (tx - 1 != @@!@state || ty != !@!@state)
          && (tx - 1 != @@!!@state || ty != !@!!@state)
          && (tx - 1 != @@!!!@state || ty != !@!!!@state)
          && (tx - 1 != @@!!!!@state || ty != !@!!!!@state)
          && (tx - 1 != @@!!!!!@state || ty != !@!!!!!@state)
          && (tx - 1 != @@!!!!!!@state || ty != !@!!!!!!@state)
          && (tx - 1 != @@!!!!!!!@state || ty != !@!!!!!!!@state)
          && (tx - 1 != @@!!!!!!!!@state || ty != !@!!!!!!!!@state)
          && (tx - 1 != @@!!!!!!!!!@state || ty != !@!!!!!!!!!@state)
          ) {
      best = @!a[ny][nx - 1];
      bdir = 3;
      }
    }
  }
  if (best > -10) {
    return bdir;
  }
  if (@a[ny - 1][nx] != 0) {
    if (best < @!a[ny - 1][nx]) {
      if (@!a[ny - 1][nx] > -3
          && (tx != @@@state || ty - 1 != !@@state)
          && (tx != @@!@state || ty -1 != !@!@state)
          && (tx != @@!!@state || ty -1 != !@!!@state)
          && (tx != @@!!!@state || ty -1 != !@!!!@state)
          && (tx != @@!!!!@state || ty -1 != !@!!!!@state)
          ) {
        best = @!a[ny - 1][nx];
        bdir = 0;
      }
    } 
  }
  if (@a[ny][nx + 1] != 0) {
    if (best < @!a[ny][nx + 1]) {
      if (@!a[ny][nx + 1] > -3
          && (tx + 1 != @@@state || ty != !@@state)
          && (tx + 1 != @@!@state || ty != !@!@state)
          && (tx + 1 != @@!!@state || ty != !@!!@state)
          && (tx + 1 != @@!!!@state || ty != !@!!!@state)
          && (tx + 1 != @@!!!!@state || ty != !@!!!!@state)
          ) {
        best = @!a[ny][nx + 1];
        bdir = 1;
      }
    }
  }
  if (@a[ny + 1][nx] != 0) {
    if (best < @!a[ny + 1][nx]) {
      if (@!a[ny + 1][nx] > -3
          && (tx != @@@state || ty + 1 != !@@state)
          && (tx != @@!@state || ty + 1 != !@!@state)
          && (tx != @@!!@state || ty + 1 != !@!!@state)
          && (tx != @@!!!@state || ty + 1 != !@!!!@state)
          && (tx != @@!!!!@state || ty + 1 != !@!!!!@state)
          ) {
        best = @!a[ny + 1][nx];
        bdir = 2;
      }
    }
  }
  if (@a[ny][nx - 1] != 0) {
    if (best < @!a[ny][nx - 1]) {
      if (@!a[ny][nx - 1] > -3
          && (tx - 1 != @@@state || ty != !@@state)
          && (tx - 1 != @@!@state || ty != !@!@state)
          && (tx - 1 != @@!!@state || ty != !@!!@state)
          && (tx - 1 != @@!!!@state || ty != !@!!!@state)
          && (tx - 1 != @@!!!!@state || ty != !@!!!!@state)
          ) {
      best = @!a[ny][nx - 1];
      bdir = 3;
      }
    }
  }
  if (best > -10) {
    return bdir;
  } else {
    if (!state < 0) {
      x = -!state;
    } else {
      x = -!state;
    }
    while (1) {
      if ((x - x / 256 * 256) / 64 == 0) {
        if (@a[ny - 1][nx] != 0) {
          return 0;
        }
      }
      if ((x - x / 256 * 256) / 64 == 1) {
        if (@a[ny][nx + 1] != 0) {
          return 1;
        }
      }
      if ((x - x / 256 * 256) / 64 == 2) {
        if (@a[ny + 1][nx] != 0) {
          return 2;
        }
      }
      if ((x - x / 256 * 256) / 64 == 3) {
        if (@a[ny][nx - 1] != 0) {
          return 3;
        }
      }
      x = x * 1000000009 + 1000000007;
    }
  }
}
