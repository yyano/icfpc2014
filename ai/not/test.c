main(world, undef) {
  return <0, aifunc>;
}

aifunc(state, world) {
  var i, d, b, k;
  b = 1000000;
  for (i = 0; i < 4; i++) {
    k = bfs(world, i);
    if (k < b) {
      b = k;
      d = i;
    }
  }
  return <0, d>;
}

bfs(world, dp) {
  var a[512], qa, qb, i, p, ca, cb, n, mp, car, h, w, x, y, ghost, g, gv, g;
  p = 0;
  h = 0;
  mp = @world;
  while (atom(mp) == 0) {
    car = @mp;
    w = 0;
    while (atom(car) == 0) {
      a[p] = <@car, 0 - 1>;
      if (@car == 4) {
        if (!!!world == 0) {
          a[p] = <1, 0 - 1>;
        }
      }
      car = !car;
      p = p + 1;
      w = w + 1;
    }
    mp = !mp;
    h = h + 1;
  }
  ghost = @!!world;
  while (atom(ghost) == 0) {
    g = @ghost;
    if (@g) {
      a[@@!g + !@!g * w] = <2, -1>;
    } else {
      a[@@!g + !@!g * w] = <0, -1>;
      a[@@!g + !@!g * w - w] = <0, -1>;
      a[@@!g + !@!g * w - 1] = <0, -1>;
      a[@@!g + !@!g * w + 1] = <0, -1>;
      a[@@!g + !@!g * w + w] = <0, -1>;
    }
    ghost = !ghost;
  }
  x = @@!@!world;
  y = !@!@!world;
  p = x + y * w;
  if (dp == 0) {
    qa = <<p - w, 0>, 0>;
  }
  if (dp == 1) {
    qa = <<p + 1, 0>, 0>;
  }
  if (dp == 2) {
    qa = <<p + w, 0>, 0>;
  }
  if (dp == 3) {
    qa = <<p - 1, 0>, 0>;
  }
  qb = 0;
  ca = 1;
  cb = 0;
  while (ca + cb > 0) {
    if (cb < 1) {
      for (i = 0; i < ca; i = i + 1) {
        qb = <@qa, qb>;
        qa = !qa;
      }
      cb = ca;
      ca = 0;
    }
    cb = cb - 1;
    p = @@qb;
    n = !@qb;
    qb = !qb;
    a[p] = <@a[p], n>;
    if (@a[p] < 5) {
      if (1 < @a[p]) {
        return n;
      }
    }
    if (0 < @a[p]) {
      if (!a[p - w] < 0) {
        qa = <<p - w, n + 1>, qa>;
        ca = ca + 1;
      }
      if (!a[p - 1] < 0) {
        qa = <<p - 1, n + 1>, qa>;
        ca = ca + 1;
      }
      if (!a[p + 1] < 0) {
        qa = <<p + 1, n + 1>, qa>;
        ca = ca + 1;
      }
      if (!a[p + w] < 0) {
        qa = <<p + w, n + 1>, qa>;
        ca = ca + 1;
      }
    }
  }
  return 1000000;
}
