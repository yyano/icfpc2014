#define int var
#define rep(i,n) for(int i=0;i<(n);i++)
#define FRONT 4
#define LIMIT 1000
#define SIZE 32
main(world, undef) {
  int mp, h, w;
  mp = @world;
  h = 0;
  while (atom(mp) == 0) {
    int car;
    car = @mp;
    w = 0;
    while (atom(car) == 0) {
      car = !car;
      w++;
    }
    mp = !mp;
    h++;
  }
  return <<h, w>, aifunc>;
}
aifunc(state, world) {
  debug(111111, state);
  return <state, bfs(state, world)>;
}
bfs(state, world) {
  int a[SIZE][SIZE], qa, qb, n, mp, x, y, ghost, gv, column, row, g, p, nx, ny, qa4, qb4, qa3, qb3, qa2, qb2, count, tmp, best, bdir, dis, absx, absy, tmp2, bx, by, h, w;
  @state;
  !state;
  mp = @world;
  h = 0;
  while (atom(mp) == 0) {
    int car;
    car = @mp;
    w = 0;
    while (atom(car) == 0) {
      car = !car;
      w++;
    }
    mp = !mp;
    h++;
  }
  mp = @world;
  row = 0;
  qa = 0;
  qb = 0;
  qa4 = 0;
  qb4 = 0;
  qa3 = 0;
  qb3 = 0;
  qa2 = 0;
  qb2 = 0;
  count = 0;
  
  nx = @@!@!world;
  ny = !@!@!world;
  
  /* read map */
  if (SIZE / 2 < ny) {
    if (h - SIZE / 2 < ny) {
      by = h - SIZE;
    } else {
      by = ny - SIZE / 2;
    }
  } else {
    by = 0;
  }
  if (SIZE / 2 < nx) {
    if (w - SIZE / 2 < nx) {
      bx = w - SIZE;
    } else {
      bx = nx - SIZE / 2;
    }
  } else {
    bx = 0;
  }
  ny = ny - by;
  nx = nx - bx;
  while (atom(mp) == 0) {
    int car;
    car = @mp;
    column = 0;
    while (atom(car) == 0) {
      if (by - 1 < row && row < by + SIZE && bx - 1 < column && column < bx + SIZE) {
        a[row - by][column - bx] = <@car, <0, -1>>;
        if (@car == 4) {
          if (!!!world == 0) {
            a[row - by][column - bx] = <1, <0, -1>>;
          }
        }
      }
      car = !car;
      column++;
    }
    mp = !mp;
    row++;
  }
  rep (i, SIZE) {
    a[i][0] = <0, <0, -1>>;
    a[0][i] = <0, <0, -1>>;
    a[i][SIZE - 1] = <0, <0, -1>>;
    a[SIZE - 1][i] = <0, <0, -1>>;
  }
  qa = <<<ny - 1, nx>, 0>, qa>;
  qa = <<<ny, nx + 1>, 1>, qa>;
  qa = <<<ny + 1, nx>, 2>, qa>;
  qa = <<<ny, nx - 1>, 3>, qa>;
  
  if (0 < @@!world) {
    /* read ghost */
    ghost = @!!world;
    g = 0;
    
    while (atom(ghost) == 0) {
      if (-1 < @@!@ghost - bx && @@!@ghost - bx < SIZE && -1 < !@!@ghost - by && !@!@ghost - by < SIZE) {
        g = <<<@@!@ghost - bx, !@!@ghost - by>, !!@ghost>, g>;
      }
      ghost = !ghost;
    }
    
    /* eat ghost */
    while (atom(g) == 0) {
      var sta, stb, now, dir;
      sta = <<@g, 0>, 0>;
      stb = 0;
      while ((atom(sta) == 0) + (atom(stb) == 0)) {
        if (atom(stb)) {
          while (atom(sta) == 0) {
            stb = <@sta, stb>;
            sta = !sta;
          }
        }
        now = @stb;
        stb = !stb;
        x = @@@now;
        y = !@@now;
        dir = !@now;
        if (!now < (@@!world / 500)) {
          if (@a[y][x] != 0) {
            if (x == nx) {
              if (y == ny - 1) {
                return 0;
              } else if (y == ny + 1) {
                return 2;
              }
            } else if (y == ny) {
              if (x == nx - 1) {
                return 3;
              } else if (x == nx + 1) {
                return 1;
              }
            }
            sta = <<<<x, y - 1>, 0>, !now + 1>, sta>;
            sta = <<<<x + 1, y>, 1>, !now + 1>, sta>;
            sta = <<<<x, y + 1>, 2>, !now + 1>, sta>;
            sta = <<<<x - 1, y>, 3>, !now + 1>, sta>;
          }
        }
      }
      g = !g;
    }
  }
  /* read ghost */
  ghost = @!!world;
  g = 0;
  while (atom(ghost) == 0) {
    if (-1 < @@!@ghost - bx && @@!@ghost - bx < SIZE && -1 < !@!@ghost - by && !@!@ghost - by < SIZE) {
      g = <<<@@!@ghost - bx, !@!@ghost - by>, !!@ghost>, g>;
    }
    ghost = !ghost;
  }

  /* ghost block */
  while (atom(g) == 0) {
    var sta, stb, now, dir;
    sta = <<@g, 0>, 0>;
    stb = 0;
    while ((atom(sta) == 0) + (atom(stb) == 0)) {
      if (atom(stb)) {
        while (atom(sta) == 0) {
          stb = <@sta, stb>;
          sta = !sta;
        }
      }
      now = @stb;
      stb = !stb;
      x = @@@now;
      y = !@@now;
      dir = !@now;
      if (!now < FRONT) {
        if (@a[y][x] != 0) {
          if (@a[y][x] != 0) { 
            if ((@!a[y][x] == 0) || (@!a[y][x] + FRONT > !now)) {
              a[y][x] = <@a[y][x], <!now - FRONT, -1>>;
            }
          }
          if (dir == 0) {
            sta = <<<<x, y - 1>, 0>, !now + 1>, sta>;
            sta = <<<<x + 1, y>, 1>, !now + 1>, sta>;
            sta = <<<<x - 1, y>, 3>, !now + 1>, sta>;
          }
          if (dir == 1) {
            sta = <<<<x, y - 1>, 0>, !now + 1>, sta>;
            sta = <<<<x + 1, y>, 1>, !now + 1>, sta>;
            sta = <<<<x, y + 1>, 2>, !now + 1>, sta>;
          }
          if (dir == 2) {
            sta = <<<<x, y + 1>, 2>, !now + 1>, sta>;
            sta = <<<<x + 1, y>, 1>, !now + 1>, sta>;
            sta = <<<<x - 1, y>, 3>, !now + 1>, sta>;
          }
          if (dir == 3) {
            sta = <<<<x, y - 1>, 0>, !now + 1>, sta>;
            sta = <<<<x - 1, y>, 3>, !now + 1>, sta>;
            sta = <<<<x, y + 1>, 2>, !now + 1>, sta>;
          }
        }
      }
    }
    g = !g;
  }

  /* bfs */
  while (((atom(qa) == 0) + (atom(qb) == 0)) * (count < LIMIT)) {
    count++;
    if (atom(qb)) {
      while (atom(qa) == 0) {
        qb = <@qa, qb>;
        qa = !qa;
      }
    }
    y = @@@qb;
    x = !@@qb;
    n = !@qb;
    qb = !qb;
    tmp = @a[y][x];
    if ((0 < tmp) && (!!a[y][x] == -1)) {
      tmp2 = @!a[y][x];
      if (-1 < tmp2) {
        a[y][x] = <tmp, <tmp2, n>>;
        if (1 < tmp && tmp < 5) {
          return n;
        }
        if (!!a[y - 1][x] == -1) {
          qa = <<<y - 1, x>, n>, qa>;
        }
        if (!!a[y][x - 1] == -1) {
          qa = <<<y, x - 1>, n>, qa>;
        }
        if (!!a[y][x + 1] == -1) {
          qa = <<<y, x + 1>, n>, qa>;
        }
        if (!!a[y + 1][x] == -1) {
          qa = <<<y + 1, x>, n>, qa>;
        }
      } else if (tmp2 == -1) {
        qa4 = <<<y, x>, n>, qa4>;
      } else if (tmp2 == -2) {
        qa3 = <<<y, x>, n>, qa3>;
      } else if (tmp2 == -3) {
        qa2 = <<<y, x>, n>, qa2>;
      }
    }
  }
  while (atom(qa4) == 0) {
    qb4 = <@qa4, qb4>;
    qa4 = !qa4;
  }
  while (((atom(qa) == 0) + (atom(qb) == 0) + (atom(qb4) == 0)) * (count < LIMIT)) {
    count++;
    if (atom(qb)) {
      if (atom(qa) == 0) {
        while (atom(qa) == 0) {
          qb = <@qa, qb>;
          qa = !qa;
        }
      } else {
        qb = <@qb4, qb>;
        qb4 = !qb4;
      }
    }
    y = @@@qb;
    x = !@@qb;
    n = !@qb;
    qb = !qb;
    tmp = @a[y][x];
    if (0 < tmp) {
      tmp2 = @!a[y][x];
      if (-2 < tmp2) {
        a[y][x] = <tmp, <tmp2, n>>;
        if (1 < tmp && tmp < 5) {
          return n;
        }
        if (!!a[y - 1][x] == -1) {
          while (atom(qb4) == 0) {
            if (!@qb4 > n) {
              goto label41;
            }
            qa = <@qb4, qa>;
            qb4 = !qb4;
          }
        label41:
          qa = <<<y - 1, x>, n>, qa>;
        }
        if (!!a[y][x - 1] == -1) {
          while (atom(qb4) == 0) {
            if (!@qb4 > n) {
              goto label42;
            }
            qa = <@qb4, qa>;
            qb4 = !qb4;
          }
        label42:
          qa = <<<y, x - 1>, n>, qa>;
        }
        if (!!a[y][x + 1] == -1) {
          while (atom(qb4) == 0) {
            if (!@qb4 > n) {
              goto label43;
            }
            qa = <@qb4, qa>;
            qb4 = !qb4;
          }
        label43:
          qa = <<<y, x + 1>, n>, qa>;
        }
        if (!!a[y + 1][x] == -1) {
          while (atom(qb4) == 0) {
            if (!@qb4 > n) {
              goto label44;
            }
            qa = <@qb4, qa>;
            qb4 = !qb4;
          }
        label44:
          qa = <<<y + 1, x>, n>, qa>;
        }
      } else if (tmp2 == -2) {
        qa3 = <<<y, x>, n>, qa3>;
      } else if (tmp2 == -3) {
        qa2 = <<<y, x>, n>, qa2>;
      }
    }
  }
  while (atom(qa3) == 0) {
    qb3 = <@qa3, qb3>;
    qa3 = !qa3;
  }
  while (((atom(qa) == 0) + (atom(qb) == 0) + (atom(qb3) == 0)) * (count < LIMIT)) {
    count++;
    if (atom(qb)) {
      if (atom(qa) == 0) {
        while (atom(qa) == 0) {
          qb = <@qa, qb>;
          qa = !qa;
        }
      } else {
        qb = <@qb3, qb>;
        qb3 = !qb3;
      }
    }
    y = @@@qb;
    x = !@@qb;
    n = !@qb;
    qb = !qb;
    tmp = @a[y][x];
    if (0 < tmp) {
      tmp2 = @!a[y][x];
      if (-3 < tmp2) {
        a[y][x] = <tmp, <tmp2, n>>;
        if (1 < tmp && tmp < 5) {
          return n;
        }
        if (!!a[y - 1][x] == -1) {
          while (atom(qb3) == 0) {
            if (!@qb3 > n) {
              goto label31;
            }
            qa = <@qb3, qa>;
            qb3 = !qb3;
          }
        label31:
          qa = <<<y - 1, x>, n>, qa>;
        }
        if (!!a[y][x - 1] == -1) {
          while (atom(qb3) == 0) {
            if (!@qb3 > n) {
              goto label32;
            }
            qa = <@qb3, qa>;
            qb3 = !qb3;
          }
        label32:
          qa = <<<y, x - 1>, n>, qa>;
        }
        if (!!a[y][x + 1] == -1) {
          while (atom(qb3) == 0) {
            if (!@qb3 > n) {
              goto label33;
            }
            qa = <@qb3, qa>;
            qb3 = !qb3;
          }
        label33:
          qa = <<<y, x + 1>, n>, qa>;
        }
        if (!!a[y + 1][x] == -1) {
          while (atom(qb3) == 0) {
            if (!@qb3 > n) {
              goto label34;
            }
            qa = <@qb3, qa>;
            qb3 = !qb3;
          }
        label34:
          qa = <<<y + 1, x>, n>, qa>;
        }
      } else if (tmp2 == -3) {
        qa2 = <<<y, x>, n>, qa2>;
      }
    }
  }
  while (atom(qa2) == 0) {
    qb2 = <@qa2, qb2>;
    qa2 = !qa2;
  }
  while (((atom(qa) == 0) + (atom(qb) == 0) + (atom(qb2) == 0)) * (count < LIMIT)) {
    count++;
    if (atom(qb)) {
      if (atom(qa) == 0) {
        while (atom(qa) == 0) {
          qb = <@qa, qb>;
          qa = !qa;
        }
      } else {
        qb = <@qb2, qb>;
        qb2 = !qb2;
      }
    }
    y = @@@qb;
    x = !@@qb;
    n = !@qb;
    qb = !qb;
    tmp = @a[y][x];
    if (0 < tmp) {
      tmp2 = @!a[y][x];
      if (-4 < tmp2) {
        a[y][x] = <tmp, <tmp2, n>>;
        if (1 < tmp && tmp < 5) {
          return n;
        }
        if (!!a[y - 1][x] == -1) {
          while (atom(qb2) == 0) {
            if (!@qb2 > n) {
              goto label21;
            }
            qa = <@qb2, qa>;
            qb2 = !qb2;
          }
        label21:
          qa = <<<y - 1, x>, n>, qa>;
        }
        if (!!a[y][x - 1] == -1) {
          while (atom(qb2) == 0) {
            if (!@qb2 > n) {
              goto label22;
            }
            qa = <@qb2, qa>;
            qb2 = !qb2;
          }
        label22:
          qa = <<<y, x - 1>, n>, qa>;
        }
        if (!!a[y][x + 1] == -1) {
          while (atom(qb2) == 0) {
            if (!@qb2 > n) {
              goto label23;
            }
            qa = <@qb2, qa>;
            qb2 = !qb2;
          }
        label23:
          qa = <<<y, x + 1>, n>, qa>;
        }
        if (!!a[y + 1][x] == -1) {
          while (atom(qb2) == 0) {
            if (!@qb2 > n) {
              goto label24;
            }
            qa = <@qb2, qa>;
            qb2 = !qb2;
          }
        label24:
          qa = <<<y + 1, x>, n>, qa>;
        }
      }
    }
  }
  best = -100000;
  if (@a[ny - 1][nx] != 0) {
    if (best < @!a[ny - 1][nx]) {
      best = @!a[ny - 1][nx];
      bdir = 0;
    } 
  }
  if (@a[ny][nx + 1] != 0) {
    if (best < @!a[ny][nx + 1]) {
      best = @!a[ny][nx + 1];
      bdir = 1;
    } 
  }
  if (@a[ny + 1][nx] != 0) {
    if (best < @!a[ny + 1][nx]) {
      best = @!a[ny + 1][nx];
      bdir = 2;
    } 
  }
  if (@a[ny][nx - 1] != 0) {
    if (best < @!a[ny][nx - 1]) {
      best = @!a[ny][nx - 1];
      bdir = 3;
    } 
  }
  return bdir;
}
