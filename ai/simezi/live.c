#define int var
#define rep(i, n) for(int i = 0; i < (n); i++)
#define inf 1000000
#define MAX_MAP 32

#define WALL 0
#define EMPTY 1
#define PILL 2
#define PPILL 3
#define FRUIT 4
#define LAMBDA 5
#define GHOST 6

main(world, undef){
  return <127, aifunc>;
}
aifunc(state, world){
  int tick = state + 127,
    mindir = 0,
    mindist = inf;
  
  rep(i, 4){
    int dist = bfs(world, i);
    if(mindist > dist){
      mindist = dist;
      mindir = i;
    }
  }
  return <tick, mindir>;
}

bfs(world, initd){
  int map[MAX_MAP][MAX_MAP],
    rowData = @world,
    sy,
    sx,
    ly,
    lx,
    size = inf,
    
    minCross = inf,
    stack1 = 0,
    stack2 = 0
  ;
  
  /* parse map */
  
  for(int y = 0; atom(rowData) == 0; y++){
    int cell = @rowData;
    for(int x = 0; atom(cell) == 0; x++){
      map[y][x] = <@cell, inf>;
      if((@cell == FRUIT) * (!!!world == 0)){ /*fruit time expired*/
        map[y][x] = <EMPTY, inf>;
      }
      cell = !cell;
    }
    rowData = !rowData;
  }
  /*
  for(int ghost = @!!world; atom(ghost) == 0; ghost = !ghost) {
    int x = @@!@ghost, y = !@!@ghost;
    if (@@ghost) {
      map[y][x] = <GHOST, inf>;
    }
  }
  */
  
  /* parse lambdaman's position */
  ly = !@!@!world;
  lx = @@!@!world;
  sy = ly;
  sx = lx;
  
  if(initd == 0){
    sy--;
  }
  if(initd == 1){
    sx++;
  }
  if(initd == 2){
    sy++;
  }
  if(initd == 3){
    sx--;
  }
  if(@map[sy][sx] == WALL){
    return inf;
  }
  
  stack2 = <<sy, sx>, 0>;
  map[ly][lx] = <@map[ly][lx], 0>;
  map[sy][sx] = <@map[sy][sx], 1>;
  
  
  /* BFS */
  while(1 - atom(stack1) * atom(stack2)){
    int y, x, top, dist,
      deg = 0
    ;
    
    if(atom(stack2)){
      /* reverse queue */
      while(1 - atom(stack1)){
        stack2 = <@stack1, stack2>;
        stack1 = !stack1;
      }
    }
    size--;
    
    top = @stack2;
    stack2 = !stack2;
    y = @top;
    x = !top;
    dist = !map[y][x];
    
    /* degree check */
    #define CHECK(y, x) \
    if(@map[y][x] != WALL){ \
      deg++; \
    }
    CHECK(y + 1, x)
    CHECK(y - 1, x)
    CHECK(y, x + 1)
    CHECK(y, x - 1)
    #undef CHECK
    
    if((deg > 2) * (minCross > dist)){
      minCross = dist;
    }
    
    /* this dir turned to be bad!!! */
    if(@map[y][x] == GHOST){
      return size;
    }
    
    if(dist > 10){
      if(1 - (dist - minCross < minCross)){
        goto END;
      }
    }
    /* push to queue */
    dist++;
    #define PUSH \
    if((@map[y][x] != WALL) * (!map[y][x] > dist)){ \
      map[y][x] = <@map[y][x], dist>; \
      stack1 = <<y, x>, stack1>; \
    }
    y++; PUSH y--;
    x++; PUSH x--;
    y--; PUSH y++;
    x--; PUSH x++;
    #undef PUSH
    
    END:
  }
  return size;
}
