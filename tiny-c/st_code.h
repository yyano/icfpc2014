/*
 * stack machine opcode
 */
#define LDC  0  // 1
#define LD   1  // 2
#define ADD  2  // 0
#define SUB  3  // 0
#define MUL  4  // 0
#define DIV  5  // 0
#define CEQ  6  // 0
#define CGT  7  // 0
#define CGTE 8  // 0
#define ATOM 9  // 0
#define CONS 10 // 0
#define CAR  11 // 0
#define CDR  12 // 0
#define SEL  13 // 2(addr)
#define JOIN 14 // 0
#define LDF  15 // 1(addr)
#define AP   16 // 1
#define RTN  17 // 0
#define DUM  18 // 1
#define RAP  19 // 1
#define STOP 20 // 0
#define TSEL 21 // 2(addr)
#define TAP  22 // 1
#define TRAP 23 // 1
#define ST   24 // 2
#define DBUG 25 // 0
#define BRK  26 // 0
#define LBL  27

#ifdef __cplusplus
extern "C" {
#endif
char *st_code_name(int code);
#ifdef __cplusplus
}
#endif
int get_st_code(char *name);


