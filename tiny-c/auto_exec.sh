set -e

make clean
make
echo $1
gcc -E -P $1 > tmpp.c
echo $1
./tiny-cc-st < tmpp.c > tmp.gcc

if [ $# -gt 1 ] && [ $2 = "-s" ]
then

  cat ../gcclib/array_get_small.gcc >> tmp.gcc
  cat ../gcclib/array_set_small.gcc >> tmp.gcc
  
else

  cat ../gcclib/array_access.gcc >> tmp.gcc
  cat ../gcclib/array_set.gcc >> tmp.gcc

fi
cat ../gcclib/array_get_around.gcc >> tmp.gcc
../gcc/convert < tmp.gcc > tmpc.gcc
../gcc/sim < tmpc.gcc