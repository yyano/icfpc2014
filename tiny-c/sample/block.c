main() {
  var x = 5, y = 10, ans = 0;

  while (x) {
    var tmp = x;
    ans += tmp;
    x--;
  }
  while (y) {
    var tmp = y;
    ans += tmp;
    y--;
  }

  debug(ans);
  return ans;
}
