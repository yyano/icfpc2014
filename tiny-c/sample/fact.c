main() {
  return fact(10);
}

fact(n) {
  var ans;
  ans = 1;
  while (n) {
    ans = ans * n;
    n = n - 1;
  }
  return ans;
}
