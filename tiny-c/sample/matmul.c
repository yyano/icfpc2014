#define rep(i,n) for(var i=0;i<(n);i++)

main() {
  var a[2][2], b[2][2], c[2][2];
  
  a[0][0] = 3;
  a[0][1] = 2;
  a[1][0] = 5;
  a[1][1] = 1;
  b[0][0] = 8;
  b[0][1] = 0;
  b[1][0] = 2;
  b[1][1] = 1;

  
  rep(i,2) {
    rep(j,2) {
      rep(k,2) {
        var p = i * 2 + k,
          q = k * 2 + j,
          r = i * 2 + j;
        c[i][j] = c[i][j] + a[i][k] * b[k][j];
      }
    }
  }
  debug(c[0][0], c[0][1], c[1][0], c[1][1]);
  debug(c[0], c[1], c[2], c[3]);
  
  return 0;
}
