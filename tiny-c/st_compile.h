#define MAX_ENV 100000
#define MAX_CODE 1000000

#define VAR_ARG 0
#define VAR_LOCAL 1

typedef struct env {
    Symbol *var;
    int var_kind;
    int pos;
    int width;
} Environment;

#ifdef __cplusplus
extern "C" {
#endif

/* defined in st_compile.c */
void compileStoreVar(Symbol *var, int line);
void compileStoreVarAST(Symbol *var,AST *v, int line);
void compileStoreVarWithOP(Symbol *var,AST *v, int op, int line);

void compileLoadVar(Symbol *var, int line);
void compileStatement(AST *p);
void compileBlock(AST *local_vars,AST *statements);
void compileReturn(AST *expr);
void compileCallFunc(Symbol *f,AST *args);
int  compileArgs(AST *args);
void compileIf(AST *cond, AST *then_part, AST *else_part);
void compileWhile(AST *cond, AST *body);
void compileFor(AST *init,AST *cond,AST *iter,AST *body);

void compileLoadArray(Symbol *var, AST *expr, int line);
void compileStoreArray(Symbol *var, AST *p, AST *q, int line);
void compileStoreArrayWithOP(Symbol *var, AST *p, AST *q, int op, int line);

void compileStoreArrayWithoutAST(Symbol *var, AST *p, int line);
void compileStoreArray2dWithoutAST(Symbol *var, AST *row, AST *column, int line);

/* defined in st_compile_expr.c */
void  compileExpr(AST *p);

/* defined in st_code_gen.c */
void initGenCode(void);
void genCode(int opcode, int line);
void genCodeI(int opcode, int i, int line);
  //void genCodeS(int opcode, std::string s);
void genCodeII(int opcode, int i, int j, int line);
  //void genCodeSS(int opcode, std::string s, std::string t);
void genFuncCode(const char *entry_name, int n_local);

void compileLand(AST *a, AST *b, int line);
void compileLor(AST *a, AST *b, int line);

#ifdef __cplusplus
}
#endif
