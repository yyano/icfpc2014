#include <stdio.h>
#include <ctype.h>
#include <string.h>

#define FALSE 0
#define TRUE 1

enum code {
    LIST,
    NUM,
    STR,
    SYM,
    EQ_OP,
    PLUS_OP,
    MINUS_OP,
    MUL_OP,
    LT_OP,
    GT_OP,
    GET_ARRAY_OP,
    GET_ARRAY2D_OP,
    SET_ARRAY_OP,
    SET_ARRAY2D_OP,
    CALL_OP,
    PRINTLN_OP,
    IF_STATEMENT,
    BLOCK_STATEMENT,
    RETURN_STATEMENT,
    WHILE_STATEMENT,
    FOR_STATEMENT,
    TUPLE,
    CAR_OP,
    CDR_OP,
    ATOM_OP,
    EQEQ_OP,
    DIV_OP,
    PLUSPLUS_OP,
    MINUSMINUS_OP,
    UNARY_MINUS,
    NEQ_OP,
    ADDEQ_OP,
    SUBEQ_OP,
    MULEQ_OP,
    DIVEQ_OP,
    SET_ARRAY_ADDOP,
    SET_ARRAY_SUBOP,
    SET_ARRAY_MULOP,
    SET_ARRAY_DIVOP,
    SET_ARRAY2D_ADDOP,
    SET_ARRAY2D_SUBOP,
    SET_ARRAY2D_MULOP,
    SET_ARRAY2D_DIVOP,
    ARRAY_PLUSPLUS_OP,
    ARRAY_MINUSMINUS_OP,
    ARRAY2D_PLUSPLUS_OP,
    ARRAY2D_MINUSMINUS_OP,
    CLABEL_STATEMENT,
    GOTO_STATEMENT,
    GET_AROUND_STATEMENT,
    LAND_OP,
    LOR_OP
};
  
typedef struct abstract_syntax_tree {
    enum code op;
    int val;
    struct symbol *sym;
    struct abstract_syntax_tree *left,*right;
    char *str;
    int line;
} AST;

typedef struct symbol {
    char *name;
    int val;
    int width;
    int *addr;
    AST *func_params;
    AST *func_body;
} Symbol;

#define MAX_SYMBOLS 100

extern Symbol SymbolTable[];
extern int n_symbols;

void ASTPrint(AST *p);

AST *makeSymbol(char *name);
Symbol *lookupSymbol(char *name);
Symbol *getSymbol(AST *p);

AST *makeNum(int val);
AST *makeStr(char *s);
AST *makeAST(enum code op,AST *right,AST *left, int line);

AST *getNth(AST *p,int nth);
AST *getNext(AST *p);
AST *addLast(AST *l,AST *p);
AST *mergeList(AST *p, AST *q);
AST *setASTVal(AST *l, int p, int w);

#define getFirst(p) getNth(p,0)
#define makeList1(x1) makeAST(LIST,x1,NULL, nline)
#define makeList2(x1,x2) makeAST(LIST,x1,makeAST(LIST,x2,NULL, nline), nline)
#define makeList3(x1,x2,x3) makeAST(LIST,x1,makeAST(LIST,x2,makeAST(LIST,x3,NULL, nline), nline), nline)
#define makeList4(x1,x2,x3,x4) makeAST(LIST,x1,makeList3(x2, x3, x4),nline)

/* prototype for interface from parser to interpreter/compiler */
void defineFunction(Symbol *fsym,AST *params,AST *body);
void declareVariable(Symbol *vsym,AST *init_value);
void declareArray(Symbol *asym,AST *size);

void error(const char *msg);
