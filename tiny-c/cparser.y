/* tiny C parser */
%token NUMBER
%token SYMBOL
%token STRING
%token VAR
%token IF
%token ELSE
%token RETURN
%token WHILE
%token FOR
%token PRINTLN
%token ATOM
%token EQUAL2
%token PLUSPLUS
%token MINUSMINUS
%token NEQ
%token ADDEQ
%token SUBEQ
%token MULEQ
%token DIVEQ
%token CLABEL
%token GOTO
%token GET_AROUND
%token LAND
%token LOR

%{
#include <stdio.h>
#include "AST.h"

extern int nline;

%}

%union {
    AST *val;
}

%right '=' ADDEQ SUBEQ MULEQ DIVEQ
%left LOR
%left LAND
%right EQUAL2 NEQ
%left '<' '>'
%left '+' '-'
%left '*' '/'
%left PLUSPLUS MINUSMINUS
%right '@' '!'

%type <val> parameter_list block local_vars symbol_list 
%type <val> statements statement expr primary_expr arg_list
%type <val> SYMBOL NUMBER STRING CLABEL

%start program

%%

program: /* empty */
	| external_definitions
	;

external_definitions:
	  external_definition
	| external_definitions external_definition
	;

external_definition:
	  SYMBOL parameter_list block  /* fucntion definition */
	{ defineFunction(getSymbol($1),$2,$3); }
	| VAR SYMBOL ';'
	{ declareVariable(getSymbol($2),NULL); }
	| VAR SYMBOL '=' expr ';'
        { declareVariable(getSymbol($2),$4); }
	| VAR SYMBOL '[' expr ']' ';'
	{ declareArray(getSymbol($2),$4); }
	;

parameter_list:
	 '(' ')'
	 { $$ = NULL; }
	| '(' symbol_list ')' 
	 { $$ = $2; }
	;

block:
    '{' statements '}'
    {  $$ = makeAST(BLOCK_STATEMENT,NULL,$2, nline); }
  | '{' local_vars statements '}'
    { $$ = makeAST(BLOCK_STATEMENT,$2,$3, nline); }
	;

local_vars: 
	  VAR symbol_list ';'
	  { $$ = $2; }
	| local_vars VAR symbol_list ';'
	  { $$ = mergeList($1, $3); }
	;

symbol_list: 
	  SYMBOL
	 { $$ = makeList1($1); }
  | SYMBOL '=' expr
  { $$ = makeList1(makeList2($1, $3)); }
	| SYMBOL '[' NUMBER ']'
   { $$ = makeList1(setASTVal($1, $3->val, 1)); }
  | SYMBOL '[' NUMBER ']' '[' NUMBER ']'
  { $$ = makeList1(setASTVal($1, $3->val * $6->val, $6->val)); }
	| symbol_list ',' SYMBOL
  { $$ = addLast($1,$3); } 
	| symbol_list ',' SYMBOL '=' expr
  { $$ = addLast($1,makeList2($3, $5)); }
	| symbol_list ',' SYMBOL '[' NUMBER ']'
  { $$ = addLast($1, setASTVal($3, $5->val, 1)); }
  | symbol_list ',' SYMBOL '[' NUMBER ']' '[' NUMBER ']'
  { $$ = addLast($1, setASTVal($3, $5->val * $8->val, $8->val)); }
	;

statements:
	  statement
	 { $$ = makeList1($1); }
	| statements statement
	 { $$ = addLast($1,$2); }
	;

statement:
	 expr ';'
	 { $$ = $1; }
	| block
	 { $$ = $1; }
	| IF '(' expr ')' statement
	{ $$ = makeAST(IF_STATEMENT,$3,makeList2($5,NULL), nline); }
        | IF '(' expr ')' statement ELSE statement
	{ $$ = makeAST(IF_STATEMENT,$3,makeList2($5,$7), nline); }
	| RETURN expr ';'
	{ $$ = makeAST(RETURN_STATEMENT,$2,NULL, nline); }
	| RETURN ';'
	{ $$ = makeAST(RETURN_STATEMENT,NULL,NULL, nline); }
	| WHILE '(' expr ')' statement
	{ $$ = makeAST(WHILE_STATEMENT,$3,$5, nline); }
	| FOR '(' expr ';' expr ';' expr ')' statement
	{ $$ = makeAST(FOR_STATEMENT,makeList3($3,$5,$7),$9, nline); } 
        | FOR '(' VAR symbol_list ';' expr ';' expr ')' statement
	{ $$ = makeAST(FOR_STATEMENT,makeList3($4,$6,$8),$10, nline); }
        | CLABEL
	{ $$ = makeAST(CLABEL_STATEMENT, $1, NULL, nline); }
        | GOTO SYMBOL ';'
        { $$ = makeAST(GOTO_STATEMENT, $2, NULL, nline); }
        | '(' SYMBOL ',' SYMBOL ',' SYMBOL ',' SYMBOL ')' '=' GET_AROUND '(' SYMBOL ',' expr ',' expr ')' ';'
        { $$ = makeAST(GET_AROUND_STATEMENT, makeList4($2, $4, $6, $8), makeList3($13, $15, $17), nline); }
	;

expr: 	 primary_expr
	| SYMBOL '=' expr
{ $$ = makeAST(EQ_OP,$1,$3, nline); }
  | SYMBOL ADDEQ expr
{ $$ = makeAST(ADDEQ_OP,$1,$3, nline); }
  | SYMBOL SUBEQ expr
{ $$ = makeAST(SUBEQ_OP,$1,$3, nline); }
  | SYMBOL MULEQ expr
{ $$ = makeAST(MULEQ_OP,$1,$3, nline); }
  | SYMBOL DIVEQ expr
{ $$ = makeAST(DIVEQ_OP,$1,$3, nline); }
  | SYMBOL PLUSPLUS
{ $$ = makeAST(PLUSPLUS_OP, $1, NULL, nline); }
  | SYMBOL MINUSMINUS
{ $$ = makeAST(MINUSMINUS_OP, $1, NULL, nline); }

	| SYMBOL '[' expr ']' '=' expr
	{ $$ = makeAST(SET_ARRAY_OP,makeList2($1,$3),$6, nline); }
	| SYMBOL '[' expr ']' ADDEQ expr
	{ $$ = makeAST(SET_ARRAY_ADDOP,makeList2($1,$3),$6, nline); }
	| SYMBOL '[' expr ']' SUBEQ expr
	{ $$ = makeAST(SET_ARRAY_SUBOP,makeList2($1,$3),$6, nline); }
	| SYMBOL '[' expr ']' MULEQ expr
	{ $$ = makeAST(SET_ARRAY_MULOP,makeList2($1,$3),$6, nline); }
	| SYMBOL '[' expr ']' DIVEQ expr
	{ $$ = makeAST(SET_ARRAY_DIVOP,makeList2($1,$3),$6, nline); }
	| SYMBOL '[' expr ']' PLUSPLUS
	{ $$ = makeAST(ARRAY_PLUSPLUS_OP, $1, $3, nline); }
	| SYMBOL '[' expr ']' MINUSMINUS
	{ $$ = makeAST(ARRAY_MINUSMINUS_OP, $1, $3, nline); }
	
  | SYMBOL '[' expr ']' '[' expr ']' '=' expr
  { $$ = makeAST(SET_ARRAY2D_OP,makeList2($1, $3), makeList2($6, $9), nline); }
  | SYMBOL '[' expr ']' '[' expr ']' ADDEQ expr
  { $$ = makeAST(SET_ARRAY2D_ADDOP,makeList2($1, $3), makeList2($6, $9), nline); }
  | SYMBOL '[' expr ']' '[' expr ']' SUBEQ expr
  { $$ = makeAST(SET_ARRAY2D_SUBOP,makeList2($1, $3), makeList2($6, $9), nline); }
  | SYMBOL '[' expr ']' '[' expr ']' MULEQ expr
  { $$ = makeAST(SET_ARRAY2D_MULOP,makeList2($1, $3), makeList2($6, $9), nline); }
  | SYMBOL '[' expr ']' '[' expr ']' DIVEQ expr
  { $$ = makeAST(SET_ARRAY2D_DIVOP,makeList2($1, $3), makeList2($6, $9), nline); }
  | SYMBOL '[' expr ']' '[' expr ']' PLUSPLUS
  { $$ = makeAST(ARRAY2D_PLUSPLUS_OP,makeList2($1, $3), $6, nline); }
  | SYMBOL '[' expr ']' '[' expr ']' MINUSMINUS
  { $$ = makeAST(ARRAY2D_MINUSMINUS_OP,makeList2($1, $3), $6, nline); }
  
  
  
  
	| expr '+' expr
	{ $$ = makeAST(PLUS_OP,$1,$3, nline); }
	| expr '-' expr
	{ $$ = makeAST(MINUS_OP,$1,$3, nline); }
	| expr '*' expr
	 { $$ = makeAST(MUL_OP,$1,$3, nline); }
        | expr '/' expr
 	 { $$ = makeAST(DIV_OP, $1, $3, nline); } 
	| expr '<' expr
	 { $$ = makeAST(LT_OP,$1,$3, nline); }
	| expr '>' expr
	 { $$ = makeAST(GT_OP,$1,$3, nline); }
	| '<' expr ',' expr '>'
	 { $$ = makeAST(TUPLE, $2, $4, nline); }
	| '@' expr
	 { $$ = makeAST(CAR_OP, $2, NULL, nline); }
	| '!' expr
	 { $$ = makeAST(CDR_OP, $2, NULL, nline); }
        | expr EQUAL2 expr
         { $$ = makeAST(EQEQ_OP, $1, $3, nline); } 
        | expr NEQ expr
        { $$ = makeAST(NEQ_OP, $1, $3, nline); }
        | '-' expr
        { $$ = makeAST(UNARY_MINUS, $2, NULL, nline); }
        | expr LAND expr
        { $$ = makeAST(LAND_OP, $1, $3, nline); }
        | expr LOR expr
        { $$ = makeAST(LOR_OP, $1, $3, nline); }
	;

primary_expr:
	  SYMBOL
	| NUMBER
	| STRING
	| SYMBOL '[' expr ']'
	  { $$ = makeAST(GET_ARRAY_OP,$1,$3, nline); }
	| SYMBOL '[' expr ']' '[' expr ']'
  { $$ = makeAST(GET_ARRAY2D_OP,$1,makeList2($3, $6), nline); }
	| SYMBOL '(' arg_list ')'
	 { $$ = makeAST(CALL_OP,$1,$3, nline); }
	| SYMBOL '(' ')'
	 { $$ = makeAST(CALL_OP,$1,NULL, nline); }
        | '(' expr ')'
         { $$ = $2; }  
	| PRINTLN  '(' arg_list ')'
	 { $$ = makeAST(PRINTLN_OP,$3,NULL, nline); }
        | ATOM  '(' expr ')'
	 { $$ = makeAST(ATOM_OP,$3,NULL, nline); }
	;

arg_list:
	 expr
	 { $$ = makeList1($1); }
	| arg_list ',' expr
	 { $$ = addLast($1,$3); }
	;

%%

#include "clex.c"





