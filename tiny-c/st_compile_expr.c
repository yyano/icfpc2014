#include "AST.h"
#include "st_code.h"
#include "st_compile.h"

static void printFunc(AST *args);

void  compileExpr(AST *p)
{
  Symbol *sym;

  if(p == NULL) return;

  switch(p->op){
  case NUM:
    genCodeI(LDC,p->val, p->line);
    return;
  case SYM:
    compileLoadVar(getSymbol(p), p->line);
    return;
  case EQ_OP:
    compileStoreVarAST(getSymbol(p->left),p->right, p->line);
    return;
  case ADDEQ_OP:
    compileStoreVarWithOP(getSymbol(p->left),p->right, ADD, p->line);
    return;
  case SUBEQ_OP:
    compileStoreVarWithOP(getSymbol(p->left),p->right, SUB, p->line);
    return;
  case MULEQ_OP:
    compileStoreVarWithOP(getSymbol(p->left),p->right, MUL, p->line);
    return;
  case DIVEQ_OP:
    compileStoreVarWithOP(getSymbol(p->left),p->right, DIV, p->line);
    return;
  
  case PLUS_OP:
    compileExpr(p->left);
    compileExpr(p->right);
    genCode(ADD, p->line);
    return;
  case MINUS_OP:
    compileExpr(p->left);
    compileExpr(p->right);
    genCode(SUB, p->line);
    return;
  case MUL_OP:
    compileExpr(p->left);
    compileExpr(p->right);
    genCode(MUL, p->line);
    return;
  case DIV_OP:
    compileExpr(p->left);
    compileExpr(p->right);
    genCode(DIV, p->line);
    return;
  case LT_OP:
    compileExpr(p->left);
    compileExpr(p->right);
    genCode(CGTE, p->line);
    genCodeI(LDC, 0, p->line);
    genCode(CEQ, p->line);
    return;
  case GT_OP:
    compileExpr(p->left);
    compileExpr(p->right);
    genCode(CGT, p->line);
    return;
  case CALL_OP:
    compileCallFunc(getSymbol(p->left),p->right);
    return;
  case PRINTLN_OP:
    printFunc(p->left);
    return;
  case ATOM_OP:
    compileExpr(p->left);
    genCode(ATOM, p->line);
    return;
    
  case GET_ARRAY_OP:
    compileLoadArray(getSymbol(p->left), p->right, p->line);
    return;
  case GET_ARRAY2D_OP:
    compileLoadArray2d(getSymbol(p->left), p->right->left, p->right->right->left, p->line);
    return;
    
  case SET_ARRAY_OP:
    compileStoreArray(getSymbol(p->left->left), p->left->right->left, p->right, p->line);
    return;
  case SET_ARRAY_ADDOP:
    compileStoreArrayWithOP(getSymbol(p->left->left), p->left->right->left, p->right, ADD, p->line);
    return;
  case SET_ARRAY_SUBOP:
    compileStoreArrayWithOP(getSymbol(p->left->left), p->left->right->left, p->right, SUB, p->line);
    return;
  case SET_ARRAY_MULOP:
    compileStoreArrayWithOP(getSymbol(p->left->left), p->left->right->left, p->right, MUL, p->line);
    return;
  case SET_ARRAY_DIVOP:
    compileStoreArrayWithOP(getSymbol(p->left->left), p->left->right->left, p->right, DIV, p->line);
    return;
  case ARRAY_PLUSPLUS_OP:
    compileLoadArray(getSymbol(p->left), p->right, p->line);
    genCodeI(LDC, 1, p->line);
    genCode(ADD, p->line);
    compileStoreArrayWithoutAST(getSymbol(p->left), p->right, p->line);
    return;
  case ARRAY_MINUSMINUS_OP:
    compileLoadArray(getSymbol(p->left), p->right, p->line);
    genCodeI(LDC, 1, p->line);
    genCode(SUB, p->line);
    compileStoreArrayWithoutAST(getSymbol(p->left), p->right, p->line);
    return;
  
  case SET_ARRAY2D_OP:
    compileStoreArray2d(getSymbol(p->left->left), p->left->right->left, p->right->left, p->right->right->left, p->line);
    return;
  case SET_ARRAY2D_ADDOP:
    compileStoreArray2dWithOP(getSymbol(p->left->left), p->left->right->left, p->right->left, p->right->right->left, ADD, p->line);
    return;
  case SET_ARRAY2D_SUBOP:
    compileStoreArray2dWithOP(getSymbol(p->left->left), p->left->right->left, p->right->left, p->right->right->left, SUB, p->line);
    return;
  case SET_ARRAY2D_MULOP:
    compileStoreArray2dWithOP(getSymbol(p->left->left), p->left->right->left, p->right->left, p->right->right->left, MUL, p->line);
    return;
  case SET_ARRAY2D_DIVOP:
    compileStoreArray2dWithOP(getSymbol(p->left->left), p->left->right->left, p->right->left, p->right->right->left, DIV, p->line);
    return;
  case ARRAY2D_PLUSPLUS_OP:
    compileLoadArray2d(getSymbol(p->left->left), p->left->right->left, p->right, p->line);
    genCodeI(LDC, 1, p->line);
    genCode(ADD, p->line);
    compileStoreArray2dWithoutAST(getSymbol(p->left->left), p->left->right->left, p->right, p->line);
    return;
  case ARRAY2D_MINUSMINUS_OP:
    compileLoadArray2d(getSymbol(p->left->left), p->left->right->left, p->right, p->line);
    genCodeI(LDC, 1, p->line);
    genCode(SUB, p->line);
    compileStoreArray2dWithoutAST(getSymbol(p->left->left), p->left->right->left, p->right, p->line);
    return;
    
    
  case TUPLE:
    compileExpr(p->left);
    compileExpr(p->right);
    genCode(CONS, p->line);
    return;  
  case CAR_OP:
    compileExpr(p->left);
    genCode(CAR, p->line);
    return;
  case CDR_OP:
    compileExpr(p->left);
    genCode(CDR, p->line);
    return;
  case EQEQ_OP:
    compileExpr(p->left);
    compileExpr(p->right);
    genCode(CEQ, p->line);
    return;
  case NEQ_OP:
    compileExpr(p->left);
    compileExpr(p->right);
    genCode(CEQ, p->line);
    genCodeI(LDC, 0, p->line);
    genCode(CEQ, p->line);
    return;
  case PLUSPLUS_OP:
    sym = getSymbol(p->left);
    compileLoadVar(sym, p->line);
    genCodeI(LDC, 1, p->line);
    genCode(ADD, p->line);
    compileStoreVar(sym, p->line);
    return;
  case MINUSMINUS_OP:
    sym = getSymbol(p->left);
    compileLoadVar(sym, p->line);
    genCodeI(LDC, 1, p->line);
    genCode(SUB, p->line);
    compileStoreVar(sym, p->line);
    return;
  case UNARY_MINUS:
    genCodeI(LDC, 0, p->line);
    compileExpr(p->left);
    genCode(SUB, p->line);
    return;
  case LAND_OP:
    compileLand(p->left, p->right, p->line);
    return;
  case LOR_OP:
    compileLor(p->left, p->right, p->line);
    return;
  default:
    fprintf(stderr, "%d\n", p->op);
    fprintf(stderr, "%d\n", ATOM_OP);
    error("unknown operater/statement");
  }
}

void printFunc(AST *args)
{
  for(; args != NULL; args = getNext(args)) {
    compileExpr(getFirst(args));
    genCode(DBUG, args->line);
  }
}

/*
 * global variable
 */
void declareVariable(Symbol *vsym,AST *init_value)
{
  /* not implemented */
}

/* 
 * Array
 */
void declareArray(Symbol *a, AST *size)
{
  /* not implemented */
}


