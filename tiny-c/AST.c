#include <stdlib.h>
#include "AST.h"

Symbol SymbolTable[MAX_SYMBOLS];
int n_symbols = 0;

AST *makeNum(int val)
{
  AST *p;
  p = (AST *)malloc(sizeof(AST));
  p->op = NUM;
  p->val = val;
  return p;
}

AST *makeStr(char *s)
{
  AST *p;
  p = (AST *)malloc(sizeof(AST));
  p->op = STR;
  p->str = s;
  return p;
}


AST *makeAST(enum code op,AST *left,AST *right, int line)
{
  AST *p;
  p = (AST *)malloc(sizeof(AST));
  p->op = op;
  p->right = right;
  p->left = left;
  p->line = line;
  return p;
}

AST *getNth(AST *p,int nth)
{
  if(p->op != LIST){
    fprintf(stderr,"bad access to list\n");
    exit(1);
  }
  if(nth > 0) return(getNth(p->right,nth-1));
  else return p->left;
}

AST *addLast(AST *l,AST *p)
{
  AST *q;

  if(l == NULL) return makeAST(LIST,p,NULL, 0);
  q = l;
  while(q->right != NULL) q = q->right;
  q->right = makeAST(LIST,p,NULL, 0);
  
  //fprintf(stderr, "%d\n", l->left->op);
  return l;
}

AST *getNext(AST *p)
{
  if(p->op != LIST){
    fprintf(stderr,"bad access to list\n");
    exit(1);
  }
  else return p->right;
}

AST *mergeList(AST *p, AST *q){
  while(q != NULL){
    p = addLast(p, getFirst(q));
    q = getNext(q);
  }
  return p;
}

Symbol *lookupSymbol(char *name)
{
  int i;
  Symbol *sp;

  sp = NULL;
  for(i = 0; i < n_symbols; i++){
    if(strcmp(SymbolTable[i].name,name) == 0){
	    sp = &SymbolTable[i];
	    break;
    }
  }
  if(sp == NULL){
    sp = &SymbolTable[n_symbols++];
    sp->name = strdup(name);
    sp->val = 1;
  }
  return sp;
}

AST *makeSymbol(char *name)
{
  AST *p;
  //fprintf(stderr, "%s\n", name);

  p = (AST *)malloc(sizeof(AST));
  p->op = SYM;
  p->sym = lookupSymbol(name);
  return p;
}

Symbol *getSymbol(AST *p)
{
  if(p->op != SYM){
    fprintf(stderr,"bad access to symbol    %d \n", p->op);
    exit(1);
  }
  else return p->sym;
}

AST *setASTVal(AST *l, int p, int w){
  //fprintf(stderr, "%s %d?????????\n", l->sym->name, p->val);
  l->sym->val = p;
  l->sym->width = w;
  return l;
}
