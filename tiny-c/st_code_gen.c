#include <string>
#include <iostream>

#include "AST.h"
#include "st_code.h"
#include "st_compile.h"

extern "C" {

using namespace std;

struct _code {
  int opcode;
  int operand[2];
  string soperand[2];
  int line;
} Codes[MAX_CODE];

int n_code;

extern int label_counter;

void initGenCode()
{
  n_code = 0;
}

void genCode(int opcode, int line)
{
  Codes[n_code].opcode = opcode;
  Codes[n_code++].line = line;
}

void genCodeI(int opcode, int i, int line)
{
  Codes[n_code].opcode = opcode;
  Codes[n_code].operand[0] = i;
  Codes[n_code++].line = line;
}

void genCodeII(int opcode, int i, int j, int line)
{
  Codes[n_code].opcode = opcode;
  Codes[n_code].operand[0] = i;
  Codes[n_code].operand[1] = j;
  Codes[n_code++].line = line;
}

void genCodeS(int opcode, string s, int line)
{
  Codes[n_code].opcode = opcode;
  Codes[n_code].soperand[0] = s;
  Codes[n_code++].line = line;
}

void genCodeSS(int opcode, string s, string t, int line)
{
  Codes[n_code].opcode = opcode;
  Codes[n_code].soperand[0] = s;
  Codes[n_code].soperand[1] = t;
  Codes[n_code++].line = line;
}

void genLabel(const char *label_name) {
  Codes[n_code].opcode = LBL;
  Codes[n_code++].soperand[0] = label_name;
}

void genFuncCode(const char *entry_name, int n_local)
{
  int i;
  printf("%s:\n", entry_name);
  string dummy(entry_name);
  dummy += "_";
  for (i = 0; i < n_local; i++) {
    printf("LDC 0\n");
  }
  printf("LDF %s\n", dummy.c_str());
  printf("AP %d\n", n_local);
  printf("RTN\n");
  printf("%s:\n", dummy.c_str());
  for(i = 0; i < n_code; i++){
    printf("%s",st_code_name(Codes[i].opcode));
    switch(Codes[i].opcode){
    case LDC:
    case AP:
    case DUM:
    case RAP:
    case TAP:
    case TRAP:
	    printf(" %d",Codes[i].operand[0]);
	    break;
    case LD:
    case ST:
	    printf(" %d %d",Codes[i].operand[0], Codes[i].operand[1]);
	    break;
    case LDF:
	    printf(" %s",Codes[i].soperand[0].c_str());
	    break;
    case SEL:
    case TSEL:
	    printf(" %s %s",Codes[i].soperand[0].c_str(), Codes[i].soperand[1].c_str());
	    break;
    case LBL:
      printf("%s:", Codes[i].soperand[0].c_str());
      break;
    }
    if(Codes[i].opcode != LBL) printf(" ;%d\n", Codes[i].line);
    else printf("\n");
  }
}

}
