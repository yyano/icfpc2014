#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "st_code.h"

struct {
  char *name;
  int code;
} st_code_table[] = {
  {"LDC", LDC },
  {"LD", LD},
  {"ADD", ADD},
  {"SUB", SUB},
  {"MUL", MUL},
  {"DIV", DIV},
  {"CEQ", CEQ},
  {"CGT", CGT},
  {"CGTE", CGTE},
  {"ATOM", ATOM},
  {"CONS", CONS},
  {"CAR", CAR},
  {"CDR", CDR},
  {"SEL", SEL},
  {"JOIN", JOIN},
  {"LDF", LDF},
  {"AP", AP},
  {"RTN", RTN},
  {"DUM", DUM},
  {"RAP", RAP},
  {"STOP", STOP},
  {"TSEL", TSEL},
  {"TAP", TAP},
  {"TRAP", TRAP},
  {"ST", ST},
  {"DBUG", DBUG},
  {"BRK", BRK},
  {"", LBL},
  { 0,0}
};

char *st_code_name(int code)
{
  int i;
  for(i = 0; st_code_table[i].name != 0; i++)
    if(st_code_table[i].code == code) 
	    return st_code_table[i].name;
  fprintf(stderr,"unknown code = %d\n",code);
  exit(1);
}    

int get_st_code(char *name)
{
  int i;
  for(i = 0; st_code_table[i].name != 0; i++)
    if(strcmp(st_code_table[i].name,name) == 0) 
	    return st_code_table[i].code;
  fprintf(stderr,"unknown code name = %s\n",name);
  exit(1);
}






