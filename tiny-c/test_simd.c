main() {
  var a[64][64], up, right, down, left, i, j;
  for(i = 0; i < 64; i++) {
    for(j = 0; j < 64; j++) {
      a[i][j] = (i + 1) * 64 + (j + 1);
    }
  }
  (up, right, down, left) = getAround(a,3,3);
  debug(up, right, down, left);
  return 0;
}
