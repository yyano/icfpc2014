#include <stdlib.h>

char yytext[100];
int nline = 1;

/* for debug */
int getChar(){
  int c;
  c = getc(stdin);
  return c;
}

void ungetChar(int c)
{
  ungetc(c,stdin);
}

int yylex()
{
  int c,n;
  char *p;
 again:
  c = getChar();
  if(c == '\n') {
    nline++;
    goto again;
  }
  if(isspace(c)) goto again;
  switch(c){
  case '*':
    c = getChar();
    if(c == '='){
      return MULEQ;
    }
    else{
      return '*';
    }
  case '>':
  case '<':
  case '(':
  case ')':
  case '{':
  case '}':
  case ']':
  case '[':
  case ';':
  case ',':
  case '@':
  case EOF:
    return c;
  case '&':
    c = getChar();
    if(c == '&') {
      return LAND;
    }
    else {
      ungetChar(c);
      return '&';
    }
  case '|':
    c = getChar();
    if(c == '|') {
      return LOR;
    }
    else {
      ungetChar(c);
      return '|';
    }
  case '+':
    c = getChar();
    if(c == '+') {
      return PLUSPLUS;
    }
    else if(c == '='){
      return ADDEQ;
    }
    else {
      ungetChar(c);
      return '+';
    }
  case '-':
   c = getChar();
    if(c == '-') {
      return MINUSMINUS;
    }
    else if(c == '='){
      return SUBEQ;
    }
    else {
      ungetChar(c);
      return '-';
    }
  case '=':
    c = getChar();
    if(c == '=') {
      return EQUAL2;
    }
    else {
      ungetChar(c);
      return '=';
    }
  case '!':
    c = getChar();
    if (c == '=') {
      return NEQ;
    } else {
      ungetChar(c);
      return '!';
    }
  case '/':
    c = getChar();
    if(c == '='){
      return DIVEQ;
    }
    else if(c == '*') {
      while(1) {
        c = getChar();
	if(c == '\n') nline++;
        if(c == '*') { 
	  c = getChar();
	  if(c == '\n') nline++;
	  if(c == '/') {
	    break;
	  }
        }
      }
      goto again;
    }
    else {
      ungetChar(c);
      return '/';
    }
  case '"':
    p = yytext;
    while((c = getChar()) != '"'){
	    *p++ = c;
    }
    *p = '\0';
    yylval.val = makeStr(strdup(yytext));
    return STRING;
  }
  if(isdigit(c)){
    n = 0;
    do {
	    n = n*10 + c - '0';
	    c = getChar();
    } while(isdigit(c));
    ungetChar(c);
    yylval.val = makeNum(n);
    return NUMBER;
  }
  if(isalpha(c)){
    p = yytext;
    do {
	    *p++ = c;
	    c = getChar();
    } while(isalnum(c) || c == ':');
    *p = '\0';
    ungetChar(c);
    if(strcmp(yytext,"var") == 0) 
	    return VAR;
    else if(strcmp(yytext,"if") == 0) 
	    return IF;
    else if(strcmp(yytext,"else") == 0) 
	    return ELSE;
    else if(strcmp(yytext,"return") == 0) 
	    return RETURN;
    else if(strcmp(yytext,"while") == 0) 
	    return WHILE;
    else if(strcmp(yytext,"for") == 0) 
	    return FOR;
    else if(strcmp(yytext,"debug") == 0) 
	    return PRINTLN;
    else if(strcmp(yytext,"atom") == 0) {
      return ATOM;
    }
    else if(strcmp(yytext, "goto") == 0) {
      return GOTO;
    }
    else if(strcmp(yytext, "getAround") == 0) {
      return GET_AROUND;
    }
    else {
      int len = strlen(yytext);
      if(yytext[len - 1] == ':') {
	yytext[len - 1] = '\0';
	yylval.val = makeSymbol(yytext);
	return CLABEL;
      }
      else {
	yylval.val = makeSymbol(yytext);
	return SYMBOL;	
      }
    }
  }
  fprintf(stderr,"bad char '%c'\n",c);
  exit(1);
}

int yyerror()
{
  fprintf(stderr, "syntax error at line %d\n", nline);
  exit(1);
  return 0;
}
