#include <string>
#include <sstream>
#include <iostream>
#include <cstdlib>

extern "C" {
#include "AST.h"
#include "st_code.h"
#include "st_compile.h"

using namespace std;

int envp = 0;
Environment Env[MAX_ENV];

int label_counter = 0;
int local_var_pos;
int local_var_pos_max;

void genCodeS(int opcode, std::string s, int line);
void genCodeSS(int opcode, std::string s, std::string t, int line);

void compileJump(string label, int line) {
  genCodeI(LDC, 0, line);
  genCodeSS(TSEL, label, label, line);  
}

string getLabelName(int i) {
  stringstream ss;
  ss << "_LABEL" << i;
  return ss.str();
}

void err_by_undef_var(Symbol *var){
  char msg[100];
  sprintf(msg, "undefined variable \"%s\"\n", var->name);
  error(msg);
}

void compileStoreVar(Symbol *var, int line)
{
  int i;
  for(i = envp-1; i >= 0; i--){
    if(Env[i].var == var){
	    switch(Env[i].var_kind){
	    case VAR_ARG:
        genCodeII(ST,1,Env[i].pos, line);
        return;
	    case VAR_LOCAL:
        genCodeII(ST,0,Env[i].pos, line);
        return;
	    }
    }
  }
  err_by_undef_var(var);
}
void compileStoreVarAST(Symbol *var, AST *v, int line) {
  compileExpr(v);
  compileStoreVar(var, line);
}
void compileStoreVarWithOP(Symbol *var, AST *v, int op, int line) {
  compileLoadVar(var, line);
  compileExpr(v);
  genCode(op, line);
  if(op != ADD && op != SUB && op != MUL && op != DIV){
    error("illeagal store with op instruction");
  }
  compileStoreVar(var, line);
}

void compileLoadVar(Symbol *var, int line)
{
  int i;
  for(i = envp-1; i >= 0; i--){
    if(Env[i].var == var){
	    switch(Env[i].var_kind){
	    case VAR_ARG:
	      genCodeII(LD,1,Env[i].pos, line);
        return;
	    case VAR_LOCAL:
	      genCodeII(LD,0,Env[i].pos, line);
        return;
	    }
    }
  }
  if (strncmp(var->name, "aifunc", 6) == 0) {
    genCodeS(LDF, var->name, line);
    return;
  }
  err_by_undef_var(var);
}


void compileLoadArray(Symbol *var, AST *expr, int line){
  int i;
  for(i = envp-1; i >= 0; i--){
    if(Env[i].var == var){
	    switch(Env[i].var_kind){
	    case VAR_ARG:
	      error("dont push array into args, throw unko!!\n");
	      exit(1);
	    case VAR_LOCAL:
	      compileExpr(expr);
	      genCodeI(LDC, Env[i].pos, line);
	      genCode(ADD, line);
	      genCodeS(LDF, "ARRAY_GET_I", line);
	      genCodeI(AP, 1, line);
        return;
	    }
    }
  }
}

void compileLoadArray2d(Symbol *var, AST *row, AST *column, int line){
  int i;
  for(i = envp-1; i >= 0; i--){
    if(Env[i].var == var){
	    switch(Env[i].var_kind){
	    case VAR_ARG:
	      error("dont push array into args, throw unko!!\n");
	      exit(1);
	    case VAR_LOCAL:
	      compileExpr(row);
        genCodeI(LDC, Env[i].width, line);
        genCode(MUL, line);
        compileExpr(column);
        genCode(ADD, line);
	      genCodeI(LDC, Env[i].pos, line);
	      genCode(ADD, line);
	      genCodeS(LDF, "ARRAY_GET_I", line);
	      genCodeI(AP, 1, line);
        return;
	    }
    }
  }
}

void compileGetAround(Symbol *var, AST *row, AST *column, int line){
  int i;
  for(i = envp-1; i >= 0; i--){
    if(Env[i].var == var){
	    switch(Env[i].var_kind){
	    case VAR_ARG:
	      error("dont push array into args, throw unko!!\n");
	      exit(1);
	    case VAR_LOCAL:
	      compileExpr(row);
	      genCodeI(LDC, Env[i].width, line);
	      genCode(MUL, line);
	      compileExpr(column);
	      genCode(ADD, line);
	      genCodeI(LDC, Env[i].pos, line);
	      genCode(ADD, line);
	      genCodeS(LDF, "ARRAY_GET_AROUND", line);
	      genCodeI(AP, 1, line);
        return;
	    }
    }
  }
}

void compileStoreArrayWithoutAST(Symbol *var, AST *p, int line){
  int i;
  
  for(i = envp-1; i >= 0; i--){
    if(Env[i].var == var){
	    switch(Env[i].var_kind){
	    case VAR_ARG:
        error("dont push array into args, throw unko!!\n");
	      exit(1);
        return;
	    case VAR_LOCAL:
        compileExpr(p);
	      genCodeI(LDC, Env[i].pos, line);
	      genCode(ADD, line);
	      genCodeS(LDF, "ARRAY_SET_I", line);
	      genCodeI(AP, 1, line);
        return;
	    }
    }
  }
  err_by_undef_var(var);
}
void compileStoreArray(Symbol *var, AST *p, AST *q, int line){
  compileExpr(q);
  compileStoreArrayWithoutAST(var, p, line);
}
void compileStoreArrayWithOP(Symbol *var, AST *p, AST *q, int op, int line){
  compileLoadArray(var, p, line);
  compileExpr(q);
  genCode(op, line);
  
  compileStoreArrayWithoutAST(var, p, line);
}

void compileStoreArray2dWithoutAST(Symbol *var, AST *row, AST *column, int line){
  int i;
  
  for(i = envp-1; i >= 0; i--){
    if(Env[i].var == var){
	    switch(Env[i].var_kind){
	    case VAR_ARG:
        error("dont push array into args, throw unko!!\n");
	      exit(1);
        return;
	    case VAR_LOCAL:
        compileExpr(row);
        genCodeI(LDC, Env[i].width, line);
        genCode(MUL, line);
        compileExpr(column);
        genCode(ADD, line);
	      genCodeI(LDC, Env[i].pos, line);
	      genCode(ADD, line);
	      genCodeS(LDF, "ARRAY_SET_I", line);
	      genCodeI(AP, 1, line);
        return;
	    }
    }
  }
  err_by_undef_var(var);
}
void compileStoreArray2d(Symbol *var, AST *row, AST *column, AST *q, int line){
  compileExpr(q);
  compileStoreArray2dWithoutAST(var, row, column, line);
}
void compileStoreArray2dWithOP(Symbol *var, AST *row, AST *column, AST *q, int op, int line){

  compileLoadArray2d(var, row, column, line);
  compileExpr(q);
  genCode(op, line);
  compileStoreArray2dWithoutAST(var, row, column, line);
}

void defineFunction(Symbol *fsym,AST *params,AST *body)
{
  int param_pos;

  initGenCode();
  envp = 0;
  param_pos = 0;
  local_var_pos = 0;
  local_var_pos_max = 0;
  for( ;params != NULL; params = getNext(params)){
  
    Env[envp].var = getSymbol(getFirst(params));
    Env[envp].var_kind = VAR_ARG;
    Env[envp].pos = param_pos++;
    envp++;
  }
  compileStatement(body);
  genFuncCode(fsym->name,local_var_pos_max);
  envp = 0;  /* reset */
}

void compileStatement(AST *p)
{
  if(p == NULL) return;
  switch(p->op){
  case BLOCK_STATEMENT:
    compileBlock(p->left,p->right);
    break;
  case RETURN_STATEMENT:
    compileReturn(p->left);
    break;
  case IF_STATEMENT:
    compileIf(p->left,getNth(p->right,0),getNth(p->right,1));
    break;
  case WHILE_STATEMENT:
    compileWhile(p->left,p->right);
    break;
  case FOR_STATEMENT:
    compileFor(p->left->left, p->left->right->left, p->left->right->right->left, p->right);
    break;
  case CLABEL_STATEMENT:
    genCodeS(LBL, getSymbol(p->left)->name, p->line);
    break;
  case GOTO_STATEMENT:
    genCodeI(LDC, 1, p->line);
    genCodeSS(TSEL, getSymbol(p->left)->name, getSymbol(p->left)->name, p->line);
    break;
  case GET_AROUND_STATEMENT:
    compileGetAround(getSymbol(p->right->left), p->right->right->left, p->right->right->right->left, p->line);
    compileStoreVar(getSymbol(p->left->right->right->right->left), p->line);
    compileStoreVar(getSymbol(p->left->right->right->left), p->line);
    compileStoreVar(getSymbol(p->left->right->left), p->line);
    compileStoreVar(getSymbol(p->left->left), p->line);
    break;
  default:
    compileExpr(p);
    //genCode(ATOM);
  }
}

void compileBlock(AST *local_vars,AST *statements)
{
  int v;
  int envp_save;
  int local_var_pos_save;

  envp_save = envp;
  local_var_pos_save = local_var_pos;
  //fprintf(stderr, "compileBlock\n");
  for( ; local_vars != NULL;local_vars = getNext(local_vars)) {
    //fprintf(stderr, "before name: %s %d\n", local_vars->left->sym->name, local_vars->left->sym->val);
  
    AST *e = getFirst(local_vars);
    Env[envp].var = getSymbol(e->op != LIST ? e : e->left);
    Env[envp].var_kind = VAR_LOCAL;
    Env[envp].pos = local_var_pos;
    Env[envp].width = Env[envp].var->width;
    local_var_pos += Env[envp].var->val;
  
  //fprintf(stderr, "name: %s %d\n", Env[envp].var->name, Env[envp].var->val);
    
    envp++;
    if (e->op == LIST) {
      compileStoreVarAST(getSymbol(e->left), e->right->left, local_vars->line);
    }
  }
  local_var_pos_max = (local_var_pos_max < local_var_pos ? local_var_pos : local_var_pos_max);

  for( ; statements != NULL; statements = getNext(statements))
    compileStatement(getFirst(statements));
  envp = envp_save;
  local_var_pos = local_var_pos_save;
}

void compileReturn(AST *expr)
{
  compileExpr(expr);
  genCode(RTN, expr->line);
}

void compileCallFunc(Symbol *f,AST *args)
{
  int nargs, l;
  nargs = compileArgs(args);
  genCodeS(LDF, f->name, args->line);
  genCodeI(AP,  nargs, args->line);
}

int compileArgs(AST *args)
{
  int nargs;
  if(args != NULL){
    compileExpr(getFirst(args));
    nargs = compileArgs(getNext(args));
    return nargs+1;
  } else return 0;
}

void compileIf(AST *cond, AST *then_part, AST *else_part)
{
  int l1,l2,l3;
  string l1name, l2name, l3name;

  compileExpr(cond);
  l1 = label_counter++;
  l3 = label_counter++;
  l1name = getLabelName(l1);
  l3name = getLabelName(l3);
  genCodeSS(TSEL, l3name, l1name, cond->line);
  genCodeS(LBL, l3name, cond->line);
  compileStatement(then_part);
  l2 = label_counter++;
  l2name = getLabelName(l2);
  if(else_part != NULL){
    genCodeI(LDC, 1, else_part->line);
    genCodeSS(TSEL, l2name, l2name, else_part->line);
    genCodeS(LBL,l1name, else_part->line);
    compileStatement(else_part);
    genCodeS(LBL,l2name, else_part->line);
  } else {
    genCodeS(LBL,l1name, 0);
  }
}

void compileWhile(AST *cond,AST *body)
{

  int l1, l2, l3;
  string l1name, l2name, l3name;
  l1 = label_counter++;
  l2 = label_counter++;
  l3 = label_counter++;
  l1name = getLabelName(l1);
  l2name = getLabelName(l2);
  l3name = getLabelName(l3);

  genCodeS(LBL, l1name, cond->line);
  compileExpr(cond);
  genCodeSS(TSEL, l3name, l2name, cond->line);
  genCodeS(LBL, l3name, cond->line);
  compileStatement(body);
  genCodeI(LDC, 1, body->line);
  genCodeSS(TSEL, l1name, l1name, body->line);
  genCodeS(LBL, l2name, body->line);
}

void compileFor(AST *init,AST *cond,AST *iter,AST *body)
{
  addLast(body->right, iter);
  //fprintf(stderr, "!!!!!!! %d\n", body->op);
  //fprintf(stderr, "!!!!!! %d\n", body->right->op);
  if (init->op == LIST) {
    AST* tmp = makeAST(WHILE_STATEMENT, cond, body, init->line);
    compileBlock(init, makeAST(LIST, tmp, NULL, init->line));
  } else {
    compileExpr(init);
    compileWhile(cond, body);
  }
}

void compileLand(AST *a, AST *b, int line) {

  int l1, l2, l3;
  string l1name, l2name, l3name;
  l1 = label_counter++;
  l2 = label_counter++;
  l3 = label_counter++;
  l1name = getLabelName(l1);
  l2name = getLabelName(l2);
  l3name = getLabelName(l3);

  compileExpr(a);
  genCodeSS(TSEL, l2name, l1name, line);
  genCodeS(LBL, l1name, line);
  genCodeI(LDC, 0, line);
  compileJump(l3name, line);
  genCodeS(LBL, l2name, line);
  compileExpr(b);
  genCodeS(LBL, l3name, line);
}

void compileLor(AST *a, AST *b, int line) {
  int l1, l2, l3;
  string l1name, l2name, l3name;
  l1 = label_counter++;
  l2 = label_counter++;
  l3 = label_counter++;
  l1name = getLabelName(l1);
  l2name = getLabelName(l2);
  l3name = getLabelName(l3);

  compileExpr(a);
  genCodeSS(TSEL, l1name, l2name, line);
  genCodeS(LBL, l1name, line);
  genCodeI(LDC, 1, line);
  compileJump(l3name, line);
  genCodeS(LBL, l2name, line);
  compileExpr(b);
  genCodeS(LBL, l3name, line);
}

}
