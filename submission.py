#! /usr/bin/env python

import os, sys, datetime
import zipfile
import hashlib

default_dst = "./"
default_src = "./"

if __name__ == "__main__":
    source_path = default_src
    print "zip contents in", source_path

    if len(sys.argv) > 1:
        target_dir = sys.argv[1]
    else:
        target_dir = default_dst
        
    target_name = str(datetime.datetime.now()).replace(' ', '-') + ".zip"
    target_path = os.path.abspath(os.path.join(target_dir, target_name))
    print "submission file will be saved at", target_path

    with zipfile.ZipFile(target_path, 'w') as zip_file:
        for dirpath, dirnames, filenames in os.walk(source_path):
            if dirpath.find('.git') >= 0: continue # ignore git files.
            for filename in filenames:
                if filename.endswith('.zip'): continue # ignore zip files.
                filepath = os.path.join(dirpath, filename)
                if os.path.basename(dirpath) == 'solution':
                    arcname = filepath
                else:
                    arcname = os.path.join('code/', filepath)
                print "processing", filepath, "write to", arcname
                zip_file.write(filepath, arcname)

    print ""
    print target_path, "created!!!"

    with open(target_path, 'rb') as zip_bin:
        print "SHA1 checksum is:", hashlib.md5(zip_bin.read()).hexdigest()



