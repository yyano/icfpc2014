import os, sys, datetime
from argparse import ArgumentParser

def parse_log(path):
    with open(path) as r:
        return [eval(line) for line in r]
            
def estimate(player, opponent):
    return 1. / (1. + 10 ** ((opponent - player) / 400))

def elo_update(winner, loser, winner_score, k = 16):
    winner_prob = estimate(winner, loser)
    loser_prob = estimate(loser, winner)
    return winner + k * (winner_score - winner_prob), loser + k * ((1. - winner_score) - loser_prob)

initial_rate = 1500.
if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('--log', nargs = '*', help = 'log file to analyse')
    args = parser.parse_args()

    history = sorted(sum([parse_log(path) for path in args.log], []))
    
    ratings = dict()
    for time, p1, p2, s1, s2 in history:
        if s1 > s2:
            winner, loser = p1, p2
            winner_score = 1.
            print 'o {wn} - {ln} x'.format(wn = winner, ln = loser)
        elif s1 < s2:
            winner, loser = p2, p1
            winner_score = 1.
            print 'o {wn} - {ln} x'.format(wn = winner, ln = loser)
        else:
            winner, loser = p1, p2
            winner_score = 0.5
            print '- {wn} - {ln} -'.format(wn = p1, ln = p2)

        winner_rate = ratings.get(winner, initial_rate)
        loser_rate = ratings.get(loser, initial_rate)
        ratings[winner], ratings[loser] = elo_update(winner_rate, loser_rate, winner_score)

        print '{wn}: {old:.6} -> {new:.6}'.format(wn = winner, old = winner_rate, new = ratings[winner])
        print '{ln}: {old:.6} -> {new:.6}'.format(ln = loser, old = loser_rate, new = ratings[loser])

    print ''
    print '!!! HALL OF FAME !!!'
    for name, rating in ratings.items():
        print '{name}: {rating:.6}'.format(name = name, rating = rating)
