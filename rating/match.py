#! /usr/bin/env python

import subprocess, shlex
import re
import datetime
import random

class Player:
    def __init__(self, conf_path_or_file):
        if type(conf_path_or_file) == file:
            body = conf_path_or_file.read()
        else:
            with open(conf_path_or_file) as r:
                body = r.read()
        conf_dict = eval(body)
        self.name = conf_dict['name']
        self.lambdamans = conf_dict['lambdamans']
        self.ghosts = conf_dict['ghosts']

def run(lambda_player, ghost_player):
    game_command = 'pypy ../sim/game.py'
    gcc_proc_command = '../gcc/processor'
    map_path = '../sim/sample_map.txt'
    lambdas = ' '.join(lambda_player.lambdamans)
    ghosts = ' '.join(ghost_player.ghosts)
    command = "{game_command} -m {map_path} -p {gcc_proc_command} -l {lambdas} -g {ghosts} --random".format(**vars())
    sout = subprocess.check_output(shlex.split(command))
    return int(re.search('Score = (\d+)', sout).group(1))

def match(player1, player2):
    return datetime.datetime.now(), player1.name, player2.name, run(player1, player2), run(player2, player1)

import os, sys
from argparse import ArgumentParser
ai_path = "./ai_confs"

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('--log', default = None, help = 'log destiation. if not specified, log goes to stdout.')
    args = parser.parse_args()
    log_dest = open(args.log, 'a') if args.log else sys.stdout

    players = []
    for filename in os.listdir(ai_path):
        if filename.endswith('.py'):
            filepath = os.path.join(ai_path, filename)
            players.append(Player(filepath))

    history = []
    try:
        while True:   
            log_str = repr(match(*random.sample(players, 2))) + '\n'
            log_dest.write(log_str)
            log_dest.flush()
    except (KeyboardInterrupt, Exception) as e:
        print e
    finally:
        log_dest.close()
