#include <bits/stdc++.h>

#define rep(i,n) for(int i=0;i<(int)(n);i++)
#define each(it,n) for(__typeof((n).begin()) it=(n).begin();it!=(n).end();++it)

using namespace std;

void gen(int index, int left, int right) {
    int middle = (left + right) / 2;
    cout << "ARRAY_GET_I_" << index << ":" << endl;
    if (left == right - 1) {
        cout << "LD 1 " << left << endl;
        cout << "RTN" << endl;
        return;
    }
    cout << "LDC " << middle << endl;
    cout << "LD 0 0" << endl;
    cout << "CGT" << endl;
    cout << "TSEL ARRAY_GET_I_" << index * 2 + 1 << " ARRAY_GET_I_" << index * 2 + 2 << endl;
    gen(index * 2 + 1, left, middle);
    gen(index * 2 + 2, middle, right);
}

int main() {
    const int a_max = 1 << 9;

    cout << "ARRAY_GET_I:" << endl;
    cout << "LDC 1" << endl;
    cout << "TSEL ARRAY_GET_I_0 ARRAY_GET_I_0" << endl;
    gen(0, 0, a_max);
    return 0;
}
