#include <bits/stdc++.h>

#define rep(i,n) for(int i=0;i<(int)(n);i++)
#define each(it,n) for(__typeof((n).begin()) it=(n).begin();it!=(n).end();++it)

using namespace std;

const int DIM = 32;
const int a_max = DIM * DIM;

void gen(int index, int left, int right) {
    int middle = (left + right) / 2;
    
    if(index == 0){
      cout << "ARRAY_GET_AROUND:" << endl;
    }
    else{
      cout << "ARRAY_GET_AROUND_" << index << ":" << endl;
    }
    if (left == right - 1) {
        cout << "LD 1 " << max(0, left - DIM) << endl;
        cout << "LD 1 " << min(left + 1, a_max - 1) << endl;
        cout << "LD 1 " << min(left + DIM, a_max - 1) << endl;
        cout << "LD 1 " << max(0, left - 1) << endl;
        cout << "RTN" << endl;
        return;
    }
    cout << "LDC " << middle << endl;
    cout << "LD 0 0" << endl;
    cout << "CGT" << endl;
    cout << "TSEL ARRAY_GET_AROUND_" << index * 2 + 1 << " ARRAY_GET_AROUND_" << index * 2 + 2 << endl;
    gen(index * 2 + 1, left, middle);
    gen(index * 2 + 2, middle, right);
}

int main() {
    /*
    cout << "ARRAY_GET_AROUND:" << endl;
    cout << "LDC 1" << endl;
    cout << "TSEL ARRAY_GET_AROUND_0 ARRAY_GET_AROUND_0" << endl;
    */
    gen(0, 0, a_max);
    return 0;
}
