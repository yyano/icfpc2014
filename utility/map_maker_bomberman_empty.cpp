#include<bits/stdc++.h>

#define rep(i,n) for(int i=0;i<(int)n;i++)
#define all(c) (c).begin(),(c).end()
#define mp make_pair
#define pb push_back
#define each(i,c) for(__typeof((c).begin()) i=(c).begin();i!=(c).end();i++)
#define dbg(x) cerr<<__LINE__<<": "<<#x<<" = "<<(x)<<endl

using namespace std;

typedef long long ll;
typedef vector<int> vi;
typedef pair<int,int> pi;
const int inf = (int)1e9;
const double INF = 1e12, EPS = 1e-9;

/*
#######################
#..........#..........#
#.###.####.#.####.###.#
#o###.####.#.####.###o#
#.....................#
#.###.#.#######.#.###.#
#.....#....#....#.....#
#####.#### # ####.#####
#   #.#    =    #.#   #
#####.# ### ### #.#####
#    .  # === #  .    #
#####.# ####### #.#####
#   #.#    %    #.#   #
#####.# ####### #.#####
#..........#..........#
#.###.####.#.####.###.#
#o..#......\......#..o#
###.#.#.#######.#.#.###
#.....#....#....#.....#
#.########.#.########.#
#.....................#
#######################
*/

int main(int argc, const char **argv){
	if(argc < 3){
	  cout << "usage a.out height width" << endl;
	  exit(1);
  }
  int h = atoi(argv[1]), w = atoi(argv[2]);
  static char ans[256][257] = {};
  
  if(h <= 2 || w <= 2 || h > 256 || w > 256 || h % 2 == 0 || w % 2 == 0){
    cout << "invald map size" << endl;
    cout << "h, w are must be odd (greater than 2 and smaller than or eq to 256" << endl;
    exit(1);
  }
  srand(time(NULL));
  
  rep(i, h) rep(j, w){
    if((i == 0 || i == h - 1) || (j == 0 || j == w - 1)){
      ans[i][j] = '#';
      continue;
    }
    if(i % 2 == 0 && j % 2 == 0) ans[i][j] = '#';
    else ans[i][j] = ' ';
  }
  int numPowerPill = h * w / 256;
  int py[4] = {1, 1, h - 2, h - 2}, px[4] = {1, w - 2, 1, w - 2};
  
  rep(d, 4){
    ans[py[d]][px[d]] = d == 3 ? '\\' : '=';
  }
  
  while(numPowerPill){
    int y = rand() % (h - 2) + 1, x = rand() % (w - 2) + 1;
    if(ans[y][x] == '#' || ans[y][x] == '=' || ans[y][x] == '\\') continue;
    ans[y][x] = 'o';
    numPowerPill--;
  }
  
  ans[h / 2][w / 2] = '%';
  ans[h / 2][w / 2 - 1] = '.';
  rep(i, h) cout << ans[i] << endl;
  
	return 0;
}

