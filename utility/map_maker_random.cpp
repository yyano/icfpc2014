#include<bits/stdc++.h>

#define rep(i,n) for(int i=0;i<(int)n;i++)
#define all(c) (c).begin(),(c).end()
#define mp make_pair
#define pb push_back
#define each(i,c) for(__typeof((c).begin()) i=(c).begin();i!=(c).end();i++)
#define dbg(x) cerr<<__LINE__<<": "<<#x<<" = "<<(x)<<endl

using namespace std;

typedef long long ll;
typedef vector<int> vi;
typedef pair<int,int> pi;
const int inf = (int)1e9;
const double INF = 1e12, EPS = 1e-9;

int h, w;
static char ans[256][257];

bool connected(){
  static bool v[256][256] = {};
  queue<pi> q;
  memset(v, 0, sizeof(v));
  
  rep(i, h) rep(j, w) if(ans[i][j] != '#'){
    q.push(mp(i, j));
    i = h;
    break;
  }
  while(!q.empty()){
    int y = q.front().first, x = q.front().second;
    q.pop();
    
    if(v[y][x]) continue;
    v[y][x] = 1;
    
    rep(d, 4){
      const int dy[] = {-1, 0, 1, 0}, dx[] = {0, -1, 0, 1};
      int ny = y + dy[d], nx = x + dx[d];
      if(ny < 0 || nx < 0 || ny >= h || nx >= w) continue;
      if(ans[ny][nx] == '#' || v[ny][nx]) continue;
      q.push(mp(ny, nx));
    }
  }
  rep(i, h) rep(j, w) if(ans[i][j] != '#' && !v[i][j]) return 0;
  return 1;
}

bool done_ok[256][256];
bool forbid[256][256];

bool done(){
  rep(i, h - 1) rep(j, w - 1) if(!done_ok[i][j]){
    rep(d, 4){
      int y = i + (d % 2), x = j + (d / 2);
      if(ans[y][x] == '#'){
        done_ok[i][j] = 1;
        break;
      }
    }
    if(!done_ok[i][j]) return 0;
  }
  return 1;
}
/*
bool randomput(char c){
  int y = rand() % (h - 2) + 1, x = rand() % (w - 2) + 1;
  if(ans[y][x]) return 0;
  ans[y][x] = c;
  return 1;
}
*/
int main(int argc, const char **argv){
	if(argc < 3){
	  cout << "usage a.out height width" << endl;
	  exit(1);
  }
  h = atoi(argv[1]); w = atoi(argv[2]);
  
  if(h <= 2 || w <= 2 || h > 256 || w > 256 || h % 2 == 0 || w % 2 == 0){
    cout << "invald map size" << endl;
    cout << "h, w are must be odd (greater than 2 and smaller than or eq to 256" << endl;
    exit(1);
  }
  
  srand(time(NULL));
  
  //rep(i, h) rep(j, w) if(i == 0 || j == 0 || i == h - 1 || j == w - 1) ans[i][j] = '#';
  
  
   rep(i, h) rep(j, w){
    if((i == 0 || i == h - 1) || (j == 0 || j == w - 1)){
      ans[i][j] = '#';
      continue;
    }
    if(i % 2 == 0 && j % 2 == 0) ans[i][j] = '#';
  }
  
  int iter = h * w / 16;
  while(iter > 0){
    int y = rand() % (h - 2) + 1, x = rand() % (w - 2) + 1;
    
    if(ans[y][x] || forbid[y][x]) continue;
    ans[y][x] = '#';
    if(!connected()){
      ans[y][x] = 0;
      forbid[y][x] = 1;
      continue;
    }
    iter--;
  }
  
  rep(i, h) rep(j, w) if(!ans[i][j]) ans[i][j] = '.';
  
  rep(i, h) cout << ans[i] << endl;
  
	return 0;
}

